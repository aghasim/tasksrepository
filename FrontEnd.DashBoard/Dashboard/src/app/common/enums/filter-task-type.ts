export enum FilterTaskType {
  Controling = 1,
  Incoming,
  Outgoing,
  ActiveControling,
  ActiveIncoming,
  ActiveOutgoing,
  NewControling,
  NewIncoming,
  NewOutgoing,
  Tomorrow,
  Today,
  Past
}