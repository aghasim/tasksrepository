﻿using ProjectWorks.AmrApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Intefaces
{
    public interface ITaskDiscussion
    {
        Task<ICollection<TaskDiscussionOfTheTasks>> GetAll(int filialId);
        Task<TaskDiscussionOfTheTasks> Get(int id, int filialId);
        System.Threading.Tasks.Task Add(TaskDiscussionOfTheTasks discussion, int filialId);
        System.Threading.Tasks.Task Delete(int id, int filialId);
    }
}
