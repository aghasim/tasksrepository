export enum ImplementationMonitoringType {
  SelectedUsers = 1,
  SelectedRoles,
  WithoutControl
}