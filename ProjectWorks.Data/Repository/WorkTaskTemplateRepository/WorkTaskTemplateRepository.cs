﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskTemplateRepository
{
    public class WorkTaskTemplateRepository : GenericRepository<WorkTaskTemplate>, IWorkTaskTemplateRepository
    {
        #region Constructor

        public WorkTaskTemplateRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskTemplate>> GetAll(TokenData tokenData)
        {
            return await _entities
                .Where(project => project.FilialId == tokenData.FilialId && !project.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        }

        #endregion
    }
}