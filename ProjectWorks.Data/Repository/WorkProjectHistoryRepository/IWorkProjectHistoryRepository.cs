﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkProjectHistoryRepository
{
    public interface IWorkProjectHistoryRepository : IGenericRepository<WorkProjectHistory>
    {
        #region Methods

        Task<List<WorkProjectHistory>> GetAllByDate(Guid projectId, DateTime startDate, DateTime endDate);

        #endregion
    }
}