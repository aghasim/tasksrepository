import { ModelBase } from '../model.base';

export class Reviewer extends ModelBase {
  public taskId: string;
  public reviewerId: number;
  public reviewerName: string;
  public reviewerRoleName: string;
}
