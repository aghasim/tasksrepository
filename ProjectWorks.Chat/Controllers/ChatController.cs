﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Chat.ChatHub;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Filters;
using System.Threading.Tasks;

namespace ProjectWorks.Chat.Controllers
{
    [Authorize]
    [Route ("api/chat")]
    public class ChatController : Controller
    {
        private readonly IChatHub _chatHub;
        public ChatController(IChatHub chatHub)
        {
            _chatHub = chatHub;
        }

        [HttpPost]
        [Route ("/register")]
        public async Task Register(string roomId, string name, TokenData tokenData)
        {
            await _chatHub.Connect(name);
            await _chatHub.JoinGroup(roomId, tokenData);
        }

        [HttpPost]
        [Route ("/leave")]
        public async Task Leave(string roomId)
        {
            await _chatHub.LeaveGroup(roomId);
            await _chatHub.Disconnect();
        }

        [HttpPost]
        [Route ("/SendToGroup")]
        public async Task SendToGroup(string roomId, string message, TokenData tokenData)
        {
            await _chatHub.SendToGroup(roomId, message, tokenData);
        }
    
        [HttpPost]
        [Route ("/SendToUser")]
        public async Task SendToUser(string connId, string message)
        {
            await _chatHub.SendToUser(connId, message);
        }


    }
}
