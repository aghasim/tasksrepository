﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;
using ProjectWorks.Task.ViewModels.WorkTaskPattern;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/workTaskPattern")]
    public class WorkTaskPatternController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskPatternService _workTaskPatternService;

        private readonly ILogger<WorkTaskPatternController> _logger;

        #endregion

        #region Constructor

        public WorkTaskPatternController(IMapper mapper, IWorkTaskPatternService workTaskPatternService, ILogger<WorkTaskPatternController> logger)
        {
            _logger = logger;
            _mapper = mapper;
            _workTaskPatternService = workTaskPatternService;
        }
        #endregion

        #region Methods


        [HttpPost]
        public IActionResult Post([FromBody] CreateViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                return this.Success(_workTaskPatternService.Create(model.WorkTaskId, model.Name));
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return this.Error(e.ToString());
            }
        }



        #endregion


    }
}