﻿using System;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Dtos.Task
{
    public class WorkTaskExecutionOrderDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Наименование порядка исполнения задачи
        public string ExecutionOrderTypeName{ get; set; }

        // Тип порядка исполнения задачи
        public WorkTaskExecutionOrderType ExecutionOrderType { get; set; }

        // Порядок исполнения задачи
        public int ExecutionOrder { get; set; }

        #endregion
    }
}