﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ProjectWorks.Token.Helpers;
using ProjectWorks.Token.Services;
using Swashbuckle.AspNetCore.Swagger;
using System.Linq;
using System.Security.Claims;
using ProjectWorks.Core.Mapping;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Context;
using ProjectWorks.Data.Repository;
using ProjectWorks.Task.Extensions;
using ProjectWorks.Token.Extensions;
using ProjectWorks.AmrApp.Extensions;
using ProjectWorks.Chat.Extenson;
using ProjectWorks.Chat.ChatHub;
using ProjectWorks.Common.Extensions;
using ProjectWorks.Project.Extensions;

namespace ProjectWorks.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Register common services
            services.AddSingleton<IConfiguration>(this.Configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var key = JwtHelper.GetSecurityKey();
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = async context =>
                        {
                            var tokenService = context.HttpContext.RequestServices.GetRequiredService<TokenService>();
                            var userId = int.Parse(context.Principal.Identity.Name);
                            var sid = int.Parse(context.Principal.Claims.FirstOrDefault(i => i.Type == ClaimTypes.Sid)?.Value);
                            var user = await tokenService.Authenticate(userId, sid);
                            if (user == null)
                                context.Fail("Unauthorized");
                        }
                    };
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = false
                    };
                });

            // swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });
            // automapper
            var mappingConfig = new MapperConfiguration(
                config =>
                {
                    config.AddProfile(new MappingProfile());
                });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            // MemoryCache
            services.AddMemoryCache();
            // signalR
            services.AddSignalR();
            // Services
            services.AddToken();
            services.AddAmrApp();
            services.AddProjects();
            services.AddTask();
            services.AddChat();
            services.AddCommon();
            // Cors
            services.AddCors(options =>
            {
            options.AddPolicy("CorsPolicy",
                builder => builder.AllowAnyOrigin()//.WithOrigins("http://localhost:4200")
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        /*.AllowCredentials()*/);
            });
            // DataContext & repository
            services.AddSingleton(typeof(IDataContext), typeof(DataContext));
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            // MemoryCacheManager
            services.AddScoped(typeof(ICacheManager), typeof(MemoryCacheManager));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }

            app.UseCors("CorsPolicy");

            app.UseSignalR(routes => routes.MapHub<ChatHub>("/chat"));
            app.UseDefaultFiles();
            app.UseStaticFiles();
            

            app.UseAuthentication();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger(); 

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}/{id?}",
                    defaults: new
                    {
                        controller = "Home",
                        action = "Index"
                    }
                );

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new
                    {
                        controller = "Home",
                        action = "Index"
                    }
                );
            });
        }
    }
}
