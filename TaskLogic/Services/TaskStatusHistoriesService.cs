﻿using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.AmrApp.Repository;
using ProjectWorks.Contracts.Intefaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Services
{
    public class TaskStatusHistoriesService : ITaskStatusHistories
    {
        readonly AmrAppRepository amrApp;
        public TaskStatusHistoriesService(AmrAppRepository amrApp)
        {
            this.amrApp = amrApp;
        }

        public async Task<ICollection<TaskStatusHistories>> GetAll(int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskStatusHistories.ToListAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<TaskStatusHistories> Get (int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskStatusHistories.FirstOrDefaultAsync(x => x.Id == id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async System.Threading.Tasks.Task Add(TaskStatusHistories item, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                amrApp.Context.TaskStatusHistories.Add(item);
                await amrApp.Context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async System.Threading.Tasks.Task Delete (int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                var history = await amrApp.Context.TaskStatusHistories.FirstOrDefaultAsync(x => x.Id == id);
                history.IsDelete = true;
                await amrApp.Context.SaveChangesAsync();
            }catch(Exception)
            {
                throw;
            }
        }

    }
}
