﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkProjectRepository
{
    public class WorkProjectRepository : GenericRepository<WorkProject>, IWorkProjectRepository
    {
        #region Constructor

        public WorkProjectRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<List<WorkProject>> GetAll(TokenData tokenData)
        {
            var projects = await _entities
                .Where(project => project.CreatorId == tokenData.UserId && project.FilialId == tokenData.FilialId &&
                                  !project.IsDeleted)
                .OrderBy(entity => entity.Created)
                .ToListAsync();

            return projects ?? new List<WorkProject>();
        }

        public async Task<List<WorkProject>> GetAllByDates(TokenData tokenData, DateTime start, DateTime end)
        {
            var projects = await _entities
                .Where(project => project.CreatorId == tokenData.UserId && project.FilialId == tokenData.FilialId &&
                                  !project.IsDeleted && project.WorkProjectHistory.Any(x => x.Created >= start && x.Created <= end))
                .OrderBy(entity => entity.Created)
                .ToListAsync();

            return projects ?? new List<WorkProject>();
        }

        #endregion
    }
}