﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskHistoryRepository
{
    public interface IWorkTaskHistoryRepository : IGenericRepository<WorkTaskHistory>
    {
        #region Methods

        Task<List<WorkTaskHistory>> GetAll(Guid taskId);

        Task<List<WorkTaskHistory>> GetAllByDate(Guid taskId, DateTime startDate, DateTime endDate);

        Task<List<WorkTaskHistory>> GetAllByDateAndEditor(Guid taskId, int editorId, DateTime startDate, DateTime endDate);

        #endregion
    }
}