﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectWorks.Core.Dtos.Project;

namespace ProjectWorks.Core.Helpers.Project
{
    public static class ProjectHelper
    {
        #region Methods

        public static void GetProjectTree(IList<WorkProjectDto> projects, List<WorkProjectDto> result, Guid parentId)
        {
            var items = projects.Where(project => project.ParentId == parentId);
            if (items.Any())
                result.AddRange(items);
            else
                return;

            foreach (var item in items)
            {
                GetProjectTree(projects, item.ChildProjects, item.Id);
                item.ProjectResourceExpense += item.ChildProjects.Sum(project => project.ProjectResourceExpense);
            }
        } 

        #endregion
    }
}