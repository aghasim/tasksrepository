﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskRepository
{
    public interface IWorkTaskRepository : IGenericRepository<WorkTask>
    {
        #region Methods

        Task<List<WorkTask>> GetAll(Guid projectId);

        Task<List<WorkTask>> GetAllByDates(Guid projectId, DateTime start, DateTime end);

        Task<List<WorkTask>> GetAllByFilter(TokenData tokenData,
            FilterDatePeriodType filterDatePeriodType = FilterDatePeriodType.Month,
            FilterTaskType filterTaskType = FilterTaskType.Incoming);

        Task<List<WorkTask>> GetAllActive(TokenData tokenData, Guid projectId);

        Task<List<WorkTask>> GetAllControling(TokenData tokenData, Guid projectId);

        Task<List<WorkTask>> GetAllIncoming(TokenData tokenData, Guid projectId);

        Task<List<WorkTask>> GetAllOutgoing(TokenData tokenData, Guid projectId);

        Task<List<WorkTask>> GetChildByParentId(Guid parentTaskId);

        Task<int> GetChildTaskCount(Guid taskId, bool onlyCompleted = false);

        Task<decimal> GetTaskExpense(Guid taskId);

        Task<decimal> GetChildTaskExpenses(Guid parentTaskId);

        #endregion
    }
}