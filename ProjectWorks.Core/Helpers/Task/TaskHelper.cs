﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Core.Helpers.Task
{
    public class TaskHelper
    {
        #region Methods

        public static void GetTaskTree(IList<WorkTaskDto> projects, List<WorkTaskDto> result, Guid parentId)
        {
            var items = projects.Where(project => project.ParentId == parentId);
            if (items.Any())
                result.AddRange(items);
            else
                return;

            foreach (var item in items)
            {
                GetTaskTree(projects, item.ChildTasks, item.Id);
                item.TaskResourceExpense += item.ChildTasks.Sum(project => project.TaskResourceExpense);
            }
        }

        public static void FilterByExecutionOrder(List<WorkTaskDto> tasks, List<WorkTaskDto> result, Guid parentId)
        {
            result.AddRange(tasks.Where(task =>
                task.ExecutionOrder.ExecutionOrderType == WorkTaskExecutionOrderType.InParallel &&
                (task.Status == WorkTaskStatus.New || task.Status == WorkTaskStatus.InProgress)));

            var items = tasks
                .Where(task =>
                    task.ParentId == parentId && task.ExecutionOrder.ExecutionOrderType ==
                    WorkTaskExecutionOrderType.Consistently &&
                    (task.Status == WorkTaskStatus.New || task.Status == WorkTaskStatus.InProgress))
                .OrderBy(task => task.ExecutionOrder.ExecutionOrder);

            var firstItem = items.FirstOrDefault();
            if (firstItem != null)
            {
                result.Add(items.FirstOrDefault());
                FilterByExecutionOrder(tasks, result, firstItem.Id);
            }
        }

        #endregion
    }
}