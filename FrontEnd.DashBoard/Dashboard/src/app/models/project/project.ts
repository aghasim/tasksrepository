import { ModelBase } from '../model.base';
import { Resource, Timeline } from '..';

export class Project extends ModelBase {
  public childProjects: Project[];
  public completionDate: string;
  public description: string;
  public name: string;
  public parentId: string;
  public projectResource: Resource = new Resource();
  public responsibleId: number;
  public responsibleName: string;
  public startDate: string;
  public taskCompletedCount: number;
  public taskCount: number;
  public status: number;
  public statusName: string;
  public timeline: Timeline[] = [];
}
