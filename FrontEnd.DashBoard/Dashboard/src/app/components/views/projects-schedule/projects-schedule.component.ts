import * as moment from 'moment';
import { Component } from '@angular/core';
import { ProjectService, CommonService } from 'src/app/services';
import { first } from 'rxjs/operators';
import { Project, ListItem } from 'src/app/models';

@Component({
    templateUrl: 'projects-schedule.component.html',
})

export class ProjectsListScheduleComponent {
    //#region Fields 

    public projects: Project[];
    public months: ListItem[];
    public selectedMonth: number;
    public selectedYear: number;

    //#endregion

    //#region Constructor

    public constructor(
        private projectService: ProjectService,
        private commonService: CommonService
    ) {
        this.getMonths();
    }

    //#endregion

    //#region Methods

    getProjects() {
        this.projectService
            .getAllByMonth(this.selectedYear, this.selectedMonth)
            .pipe(first())
            .subscribe(
                projects => {
                    if (projects) {
                        this.projects = projects;
                        console.log(projects);
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    getMonths() {
        this.commonService
            .getMonths()
            .pipe(first())
            .subscribe(
                items => {
                    if (items) {
                        this.months = items.map(function (item) {
                            let mapItem = new ListItem();
                            mapItem.id = item.id;
                            mapItem.name = item.name;
                            mapItem.isChecked = false;
                            return mapItem;
                        });

                        this.selectedMonth = moment().month() + 1;
                        this.selectedYear = moment().year();
                        this.getProjects();
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    onSelectedMonthChange(month: number) {
        this.getProjects();
    }

    onSelectedYearChange(year: number) {
        this.getProjects();
    }
    //#endregion
}