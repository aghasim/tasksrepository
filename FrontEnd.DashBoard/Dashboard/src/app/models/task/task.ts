import { ModelBase } from '../model.base';
import { CloseControlType, CommunicationSettingsType, ImplementationMonitoringType, TaskAppointedTarget } from 'src/app/common';
import { Resource, ExecutionOrder, Timeline } from '..';

export class Task extends ModelBase {
  public name: string;
  public description: string;
  public responsibleId: number;
  public responsibleName: string;
  public responsibleRoleName: string;
  public startDate: string;
  public completionDate: string;
  public parentId: string;
  public taskResource: Resource = new Resource();
  public taskResourceId: string;
  public status: number;
  public statusName: string;
  public projectId: string;
  public childTasks: Task[];
  public appointedTarget: TaskAppointedTarget;
  public closeControlType: CloseControlType;
  public communicationSettingsType: CommunicationSettingsType;
  public implementationMonitoringType: ImplementationMonitoringType;
  public executionOrder: ExecutionOrder;
  public implementers: number[];
  public reviewers: number[];
  public filesToUpload: Array<File> = [];
  public messagesCount: number;
  public taskCompletedCount: number;
  public taskCount: number;
  public timeline: Timeline[] = [];
  public displayChild: boolean = false;
}