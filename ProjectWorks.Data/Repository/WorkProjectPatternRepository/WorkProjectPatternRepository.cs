﻿using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Data.Repository.WorkProjectPatternRepository
{
    public class WorkProjectPatternRepository: GenericRepository<WorkProjectPattern>, IWorkProjectPatternRepository
    {
        public WorkProjectPatternRepository(IDataContext context): base(context)
        {
        }
    }
}
