﻿using Microsoft.Extensions.DependencyInjection;
using ProjectWorks.Chat.ChatHub;
using ProjectWorks.Data.Repository.ChatConversationRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Chat.Extenson
{
    public static class AddChatExtension
    {
        public static IServiceCollection AddChat(this IServiceCollection services)
        {
            services.AddTransient<IChatConversationRepository, ChatConversationRepository>();
            services.AddTransient<IChatHub, ChatHub.ChatHub>();
            return services;

        }
    }
}
