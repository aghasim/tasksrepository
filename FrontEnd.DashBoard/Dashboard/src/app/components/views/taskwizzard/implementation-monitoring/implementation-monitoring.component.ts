import { Component, Input } from "@angular/core";
import { Task, ListItem } from 'src/app/models';
import { AmrappService } from 'src/app/services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'implementation-monitoring',
  templateUrl: "implementation-monitoring.component.html",
  styleUrls: ["implementation-monitoring.component.css"],
})
export class ImplementationMonitoringComponent {
  @Input() task: Task;
  private roleItems: ListItem[];
  private userItems: ListItem[];

  constructor(
    private amrappService: AmrappService
  ) {
    this.getRoles();
    this.getUsers();
  }

  onRoleChanged(selectedRoles: number[]) {
    this.setReviewersByRoles(selectedRoles);
  }

  onUserChanged(selectedUsers: number[]) {
    this.task.reviewers = selectedUsers;
  }

  getRoles() {
    if (!this.roleItems) {
      this.amrappService
        .getAllRoles()
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              if (items) {
                this.roleItems = items.map(function (item) {
                  let mapItem = new ListItem();
                  mapItem.id = item.id;
                  mapItem.name = item.name;
                  mapItem.isChecked = false;
                  return mapItem;
                });
              }
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  getUsers() {
    if (!this.userItems) {
      this.amrappService
        .getAllUsers()
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              if (items) {
                this.userItems = items.map(function (item) {
                  let mapItem = new ListItem();
                  mapItem.id = item.id;
                  mapItem.name = item.name;
                  mapItem.isChecked = false;
                  return mapItem;
                });
              }
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  setReviewersByRoles(ids: number[]) {
    this.amrappService
      .getUsersByRoles(ids)
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.task.reviewers = items.map(function (item) {
              return item.id;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }
}