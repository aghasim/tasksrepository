﻿using System.Collections.Generic;
using AutoMapper;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Project;
using ProjectWorks.Core.Dtos.Task;
using System.Linq;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Helpers.Enum;

namespace ProjectWorks.Core.Mapping
{
    public class MappingProfile : Profile
    {
        #region Constructor

        public MappingProfile()
        {
            // WorkProject
            CreateMap<WorkProject, WorkProjectDto>()
                .ForMember(dest => dest.StatusName, source => source.MapFrom(item => item.Status.DisplayName()))
                .ForMember(dest => dest.TaskCount, source => source.MapFrom(item => item.WorkTasks.Count))
                .ForMember(dest => dest.TaskCompletedCount, source => source.MapFrom(item => item.WorkTasks.Count(task => task.Status == WorkTaskStatus.Done || task.Status == WorkTaskStatus.Completed)))
                .ForMember(dest => dest.ProjectResource, source => source.MapFrom(item => item.WorkProjectResource));
            CreateMap<WorkProjectDto, WorkProject>()
                .ForMember(dest => dest.WorkProjectResource, source => source.MapFrom(item => item.ProjectResource));

            // WorkProjectResource
            CreateMap<WorkProjectResource, WorkProjectResourceDto>()
                .ForMember(dest => dest.TypeName, source => source.MapFrom(item => item.Type.DisplayName()));
            CreateMap<WorkProjectResourceDto, WorkProjectResource>();

            // WorkTask
            CreateMap<WorkTask, WorkTaskDto>()
                .ForMember(dest => dest.ProjectId, source => source.MapFrom(item => item.WorkProjectId))
                .ForMember(dest => dest.StatusName, source => source.MapFrom(item => item.Status.DisplayName()))
                .ForMember(dest => dest.TaskResource, source => source.MapFrom(item => item.WorkTaskResource ?? new WorkTaskResource()))
                .ForMember(dest => dest.CloseControlTypeName, source => source.MapFrom(item => item.CloseControlType.DisplayName()))
                .ForMember(dest => dest.CommunicationSettingsType, source => source.MapFrom(item => item.SettingCommunicationType))
                .ForMember(dest => dest.ExecutionOrder, source => source.MapFrom(item => item.WorkTaskExecutionOrder ?? new WorkTaskExecutionOrder()))
                .ForMember(dest => dest.Reviewers, source => source.MapFrom(item => item.WorkTaskReviewers.Any() ? item.WorkTaskReviewers.Select(x => x.ReviewerId) : new List<int>()))
                .ForMember(dest => dest.Implementers, source => source.MapFrom(item => item.WorkTaskImplementers.Any() ? item.WorkTaskImplementers.Select(x => x.ImplementerId) : new List<int>()));
            CreateMap<WorkTaskDto, WorkTask>()
                .ForMember(dest => dest.WorkProjectId, source => source.MapFrom(item => item.ProjectId))
                .ForMember(dest => dest.WorkTaskExecutionOrder, source => source.MapFrom(item => item.ExecutionOrder))
                .ForMember(dest => dest.WorkTaskResource, source => source.MapFrom(item => item.TaskResource))
                .ForMember(dest => dest.SettingCommunicationType, source => source.MapFrom(item => item.CommunicationSettingsType));

            // WorkTaskResource
            CreateMap<WorkTaskResource, WorkTaskResourceDto>()
                .ForMember(dest => dest.TypeName, source => source.MapFrom(item => item.Type.DisplayName()));
            CreateMap<WorkTaskResourceDto, WorkTaskResource>();

            // WorkTaskExecutionOrder
            CreateMap<WorkTaskExecutionOrder, WorkTaskExecutionOrderDto>()
                .ForMember(dest => dest.ExecutionOrderType, source => source.MapFrom(item => item.WorkTaskExecutionOrderType))
                .ForMember(dest => dest.ExecutionOrderTypeName, source => source.MapFrom(item => item.WorkTaskExecutionOrderType.DisplayName()));
            CreateMap<WorkTaskExecutionOrderDto, WorkTaskExecutionOrder>()
                .ForMember(dest => dest.WorkTaskExecutionOrderType, source => source.MapFrom(item => item.ExecutionOrderType));

            // WorkTaskResourceExpense
            CreateMap<WorkTaskResourceExpense, WorkTaskResourceExpenseDto>();
            CreateMap<WorkTaskResourceExpenseDto, WorkTaskResourceExpense>()
                .ForMember(dest => dest.WorkTaskResourceId, source => source.MapFrom(item => item.TaskResourceId));

            // WorkTaskHistory
            CreateMap<WorkTaskHistory, WorkTaskHistoryDto>()
                .ForMember(dest => dest.TaskId, source => source.MapFrom(item => item.WorkTaskId))
                .ForMember(dest => dest.StatusName, source => source.MapFrom(item => item.Status.DisplayName()))
                .ForMember(dest => dest.HistoryOperationTypeName, source => source.MapFrom(item => item.HistoryOperationType.DisplayName()));
            CreateMap<WorkTaskHistoryDto, WorkTaskHistory>()
                .ForMember(dest => dest.WorkTaskId, source => source.MapFrom(item => item.TaskId));

            // WorkTaskTemplate
            CreateMap<WorkTaskTemplate, WorkTaskTemplateDto>();
            CreateMap<WorkTaskTemplateDto, WorkTaskTemplate>();

            // WorkTaskReviewer
            CreateMap<WorkTaskReviewer, WorkTaskReviewerDto>()
                .ForMember(dest => dest.TaskId, source => source.MapFrom(item => item.WorkTaskId));
            CreateMap<WorkTaskReviewerDto, WorkTaskReviewer>();

            // WorkTaskImplementer
            CreateMap<WorkTaskImplementer, WorkTaskImplementerDto>()
                .ForMember(dest => dest.TaskId, source => source.MapFrom(item => item.WorkTaskId))
                .ForMember(dest => dest.StatusName, source => source.MapFrom(item => item.Status.DisplayName()));
            CreateMap<WorkTaskImplementerDto, WorkTaskImplementer>();

            // Conversation
            CreateMap<Conversation, ConversationDto>()
                .ForMember(dest => dest.TaskId, source => source.MapFrom(item => item.WorkTaskId));
            CreateMap<ConversationDto, Conversation>()
                .ForMember(dest => dest.WorkTaskId, source => source.MapFrom(item => item.TaskId));
        }

        #endregion
    }
}