﻿using System;
using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     История задачи
    /// </summary>
    public class WorkTaskHistory : EntityBase
    {
        #region Properties

        // Тип операции
        public HistoryOperationType HistoryOperationType { get; set; }

        // Статус задачи
        public WorkTaskStatus Status { get; set; }

        // Кто изменил, пользователь из А2
        [Required]
        public int EditorId { get; set; }

        // Описание
        public string Description { get; set; }

        // Задача Id
        public Guid WorkTaskId { get; set; }

        // Задача
        public virtual WorkTask WorkTask { get; set; }

        #endregion
    }
}