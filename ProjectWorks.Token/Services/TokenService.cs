﻿using System.Threading.Tasks;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.Token.Services
{
    public class TokenService
    {
        #region Fields

        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public TokenService(IUserService userService)
        {
            _userService = userService;
        }

        #endregion

        #region Methods

        public async Task<AppUsers> Authenticate(int userId, int filialId) =>
            await _userService.Get(filialId, userId);

        #endregion
    }
}