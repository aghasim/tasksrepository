﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Add_WorkTaskReviewer_WorkTaskImplementer_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CloseControlType",
                table: "WorkTasks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SettingCommunicationType",
                table: "WorkTasks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "WorkTaskImplementers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ImplementerId = table.Column<int>(nullable: false),
                    WorkTaskId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskImplementers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkTaskImplementers_WorkTasks_WorkTaskId",
                        column: x => x.WorkTaskId,
                        principalTable: "WorkTasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkTaskReviewers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ReviewerId = table.Column<int>(nullable: false),
                    WorkTaskId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskReviewers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkTaskReviewers_WorkTasks_WorkTaskId",
                        column: x => x.WorkTaskId,
                        principalTable: "WorkTasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskImplementers_WorkTaskId",
                table: "WorkTaskImplementers",
                column: "WorkTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskReviewers_WorkTaskId",
                table: "WorkTaskReviewers",
                column: "WorkTaskId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkTaskImplementers");

            migrationBuilder.DropTable(
                name: "WorkTaskReviewers");

            migrationBuilder.DropColumn(
                name: "CloseControlType",
                table: "WorkTasks");

            migrationBuilder.DropColumn(
                name: "SettingCommunicationType",
                table: "WorkTasks");
        }
    }
}
