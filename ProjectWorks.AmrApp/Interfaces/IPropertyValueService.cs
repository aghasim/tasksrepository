﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IPropertyValueService
    {
        #region Methods

        Task<PropertyValues> Get(int filialId, int objectId, int id);

        Task<IEnumerable<PropertyValues>> GetByObjectId(int filialId, int objectId);

        Task AddPropertyValue(TokenData tokenData, PropertyValues propertyValue);

        Task<Properties> GetDefaultProperties(int filialId);

        #endregion
    }
}