﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Helpers.Cache;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkTaskImplementerRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskImplementerService : ServiceBase<WorkTaskImplementer>, IWorkTaskImplementerService
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskImplementerRepository _workTaskImplementerRepository;

        #endregion

        #region Constructor

        public WorkTaskImplementerService(
            IMapper mapper,
            ICacheManager cacheManager, 
            IWorkTaskImplementerRepository workTaskImplementerRepository, 
            IUserService userService) :
            base(cacheManager, workTaskImplementerRepository, userService)
        {
            _mapper = mapper;
            _cacheManager = cacheManager;
            _workTaskImplementerRepository = workTaskImplementerRepository;
        }

        #endregion

        #region Methods

        public void Update(TokenData tokenData, WorkTaskImplementer entity)
        {
            CacheHelper.ClearCache(_cacheManager, tokenData, entity.WorkTask.WorkProjectId, entity.WorkTaskId);
            Update(entity);
        }

        public async Task<WorkTaskImplementerDto> GetByTask(TokenData tokenData, Guid taskId)
        {
            var dto = new WorkTaskImplementerDto();
            var key = string.Format(Constants.CacheConstants.IMPLEMENTER_KEY, taskId, tokenData.UserId);
            var implementer = await _cacheManager.Get(key, async () => await _workTaskImplementerRepository.GetByTask(tokenData, taskId));
            if (implementer != null)
            {
                dto = _mapper.Map<WorkTaskImplementerDto>(implementer);
                var user = await _userService.Get(implementer.WorkTask.FilialId, dto.ImplementerId);
                dto.ImplementerName = user?.UserFio;
                dto.ImplementerRoleName = user?.AppUserDepartment?.FuncRoles;
            }

            return dto;
        }

        public async Task<List<WorkTaskImplementerDto>> GetAll(TokenData tokenData, Guid taskId)
        {
            var key = string.Format(Constants.CacheConstants.IMPLEMENTER_ALL_KEY, taskId);
            var implementers = await _cacheManager.Get(key, async () => await _workTaskImplementerRepository.GetAll(taskId));
            var dtos = _mapper.Map<IList<WorkTaskImplementerDto>>(implementers);

            foreach (var dto in dtos)
            {
                var implementer = implementers.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(implementer.WorkTask.FilialId, dto.ImplementerId);
                dto.ImplementerName = user?.UserFio;
                dto.ImplementerRoleName = user?.AppUserDepartment?.FuncRoles;
            }

            return dtos.ToList();
        }

        #endregion
    }
}