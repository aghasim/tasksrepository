﻿using ProjectWorks.Core.Common.Enums;
using System;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Исполняющий задачу
    /// </summary>
    public class WorkTaskImplementer : EntityBase
    {
        #region Properties

        // Статус задачи
        public WorkTaskStatus Status { get; set; }

        // Исполняющий, пользователь из А2
        public int ImplementerId { get; set; }

        // Задача Id
        public Guid WorkTaskId { get; set; }

        // Задача
        public virtual WorkTask WorkTask { get; set; }

        #endregion
    }
}