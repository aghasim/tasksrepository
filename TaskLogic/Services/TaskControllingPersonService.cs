﻿using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.AmrApp.Repository;
using ProjectWorks.Contracts.Intefaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Services
{
    public class TaskControllingPersonService : ITaskControllingPerson
    {
        readonly AmrAppRepository amrApp;

        public TaskControllingPersonService(AmrAppRepository amrApp)
        {

            this.amrApp = amrApp;
        }

        public async Task<ICollection<TaskControllingPersons>> GetAll(int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskControllingPersons.ToListAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async Task <TaskControllingPersons> Get(int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskControllingPersons.FirstOrDefaultAsync(x => x.Id == id);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async System.Threading.Tasks.Task Add(TaskControllingPersons person, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                amrApp.Context.TaskControllingPersons.Add(person);
                await amrApp.Context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async System.Threading.Tasks.Task Delete(int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                var person = await amrApp.Context.TaskControllingPersons.FirstOrDefaultAsync(x => x.Id == id);
                person.IsDelete = true;
                await amrApp.Context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

    }
}
