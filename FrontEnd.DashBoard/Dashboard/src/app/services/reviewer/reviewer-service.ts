import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";

import { environment } from "../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ReviewerService {
    private apiUrl: string = environment.apiUrl + "reviewers/";

    constructor(private http: HttpClient) { }

    getAll(taskId: string) {
        return this.http
            .get<any>(this.apiUrl + "all/" + taskId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }
}
