﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ProjectWorks.Data.Resources;

namespace ProjectWorks.Data.Extensions
{
    public static class ActionResultExtension
    {
        public static IActionResult Success(this Controller controller)
        {
            var response = new ResourceResponse
            {
                Success = true,
                Data = null,
                Links = new List<ResourceLink>()
            };

            return controller.Ok(response);
        }



        public static IActionResult Success(this Controller controller, Object data)
        {
            var response = new ResourceResponse
            {
                Success = true,
                Data = data,
                Links = new List<ResourceLink>()
            };

            return controller.Ok(response);
        }



        public static IActionResult Success(this Controller controller, Object data, IEnumerable<String> messages)
        {
            var response = new ResourceResponse
            {
                Success = true,
                Data = data,
                Messages = messages,
                Links = new List<ResourceLink>()
            };

            return controller.Ok(response);
        }



        public static IActionResult Error(this Controller controller, ModelStateDictionary modelState)
        {
            var errors = modelState.SelectMany(s => s.Value.Errors.Select(e => new ResourceError()
            {
                Code = s.Key,
                Description = e.ErrorMessage
            }));

            return controller.Error(errors);
        }



        public static IActionResult Error(this Controller controller, String description)
        {
            return controller.Error("", description);
        }



        public static IActionResult Error(this Controller controller, String code, String description)
        {
            var error = new ResourceError()
            {
                Code = code,
                Description = description
            };

            return controller.Error(error);
        }



        public static IActionResult Error(this Controller controller, ResourceError error)
        {
            var errors = new List<ResourceError>() {
                error
            };

            return controller.Error(errors);
        }



        public static IActionResult Error(this Controller controller, IEnumerable<ResourceError> errors)
        {
            var response = new ResourceResponse
            {
                Success = false,
                Errors = errors,
                Links = new List<ResourceLink>()
            };

            return controller.BadRequest(response);
        }



        public static IActionResult Error(this Controller controller, IEnumerable<String> errors)
        {
            var response = new ResourceResponse
            {
                Success = false,
                Errors = errors.Select(error => new ResourceError() { Code = "", Description = error }),
                Links = new List<ResourceLink>()
            };

            return controller.BadRequest(response);
        }



        public static IActionResult Error(this Controller controller, Exception exception)
        {
            var errors = new List<String> {
                exception.Message,
                exception.StackTrace
            };

            if (exception.InnerException != null)
            {
                errors.Add(exception.InnerException.StackTrace);
                errors.Add(exception.InnerException.Message);
            }

            return controller.Error(errors);
        }



        public static IActionResult Fail(this Controller controller, Object data)
        {
            var response = new ResourceResponse
            {
                Data = data
            };

            return controller.StatusCode(500, response);
        }



        public static IActionResult Text(this Controller controller, IEnumerable<Object> list)
        {
            return controller.Text(list.Select(m => m.ToString()));
        }



        public static IActionResult Text(this Controller controller, IEnumerable<String> lines)
        {
            return controller.Text(lines.Implode(Environment.NewLine));
        }



        public static IActionResult Text(this Controller controller, params String[] lines)
        {
            return controller.Text(lines.AsEnumerable());
        }



        public static IActionResult Text(this Controller controller, StringBuilder stringBuilder)
        {
            return controller.Text(stringBuilder.ToString());
        }



        public static IActionResult Text(this Controller controller, String text)
        {
            return controller.Content(text, "text/plain", Encoding.UTF8);
        }



        public static IActionResult Csv<TRow>(this Controller controller, IEnumerable<TRow> rows) where TRow : class
        {
            return controller.Csv(
                groups: rows.GroupBy(m => ""),
                headings: null
            );
        }



        public static IActionResult Csv<TRow>(this Controller controller, IEnumerable<TRow> rows, IEnumerable<String> headings) where TRow : class
        {
            return controller.Csv(
                groups: rows.GroupBy(m => ""),
                headings: headings
            );
        }



        public static IActionResult Csv<TRow>(this Controller controller, IEnumerable<TRow> rows, IEnumerable<String> headings, String fileName) where TRow : class
        {
            return controller.Csv(
                groups: rows.GroupBy(m => ""),
                headings: headings,
                fileName: fileName
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: null
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, Action<IGrouping<TKey, TRow>, StringBuilder> action) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: null,
                action: action
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, Action<IGrouping<TKey, TRow>, StringBuilder> action) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: "Dump",
                action: action
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, String fileName, Action<IGrouping<TKey, TRow>, StringBuilder> action) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: fileName,
                action: action,
                addHeading: true
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, String fileName, Boolean addHeading, Action<IGrouping<TKey, TRow>, StringBuilder> action) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: fileName,
                action: action,
                addHeading: true,
                addSeparator: true
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: "Dump"
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, String fileName) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: null,
                fileName: fileName
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, String fileName) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: fileName,
                addSeparator: true
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: null,
                addSeparator: addSeparator
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: "Dump",
                addSeparator: addSeparator
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, Boolean addHeading, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: null,
                addHeading: addHeading,
                addSeparator: addSeparator
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, Boolean addHeading, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: "Dump",
                addHeading: addHeading,
                addSeparator: addSeparator
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, String fileName, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: null,
                fileName: fileName,
                addSeparator: addSeparator
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, String fileName, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: fileName,
                addHeading: true,
                addSeparator: addSeparator
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, String fileName, Boolean addHeading, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: null,
                fileName: fileName,
                addHeading: addHeading,
                addSeparator: addSeparator
            );
        }



        public static IActionResult Csv<TKey, TRow>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, String fileName, Boolean addHeading, Boolean addSeparator) where TRow : class
        {
            return controller.Csv(
                groups: groups,
                headings: headings,
                fileName: fileName,
                addHeading: addHeading,
                addSeparator: addSeparator,
                action: null
            );
        }



        public static IActionResult Csv<TRow, TKey>(this Controller controller, IEnumerable<IGrouping<TKey, TRow>> groups, IEnumerable<String> headings, String fileName, Boolean addHeading, Boolean addSeparator, Action<IGrouping<TKey, TRow>, StringBuilder> action) where TRow : class
        {
            var separator = ',';
            var builder = new StringBuilder();

            // Add separator directive for MS Excel
            if (addSeparator)
            {
                builder.AppendLine("sep=" + separator);
            }

            // Iterate all groupped rows
            foreach (var group in groups)
            {
                // Check if need to add heading row
                if (addHeading)
                {
                    // Check if heading row has been provided
                    if (headings == null)
                    {
                        // Create heading row from model properties
                        headings = typeof(TRow)
                            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                            .Select(m => m.Name)
                            .ToList();
                    }

                    // Add headings row
                    builder.AppendLine(headings
                        .Select(m => '"' + m.Trim('"') + '"')
                        .Implode(separator)
                    );
                }

                // Iterate each row in group
                foreach (var row in group)
                {
                    // Check if row has values
                    if (row == null)
                    {
                        // Add empty line
                        builder.AppendLine();
                        continue;
                    }

                    // Create row
                    var value = row.GetType()
                        .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                        .ToDictionary(m => m.Name, m => m.GetValue(row))
                        .Aggregate("", (x, y) => x + "\"" + y.Value + "\",").TrimEnd(',');

                    // Add row
                    builder.AppendLine(value);
                }

                // Invoke custom action after each group
                action?.Invoke(group, builder);
            }

            return controller.File(
                Encoding.UTF8.GetBytes(builder.ToString()),
                "application/csv",
                fileName
            );
        }

        public static IActionResult AccessDenied(this Controller controller)
        {
            var response = new ResourceResponse
            {
                Success = false,
                Data = null,
                Links = new List<ResourceLink>()
            };

            return controller.Ok(response);
        }
    }
}
