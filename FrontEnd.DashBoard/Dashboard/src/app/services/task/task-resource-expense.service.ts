import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";

import { TaskHistory } from "../../models";
import { environment } from "../../../environments/environment";
import { TaskResourceExpense } from 'src/app/models/task/task-resource-expense';

@Injectable({ providedIn: "root" })
export class TaskResourceExpenseService {
    private apiUrl: string = environment.apiUrl + "resourceexpenses/";

    constructor(private http: HttpClient) { }

    get(taskHistoryId: string) {
        return this.http
            .get<any>(this.apiUrl + taskHistoryId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getAll(taskId: string) {
        return this.http
            .get<any>(this.apiUrl + "all/" + taskId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    add(expense: TaskResourceExpense) {
        return this.http
            .post<any>(this.apiUrl + "add", { expense })
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }
}
