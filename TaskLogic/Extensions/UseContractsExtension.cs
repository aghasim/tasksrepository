﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Task;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Contracts.Extensions
{
    public static class UseContractsExtension
    {
        public static IApplicationBuilder UseContracts(this IApplicationBuilder builder)
        {
            // Apply database migrations if any
            //using (var context = new DataContextFactory().CreateDbContext(new String[] { }))
            //{
            //    context.Database.Migrate();
            //}

            return builder;
        }
    }
}
