﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class changeidinpatterns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkProjectPatterns_WorkProjects_WorkProjectId1",
                table: "WorkProjectPatterns");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkTaskPatterns_WorkTasks_WorkTaskId1",
                table: "WorkTaskPatterns");

            migrationBuilder.DropIndex(
                name: "IX_WorkTaskPatterns_WorkTaskId1",
                table: "WorkTaskPatterns");

            migrationBuilder.DropIndex(
                name: "IX_WorkProjectPatterns_WorkProjectId1",
                table: "WorkProjectPatterns");

            migrationBuilder.DropColumn(
                name: "WorkTaskId1",
                table: "WorkTaskPatterns");

            migrationBuilder.DropColumn(
                name: "WorkProjectId1",
                table: "WorkProjectPatterns");

            migrationBuilder.AlterColumn<Guid>(
                name: "WorkTaskId",
                table: "WorkTaskPatterns",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "WorkProjectId",
                table: "WorkProjectPatterns",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskPatterns_WorkTaskId",
                table: "WorkTaskPatterns",
                column: "WorkTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkProjectPatterns_WorkProjectId",
                table: "WorkProjectPatterns",
                column: "WorkProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkProjectPatterns_WorkProjects_WorkProjectId",
                table: "WorkProjectPatterns",
                column: "WorkProjectId",
                principalTable: "WorkProjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkTaskPatterns_WorkTasks_WorkTaskId",
                table: "WorkTaskPatterns",
                column: "WorkTaskId",
                principalTable: "WorkTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkProjectPatterns_WorkProjects_WorkProjectId",
                table: "WorkProjectPatterns");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkTaskPatterns_WorkTasks_WorkTaskId",
                table: "WorkTaskPatterns");

            migrationBuilder.DropIndex(
                name: "IX_WorkTaskPatterns_WorkTaskId",
                table: "WorkTaskPatterns");

            migrationBuilder.DropIndex(
                name: "IX_WorkProjectPatterns_WorkProjectId",
                table: "WorkProjectPatterns");

            migrationBuilder.AlterColumn<string>(
                name: "WorkTaskId",
                table: "WorkTaskPatterns",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "WorkTaskId1",
                table: "WorkTaskPatterns",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "WorkProjectId",
                table: "WorkProjectPatterns",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "WorkProjectId1",
                table: "WorkProjectPatterns",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskPatterns_WorkTaskId1",
                table: "WorkTaskPatterns",
                column: "WorkTaskId1");

            migrationBuilder.CreateIndex(
                name: "IX_WorkProjectPatterns_WorkProjectId1",
                table: "WorkProjectPatterns",
                column: "WorkProjectId1");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkProjectPatterns_WorkProjects_WorkProjectId1",
                table: "WorkProjectPatterns",
                column: "WorkProjectId1",
                principalTable: "WorkProjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkTaskPatterns_WorkTasks_WorkTaskId1",
                table: "WorkTaskPatterns",
                column: "WorkTaskId1",
                principalTable: "WorkTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
