﻿using Microsoft.AspNetCore.SignalR;
using ProjectWorks.Chat.Model;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Repository.ChatConversationRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.AmrApp.Interfaces;

namespace ProjectWorks.Chat.ChatHub
{
    public class ChatHub : Hub, IChatHub
    {
        #region Fields

        static readonly List<User> _users = new List<User>();

        private readonly IMapper _mapper;

        private readonly ICacheManager _cacheManager;

        private readonly IUserService _userService;

        private readonly IChatConversationRepository _repository;

        #endregion

        #region Constructor

        public ChatHub(
            IMapper mapper, 
            ICacheManager cacheManager, 
            IUserService userService, 
            IChatConversationRepository repository)
        {
            _mapper = mapper;
            _cacheManager = cacheManager;
            _repository = repository;
            _userService = userService;
        }

        #endregion

        #region Methods

        static bool IsUserExist(string connId)
        {
            return _users.Any(x => x.ConnectionId == connId);
        }

        public async Task Connect(string user)
        {
            if (IsUserExist(Context.ConnectionId)) return;
            _users.Add(new User { ConnectionId = Context.ConnectionId, UserName = user });
            await OnConnectedAsync();
        }

        public async Task JoinGroup(Guid taskId)
        {
            var connectionId = Context.ConnectionId;
            if (IsUserExist(connectionId))
                await Groups.AddToGroupAsync(connectionId, taskId.ToString());

            var conversations = await GetAll(taskId);
            foreach (var conversation in conversations)
                await Clients.Caller.SendAsync("ReceiveMessage", conversation);
        }

        public async Task Refresh(Guid taskId)
        {
            var conversations = await GetAll(taskId);
            foreach (var conversation in conversations)
                await Clients.Caller.SendAsync("ReceiveMessage", conversation);
        }

        public async Task LeaveGroup(string group)
        {
            var connId = Context.ConnectionId;
            if (IsUserExist(connId))
                await Groups.RemoveFromGroupAsync(connId, group);
        }

        public async Task Disconnect()
        {
            var connId = Context.ConnectionId;
            if (IsUserExist(connId))
            {
                var user = _users.FirstOrDefault(x => x.ConnectionId == connId);
                _users.Remove(user);
                await OnDisconnectedAsync(null);
            }
        }

        public async Task SendToAll(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", message);
        }

        public async Task SendToGroup(ConversationDto dto)
        {
            if (!IsUserExist(Context.ConnectionId))
                return;
            var conversation = _mapper.Map<Conversation>(dto);
            await _repository.Add(conversation);
            _repository.Save();

            ClearConversationCache(conversation.WorkTaskId);

            dto = _mapper.Map<ConversationDto>(conversation);
            if (conversation != null)
            {
                var user = await _userService.Get(conversation.WorkTask.FilialId, dto.SenderId);
                dto.SenderName = user?.UserFio;
                dto.SenderRoleName = user?.AppUserDepartment.FuncRoles;
            }
            await Clients.Group(dto.TaskId.ToString()).SendAsync("ReceiveMessage", dto);
        }

        public async Task SendToUser(string connId, string message)
        {
            await Clients.Client(connId).SendAsync("ReceiveMessage", message);
        }

        public async Task SendToCaller(string message)
        {
            await Clients.Caller.SendAsync("ReceiveMessage", message);
        }

        private async Task<IList<ConversationDto>> GetAll(Guid taskId)
        {
            var key = string.Format(Constants.CacheConstants.CONVERSATION_ALL_KEY, taskId);
            var conversations = await _cacheManager.Get(key, async () => await _repository.GetAll(taskId));
            var dtos = _mapper.Map<IList<ConversationDto>>(conversations);

            foreach (var dto in dtos)
            {
                var conversation = conversations?.FirstOrDefault(x => x.Id == dto.Id);
                if (conversation != null)
                {
                    var user = await _userService.Get(conversation.WorkTask.FilialId, dto.SenderId);
                    dto.SenderName = user?.UserFio;
                    dto.SenderRoleName = user?.AppUserDepartment.FuncRoles;
                }
            }

            return dtos.ToList();
        }

        private void ClearConversationCache(Guid taskId)
        {
            var key = string.Format(Constants.CacheConstants.CONVERSATION_ALL_KEY, taskId);
            _cacheManager.Remove(key);
        }

        #endregion
    }
}
