﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkTaskReviewerRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskReviewerService : ServiceBase<WorkTaskReviewer>, IWorkTaskReviewerService
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskReviewerRepository _taskReviewerRepository;

        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public WorkTaskReviewerService(
            IMapper mapper,
            ICacheManager cacheManager,
            IWorkTaskReviewerRepository taskReviewerRepository, 
            IUserService userService) : 
            base(cacheManager, taskReviewerRepository, userService)
        {
            _mapper = mapper;
            _cacheManager = cacheManager;
            _taskReviewerRepository = taskReviewerRepository;
            _userService = userService;
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskReviewerDto>> GetAll(Guid taskId)
        {
            var key = string.Format(Constants.CacheConstants.REVIEWER_ALL_KEY, taskId);
            var reviewers = await _cacheManager.Get(key, async () => await _taskReviewerRepository.GetAll(taskId));
            var dtos = _mapper.Map<IList<WorkTaskReviewerDto>>(reviewers);

            foreach (var dto in dtos)
            {
                var reviewer = reviewers.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(reviewer.WorkTask.FilialId, dto.ReviewerId);
                dto.ReviewerName = user?.UserFio;
                dto.ReviewerRoleName = user?.AppUserDepartment?.FuncRoles;
            }

            return dtos.ToList();
        } 

        #endregion
    }
}