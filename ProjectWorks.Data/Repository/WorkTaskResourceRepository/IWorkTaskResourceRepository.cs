﻿using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskResourceRepository
{
    public interface IWorkTaskResourceRepository : IGenericRepository<WorkTaskResource>
    {
    }
}