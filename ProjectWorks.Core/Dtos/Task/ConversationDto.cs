﻿using System;

namespace ProjectWorks.Core.Dtos.Task
{
    public class ConversationDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Дата
        public DateTime Created { get; set; }

        // отправитель
        public int SenderId { get; set; }

        // отправитель
        public string SenderName { get; set; }

        // отправитель
        public string SenderRoleName { get; set; }

        // получатель
        public int? RecipientId { get; set; }

        // сообщение
        public string Message { get; set; }

        // Задача Id
        public Guid TaskId { get; set; }

        #endregion
    }
}