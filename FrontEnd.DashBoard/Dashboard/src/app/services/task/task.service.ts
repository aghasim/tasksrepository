import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";

import { Task } from "../../models";
import { environment } from "../../../environments/environment";
import { FilterDatePeriodType } from 'src/app/common';

@Injectable({ providedIn: "root" })
export class TaskService {
    private apiUrl: string = environment.apiUrl + "tasks/";

    constructor(private http: HttpClient) { }

    get(projectId: string) {
        return this.http
            .get<any>(this.apiUrl + projectId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getAll(projectId: string) {
        return this.http
            .get<any>(this.apiUrl + "all/" + projectId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getAllByMonth(projectId: string, year: number, month: number) {
        return this.http
            .get<any>(this.apiUrl + "allbymonth/" + projectId + "/" + year + "/" + month)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getAllByFilter(filterDatePeriodType: string, filterTaskType: string) {
        return this.http
            .get<any>(this.apiUrl + "allbyfilter/" + filterDatePeriodType + "/" + filterTaskType)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getActive(projectId: string) {
        return this.http
            .get<any>(this.apiUrl + "active/" + projectId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getFilter(projectId: string, filterDatePeriodType: FilterDatePeriodType) {
        return this.http
            .get<any>(this.apiUrl + "filter/" + projectId + "/" + filterDatePeriodType)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    add(task: Task) {
        return this.http
            .post<any>(this.apiUrl + "add", { task })
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    update(task: Task) {
        return this.http
            .put<any>(this.apiUrl + "update", { task })
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    delete(taskId: string) {
        return this.http
            .delete<any>(this.apiUrl + taskId)
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    uploadFile(taskId: string, formData: FormData) {
        return this.http
            .post<any>(this.apiUrl + 'upload/' + taskId, formData)
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }
    
    updateStatus(taskId: string) {
        return this.http
            .get<any>(this.apiUrl + "updatestatus/" + taskId)
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }
}
