﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskTemplateRepository
{
    public interface IWorkTaskTemplateRepository : IGenericRepository<WorkTaskTemplate>
    {
        #region Methods

        Task<List<WorkTaskTemplate>> GetAll(TokenData tokenData);

        #endregion
    }
}