﻿using System;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectWorks.Data.Repository.ChatConversationRepository
{
    public class ChatConversationRepository : GenericRepository<Conversation>, IChatConversationRepository
    {
        public ChatConversationRepository(IDataContext context) : base(context)
        {
        }

        public async Task Add(Conversation conversation)
        {
            await _entities.AddAsync(conversation);
        }

        public async Task<IEnumerable<Conversation>> GetAll(Guid taskId)
        {
            return await _entities.Where(x => x.WorkTaskId == taskId && !x.IsDeleted).OrderBy(x => x.Created)
                .ToListAsync();
        }
    }
}
