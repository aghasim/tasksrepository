﻿using System;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Dtos.Task
{
    public class WorkTaskHistoryDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Дата
        public DateTime Created { get; set; }

        // Тип операции
        public HistoryOperationType HistoryOperationType { get; set; }

        // Тип операции
        public string HistoryOperationTypeName { get; set; }

        // Новый статус задачи
        public WorkTaskStatus Status { get; set; }

        // Новый статус задачи
        public string StatusName { get; set; }

        // Кто изменил, пользователь из А2
        public int EditorId { get; set; }

        // Кто изменил, пользователь из А2
        public string EditorName { get; set; }

        // Кто изменил, пользователь из А2
        public string EditorRoleName { get; set; }

        // Задача Id
        public Guid TaskId { get; set; }

        // Описание
        public string Description { get; set; }

        #endregion
    }
}