﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using Serilog;
using Constants = ProjectWorks.Core.Common.Constants;

namespace ProjectWorks.AmrApp.Services
{
    public class DepartmentService : IDepartmentService
    {
        #region Fields

        private readonly IAmrAppRepository _amrApp;

        private readonly ICacheManager _cacheManager;

        #endregion

        #region Constructor

        public DepartmentService(IAmrAppRepository amrApp,
            ICacheManager cacheManager)
        {
            _amrApp = amrApp;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public async Task<AppUserDepartments> Get(int filialId, int id)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.DEPARTMENT_CURRENT_KEY, filialId, id);
                Log.Information(string.Format(Constants.LogConstants.DEPARTMENTSERVICE_GETDEPARTMENT, filialId, id));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.AppUserDepartments
                        .FirstOrDefaultAsync(x => x.Id == id && x.FilialId == filialId));
            }

            return null;
        }

        public async Task<IEnumerable<AppUserDepartments>> GetByFilialId(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.DEPARTMENT_ALL_KEY, filialId);
                Log.Information(string.Format(Constants.LogConstants.DEPARTMENTSERVICE_GETDEPARTMENTS, filialId));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.AppUserDepartments
                        .Where(x => x.FilialId == filialId).ToListAsync());
            }

            return null;
        }

        #endregion
    }
}