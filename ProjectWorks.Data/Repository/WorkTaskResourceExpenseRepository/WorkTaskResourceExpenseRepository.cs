﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskResourceExpenseRepository
{
    public class WorkTaskResourceExpenseRepository : GenericRepository<WorkTaskResourceExpense>, IWorkTaskResourceExpenseRepository
    {
        #region Constructor

        public WorkTaskResourceExpenseRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskResourceExpense>> GetAll(Guid resourceId)
        {
            return await _entities
                .Where(expense => expense.WorkTaskResourceId == resourceId && !expense.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        }

        #endregion
    }
}