import { ModelBase } from '../model.base';
import { ExecutionOrderType } from 'src/app/common';

export class ExecutionOrder extends ModelBase {
  public executionOrderType: ExecutionOrderType;
  public executionOrder: number;
}