﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Тип ресурса
    /// </summary>
    public enum WorkTaskResourceType
    {
        /// <summary>
        ///     Денежные средства
        /// </summary>
        [Display(Name = "Денежные средства")]
        Money = 1,

        /// <summary>
        ///     Другой тип ресурса
        /// </summary>
        [Display(Name = "Другой тип")]
        Other
    }
}