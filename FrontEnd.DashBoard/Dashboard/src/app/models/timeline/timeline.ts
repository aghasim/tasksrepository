export class Timeline {
  public date: string;
  public day: number;
  public wDay: string;
  public status: number;
  public statusName: string;
  public duration: number;
  public today: boolean;
  public colorBg: string = "";
}