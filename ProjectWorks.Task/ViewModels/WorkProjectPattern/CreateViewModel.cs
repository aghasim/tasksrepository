﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectWorks.Task.ViewModels.WorkProjectPattern
{
    public class CreateViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string WorkProjectId { get; set; }
    }
}
