import * as moment from 'moment';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService, ProjectService, CommonService } from 'src/app/services';
import { first } from 'rxjs/operators';
import { Task, Project, ListItem, Timeline } from 'src/app/models';

@Component({
    templateUrl: 'project-schedule.component.html',
})

export class ProjectScheduleComponent {
    //#region Fields 

    public project: Project;
    public tasks: Task[];
    public months: ListItem[];
    public selectedMonth: number;
    public selectedYear: number;
    public timeLineTitleArr: Timeline[];
    public timeLineDateList: Timeline[];

    //#endregion

    //#region Constructor

    public constructor(
        private route: ActivatedRoute,
        private projectService: ProjectService,
        private taskService: TaskService,
        private commonService: CommonService,
    ) {
        this.timeLineDateList = [];

        var startDate = moment('01.01.2019');
        var endDate = moment('31.01.2019');
        
        var date = startDate;

        for(var i = 1; i <= 31; i++){

            var model = new Timeline();
                                
            model.date = date.toString();
            model.day = i;
            model.duration = 1;
            model.status = 7;
            model.statusName = "Новая";
            model.wDay = "ПН";
            model.today = (i == 10) ? true : false;

            this.timeLineDateList.push(model)
        }

        this.getMonths();
        this.route.paramMap.subscribe(params => {
            let projectId = params.get("id").toString();
            this.getProject(projectId);
        });
    }

    //#endregion

    //#region Methods

    getProject(projectId: string) {
        this.projectService
            .get(projectId)
            .pipe(first())
            .subscribe(
                project => {
                    if (project) {
                        this.project = project;
                        this.getTasks();
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }
    
    getTasks() {
        this.taskService
            .getAllByMonth(this.project.id, this.selectedYear, this.selectedMonth)
            .pipe(first())
            .subscribe(
                tasks => {
                    if (tasks) {
                        var getCurrentTask = function (task: Task) {

                            var timeLine: Array<Timeline> = [];
                            var i = 1;
                            
                            while(i <= 21)
                            {
                                var model = new Timeline();
                                
                                var date = moment(((i<10) ? '0' + i : i) + '.01.2019');

                                model.date = date.toString();
                                model.day = i;
                                model.duration = 1;
                                model.status = 0;
                                model.statusName = "";
                                model.wDay = "ПН";
                                model.today = (i == 10) ? true : false;

                                timeLine.push(model);
                                
                                i++;
                            }
                            
                            timeLine[4].duration = 1;
                            timeLine[4].status = 5;
                            timeLine[4].statusName = "новая";
                            timeLine[4].colorBg = "blue"; // ЭТО НЕ НАДО ПЕРЕДАВАТЬ С БЕКЕНДА )))))

                            timeLine[5].duration = 11;
                            timeLine[5].status = 1;
                            timeLine[5].statusName = "в работе";
                            timeLine[5].colorBg = "orange"; // ЭТО НЕ НАДО ПЕРЕДАВАТЬ С БЕКЕНДА )))))

                            task.startDate = moment(task.startDate).format('DD.MM.YYYY');
                            task.completionDate = moment(task.completionDate).format('DD.MM.YYYY');
                            task.timeline = timeLine;

                            task.childTasks.forEach(function (inTask: Task) {
                                return getCurrentTask(inTask);
                            });

                            task.childTasks.push(task);
                            task.childTasks.push(task);
                            task.childTasks.push(task);

                            return task;
                        }
                        
                        this.tasks = tasks.map(function (task: Task) {
                            return getCurrentTask(task);
                        })

                        console.log(this.tasks);
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    getMonths() {
        this.commonService
            .getMonths()
            .pipe(first())
            .subscribe(
                items => {
                    if (items) {
                        this.months = items.map(function (item) {
                            let mapItem = new ListItem();
                            mapItem.id = item.id;
                            mapItem.name = item.name;
                            mapItem.isChecked = false;
                            return mapItem;
                        });

                        this.selectedMonth = moment().month() + 1;
                        this.selectedYear = moment().year();
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    onSelectedMonthChange(month: number) {
        this.getTasks();
    }

    onSelectedYearChange(year: number) {
        this.getTasks();
    }
    //#endregion
}