﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkTaskTemplateService : IServiceBase<WorkTaskTemplate>
    {
        #region Methods

        Task<List<WorkTaskTemplate>> GetAll(TokenData tokenData);

        #endregion
    }
}