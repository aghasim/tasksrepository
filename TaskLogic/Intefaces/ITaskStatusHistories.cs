﻿using ProjectWorks.AmrApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Intefaces
{ 

    public interface ITaskStatusHistories
    {
        Task<ICollection<TaskStatusHistories>> GetAll(int filialId);
        Task<TaskStatusHistories> Get(int id, int filialId);
        System.Threading.Tasks.Task Delete(int id, int filialId);
        System.Threading.Tasks.Task Add(TaskStatusHistories item, int filialId);
    }
}
