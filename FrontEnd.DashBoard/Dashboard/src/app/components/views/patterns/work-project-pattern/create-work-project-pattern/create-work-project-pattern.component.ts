import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectPatternService } from 'src/app/services/project-pattern/project-pattern.service';
import { first } from 'rxjs/operators';

@Component({
    templateUrl: 'create-work-project-pattern.component.html',
    styleUrls: ['create-work-project-pattern.component.css'],
})

export class CreateWorkProjectPatternComponent implements OnInit {

    public name: string;
    public workProjectId: string;

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private projectPatternService: ProjectPatternService) {
        this.route.paramMap.subscribe(params => {
            this.workProjectId = params.get("id").toString();
        });
    }

    ngOnInit() {

    }

    onSave() {
        this.projectPatternService
          .add(this.name, this.workProjectId)
          .pipe(first())
          .subscribe(
            result => {
              if (result) {
                this.router.navigate(["/projects"]);
              }
            },
            error => {
              console.log(error);
            }
        );
    }
}