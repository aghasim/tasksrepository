import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ListItem } from 'src/app/models';
import { ListItemType } from 'src/app/common';

@Component({
  selector: 'checkbox-list',
  templateUrl: "checkbox-list.component.html"
})
export class CheckBoxListComponent implements OnInit {
  @Output() onSelectedItemsChanged = new EventEmitter<number[]>();
  private _items: ListItem[];
  private _selectedIds: number[];
  public title: string;
  private isAllChecked: boolean = false;
  private _listItemType: ListItemType;

  get items(): ListItem[] {
    return this._items;
  }

  @Input()
  set items(items: ListItem[]) {
    this._items = items;
    if (this._items)
      this.setSelectedItems();
  }

  get selectedIds(): number[] {
    return this._selectedIds;
  }

  @Input()
  set selectedIds(selectedIds: number[]) {
    this._selectedIds = selectedIds;
  }

  get listItemType(): ListItemType {
    return this._listItemType;
  }

  @Input()
  set listItemType(listItemType: ListItemType) {
    this._listItemType = listItemType;
    this.setTitle();
  }

  setSelectedItems() {
    var context = this;
    if (context.selectedIds) {
      this.items.forEach(function (item) {
        item.isChecked = context.isNeedChecked(item, context.selectedIds);
      });

      if (this.onSelectedItemsChanged)
        this.onSelectedItemsChanged.emit(this.selectedIds);
    }
  }

  isNeedChecked(item: ListItem, selectedIds: number[]): boolean {
    return selectedIds.some((id: number) => id == item.id);
  }

  selectedItemsChanged() {
    var filterItems = this.items.filter(function (item) {
      return item.isChecked;
    });
    var selectedIds = filterItems.map(function (item) {
      return item.id;
    });

    if (this.onSelectedItemsChanged)
      this.onSelectedItemsChanged.emit(selectedIds);
  }

  ngOnInit() {
    this.setTitle();
  }

  setTitle() {
    if (this.listItemType) {
      switch (this.listItemType) {
        case ListItemType.ObjectType: {
          this.title = "ВЫБЕРЕТЕ ТИПЫ ОБЬЕКТОВ"
          break;
        }
        case ListItemType.Object: {
          this.title = "ВЫБЕРЕТЕ ОБЪЕКТЫ"
          break;
        }
        case ListItemType.Role: {
          this.title = "ВЫБЕРЕТЕ РОЛИ"
          break;
        }
        case ListItemType.Department: {
          this.title = "ВЫБЕРЕТЕ ОТДЕЛЫ"
          break;
        }
        case ListItemType.User: {
          this.title = "ВЫБЕРИТЕ ПОЛЬЗОВАТЕЛЕЙ"
          break;
        }
        default: {
          this.title = "КОМУ ПОСТАВИТЬ ЗАДАЧУ"
          break;
        }
      }
    }
  }

  onAllChecked() {
    var checked = this.isAllChecked;
    this.items.forEach(function (item) {
      item.isChecked = checked;
    });
    this.selectedItemsChanged();
  }
}