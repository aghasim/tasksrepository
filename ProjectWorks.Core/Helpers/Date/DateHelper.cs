﻿using System;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Models.Date;

namespace ProjectWorks.Core.Helpers.Date
{
    public static class DateHelper
    {
        #region Methods

        public static DatePeriod GetDatePeriod(FilterDatePeriodType periodType)
        {
            var result = new DatePeriod();
            var now = DateTime.UtcNow;

            switch (periodType)
            {
                case FilterDatePeriodType.Week:
                    var daysToMonday = now.DayOfWeek - DayOfWeek.Monday;
                    result.Start = now.AddDays(-daysToMonday);
                    result.End = result.Start.AddDays(6);
                    break;
                case FilterDatePeriodType.Month:
                    result.Start = new DateTime(now.Year, now.Month, 1);
                    result.End = result.Start.AddMonths(1).AddDays(-1);
                    break;
                case FilterDatePeriodType.Quarter:
                    var quarter = (now.Month - 1) / 3 + 1;
                    result.Start = new DateTime(now.Year, 3 * quarter - 2, 1);
                    result.End = new DateTime(now.Year, 3 * quarter + 1, 1).AddDays(-1);
                    break;
                case FilterDatePeriodType.None:
                    return null;
            }

            return result;
        }

        #endregion
    }
}