﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.AmrApp.Repository;
using ProjectWorks.AmrApp.Services;

namespace ProjectWorks.AmrApp.Extensions
{
    public static class AddAmrAppExtension
    {
        #region Methods

        public static IServiceCollection AddAmrApp(this IServiceCollection services)
        {
            var configuration = services.BuildServiceProvider()
                .GetRequiredService<IConfiguration>();

            services.AddTransient<IChangeConnectionString, GetConnectionStringService>();
            // Configure IOptions

            // Register databse context
            //services.AddDbContext<aspnetsmrContext>(builder =>
            //{
            //    builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            //});
            services.AddTransient<AmrAmmContextFactory>();
            services.AddTransient<IAmrAppRepository, AmrAppRepository>();
            // Register module providers

            // Register module services
            services.AddTransient<IUserService, UsersService>();
            services.AddTransient<IObjectService, ObjectService>();
            services.AddTransient<IObjectTypeSevice, ObjectTypeSevice>();
            services.AddTransient<IDepartmentService, DepartmentService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IExpenditureService, ExpenditureService>();
            services.AddTransient<IPropertyValueService, PropertyValueService>();
            // Register module repositories
            return services;
        }

        #endregion
    }
}