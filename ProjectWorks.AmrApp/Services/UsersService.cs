﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using System.Threading.Tasks;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using Serilog;
using Constants = ProjectWorks.Core.Common.Constants;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace ProjectWorks.AmrApp.Services
{
    public class UsersService : IUserService
    {
        #region Fields

        private readonly IAmrAppRepository _amrApp;

        private readonly ICacheManager _cacheManager;

        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constructor

        public UsersService(IAmrAppRepository amrApp,
           ICacheManager cacheManager, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _amrApp = amrApp;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public async Task<AppUsers> Get(int filialId, int id)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.APPUSER_CURRENT_KEY, filialId, id);
                Log.Information(string.Format(Constants.LogConstants.USERSERVICE_GETUSER, filialId, id));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.AppUsers.Include(x => x.AppUserDepartment)
                        .FirstOrDefaultAsync(x => x.Id == id && x.NotDel && x.FilialId == filialId));
            }

            return null;
        }

        public async Task<IEnumerable<AppUsers>> GetByFilialId(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.APPUSER_ALL_KEY, filialId);
                Log.Information(string.Format(Constants.LogConstants.USERSERVICE_GETUSERS, filialId));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.AppUsers.Include(x => x.AppUserDepartment)
                        .Where(x => x.NotDel && x.FilialId == filialId).ToListAsync());
            }

            return new List<AppUsers>();
        }

        public async Task<IEnumerable<AppUsers>> GetAllUsersByObjects(int filialId, List<int> ids)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.APPUSER_ALL_BY_OBJECTS_KEY, filialId, ids.GetHashCode());
                Log.Information(string.Format(Constants.LogConstants.USERSERVICE_GETUSERS, filialId));
                return await _cacheManager.Get(key, async () => await GetDepartmentsByObjects(filialId, ids));
            }

            return new List<AppUsers>();

        }

        public async Task<IEnumerable<AppUsers>> GetAllUsersByDepartments(int filialId, List<int> ids)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.APPUSER_ALL_BY_DEPARTMENT_KEY, filialId, ids.GetHashCode());
                Log.Information(string.Format(Constants.LogConstants.USERSERVICE_GETUSERS, filialId));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.AppUsers.Include(x => x.AppUserDepartment)
                        .Where(x => x.NotDel && x.FilialId == filialId && x.AppUserDepartmentId.HasValue &&
                                    ids.Contains(x.AppUserDepartmentId.Value)).ToListAsync());
            }

            return new List<AppUsers>();
        }

        public async Task<IEnumerable<AppUsers>> GetAllUsersByRoles(int filialId, List<int> ids)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.APPUSER_ALL_BY_ROLES_KEY, filialId, ids.GetHashCode());
                Log.Information(string.Format(Constants.LogConstants.USERSERVICE_GETUSERS, filialId));
                return await _cacheManager.Get(key, async () => await GetUsersByRoles(filialId, ids));
            }

            return new List<AppUsers>();
        }

        private async Task<List<AppUsers>> GetUsersByRoles(int filialId, List<int> ids)
        {
            var result = new List<AppUsers>();
            foreach (var id in ids)
                result.AddRange(await GetUsersByRoleId(id, filialId));

            return result;
        }

        private async Task<List<AppUsers>> GetUsersByRoleId(int roleId, int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.GET_APPUSER_BY_ROLE_ID_KEY, filialId, roleId);
                var users = from us in _amrApp.Context.AppUsers
                    join usDp in _amrApp.Context.AppUserDepartments on us.AppUserDepartmentId equals usDp.Id
                    join usDpRl in _amrApp.Context.AppUserDepartmentRoles on usDp.Id equals usDpRl.DepartmentId
                    where usDpRl.FuncRoleId == roleId && usDp.NotDel && usDp.FilialId == filialId
                    select us;

                return await _cacheManager.Get(key, async () => await users.ToListAsync());
            }

            return new List<AppUsers>();
        }

        private async Task<IEnumerable<AppUsers>> GetDepartmentsByObjects(int filialId, List<int> ids)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var objectNames = _amrApp.Context.Object1.Where(x => ids.Contains(x.Id)).Select(x => x.ObjectName);
                var departments = await _amrApp.Context.AppUserDepartments.Where(x => objectNames.Contains(x.Name))
                    .Select(x => x.Id).ToListAsync();

                return await GetAllUsersByDepartments(filialId, departments);
            }

            return new List<AppUsers>();
        }

        public string GetRoleNameByUserId(int filialId, int userId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                return _amrApp.Context.AppUserDepartments
                    .FirstOrDefault(x => x.Id == _amrApp.Context.AppUsers.FirstOrDefault(z => z.Id == userId).AppUserDepartmentId)?.FuncRoles;
            }

            return string.Empty;
        }

        #endregion

        #region Properties

        public int CurrentUserFilialId
        {
            get
            {
                var userClaim = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                return userClaim != null ? int.Parse(userClaim.Value) : 0;
            }
        }

        public int CurrentUserId
        {
            get
            {
                var filialClaim = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                return filialClaim != null ? int.Parse(filialClaim.Value) : 0;
            }
        }

        #endregion


    }
}
