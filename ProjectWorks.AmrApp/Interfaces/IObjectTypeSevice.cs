﻿using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IObjectTypeSevice : IAmrService<ObjectTypes>
    {
    }
}