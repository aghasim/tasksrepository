﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Filters;
using ProjectWorks.Core.Models.Response;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/taskhistories")]
    public class TaskHistoryController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskHistoryService _taskHistoryService;

        #endregion

        #region Constructor

        public TaskHistoryController(IMapper mapper, IWorkTaskHistoryService taskHistoryService)
        {
            _mapper = mapper;
            _taskHistoryService = taskHistoryService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var item = await _taskHistoryService.Get(id);
            var dto = _mapper.Map<WorkTaskHistoryDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpGet("all/{id}")]
        public async Task<IActionResult> GetAll(Guid id)
        {
            var items = await _taskHistoryService.GetAll(id);
            var dtos = _mapper.Map<IList<WorkTaskHistoryDto>>(items);
            return this.Success(dtos);
        }

        [FilialAction]
        [HttpPost("add")]
        public IActionResult Add([FromBody] WorkTaskHistoryResponse response)
        {
            var item = _mapper.Map<WorkTaskHistory>(response.History);
            _taskHistoryService.Add(item);
            return this.Success();
        }

        [FilialAction]
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] WorkTaskHistoryResponse response)
        {
            var item = await _taskHistoryService.Get(response.History.Id);
            if (item != null)
            {
                item.EditorId = response.History.EditorId;
                item.Status = response.History.Status;

                _taskHistoryService.Update(item);
                return this.Success();
            }

            return this.Error($"Task history {response.History.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _taskHistoryService.Delete(id);
            return this.Success();
        }

        #endregion
    }
}