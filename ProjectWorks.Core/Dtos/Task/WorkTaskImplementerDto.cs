﻿using System;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Dtos.Task
{
    public class WorkTaskImplementerDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Статус задачи
        public WorkTaskStatus Status { get; set; }

        // Статус задачи
        public string StatusName { get; set; }

        // Задача Id
        public Guid TaskId { get; set; }

        // Исполняющий, пользователь из А2
        public int ImplementerId { get; set; }

        // Исполняющий, пользователь из А2
        public string ImplementerName { get; set; }

        // Исполняющий, пользователь из А2
        public string ImplementerRoleName { get; set; }

        #endregion
    }
}