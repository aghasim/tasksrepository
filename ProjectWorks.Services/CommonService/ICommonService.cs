﻿using System.Collections.Generic;
using ProjectWorks.Core.Dtos.Common;

namespace ProjectWorks.Services.CommonService
{
    public interface ICommonService
    {
        #region Methods

        IEnumerable<CommonDto> GetTaskStatusTypes();

        IEnumerable<CommonDto> GetTaskExecutionOrderTypes();

        IEnumerable<CommonDto> GetTaskResourceTypes();

        IEnumerable<CommonDto> GetMonths();

        #endregion
    }
}