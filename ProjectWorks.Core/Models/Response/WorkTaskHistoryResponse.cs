﻿using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Core.Models.Response
{
    public class WorkTaskHistoryResponse
    {
        #region Properties

        public WorkTaskHistoryDto History { get; set; }

        #endregion
    }
}