﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Domain;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : EntityBase
    {
        #region Constructor

        public GenericRepository(IDataContext context)
        {
            _context = context;
            _entities = _context.Set<TEntity>();
        }

        #endregion

        #region Fields

        private readonly IDataContext _context;

        protected readonly DbSet<TEntity> _entities;

        #endregion

        #region Methods

        public async Task<TEntity> Get(Guid id)
        {
            return await _entities.FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public TResult Query<T, TResult>(Func<IQueryable<T>, TResult> query) where T : EntityBase
        {
            return query(_context.Set<T>());
        }

        public async Task Insert(TEntity entity)
        {
            await _entities.AddAsync(entity);
            await Save();
        }

        public async Task Update(TEntity entity)
        {
            await Save();
        }

        public async Task Delete(Guid id)
        {
            var entity = await _entities.FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                entity.IsDeleted = true;
                await Save();
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        #endregion
    }
}