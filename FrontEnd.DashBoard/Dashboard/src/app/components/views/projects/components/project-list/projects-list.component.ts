import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService } from "../../../../../services";
import { first } from 'rxjs/operators';
import * as moment from 'moment';
import { Project } from 'src/app/models';

@Component({
    selector: 'project-list',
    templateUrl: 'projects-list.component.html',
    styleUrls: ['projects-list.component.css'],
})

export class ProjectsListComponent implements OnInit {

    public allProject: Project[];

    constructor(
        private projectService: ProjectService,
    ) {
    }
   
    ngOnInit() {
        this.getAll();
    }

    onDelete() {
        this.getAll();
    }

    getAll() {
        this.projectService
            .getAll()
            .pipe(first())
            .subscribe(
                projects => {
                    if (projects) {
                        var GetCurrentProject = function(project: Project) {
                            project.startDate = moment(project.startDate).format('DD.MM.YYYY');
                            project.completionDate = moment(project.completionDate).format('DD.MM.YYYY');
                            project.childProjects.forEach(function(inProject: Project){
                                return GetCurrentProject(inProject);
                            });
                            return project;
                        }
                        this.allProject = projects.map(function(project : Project){
                            return GetCurrentProject(project);
                        })
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }
}