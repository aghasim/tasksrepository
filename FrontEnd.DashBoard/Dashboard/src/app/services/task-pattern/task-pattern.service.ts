import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Project } from '../../models';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class TaskPatternService {
  private apiUrl: string = environment.apiUrl + 'workTaskPattern/';

  constructor(private http: HttpClient) {
  }

  add(name: string, workTaskId: string) {
    return this.http.post<any>(this.apiUrl, { Name: name, WorkTaskId: workTaskId })
      .pipe(map(result => {
        return result.success;
      }))
      .pipe(catchError((err, caught) => {
        return null;
      }))
  }
}