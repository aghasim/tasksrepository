﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Ресурс задачи
    /// </summary>
    public class WorkTaskResource : EntityBase
    {
        #region Properties

        // Тип ресурса
        public WorkTaskResourceType Type { get; set; }

        // Количество ресурса на выполнение
        [Required]
        [Range(0, double.MaxValue)]
        public decimal Count { get; set; }

        // Статья расхода из а2
        public int? ExpenditureId { get; set; }


        // Расходы ресурса
        public virtual ICollection<WorkTaskResourceExpense> WorkTaskResourceExpenses { get; set; } = new List<WorkTaskResourceExpense>();

        #endregion
    }
}