export enum CommunicationSettingsType {
  WithGroupDiscussion = 1,
  WithPersonalDiscussion,
  WithoutDiscussion
}