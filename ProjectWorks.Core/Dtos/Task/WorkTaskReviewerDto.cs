﻿using System;

namespace ProjectWorks.Core.Dtos.Task
{
    public class WorkTaskReviewerDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Задача Id
        public Guid TaskId { get; set; }

        // Проверяющий, пользователь из А2
        public int ReviewerId { get; set; }

        // Проверяющий, пользователь из А2
        public string ReviewerName { get; set; }

        // Проверяющий, пользователь из А2
        public string ReviewerRoleName { get; set; }

        #endregion
    }
}