﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Filters;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/resources")]
    public class TaskResourceController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskResourceService _taskResourceService;

        #endregion

        #region Constructor

        public TaskResourceController(IMapper mapper, IWorkTaskResourceService taskResourceService)
        {
            _mapper = mapper;
            _taskResourceService = taskResourceService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var item = await _taskResourceService.Get(id);
            var dto = _mapper.Map<WorkTaskResourceDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpPost("add")]
        public IActionResult Add([FromBody] WorkTaskResourceDto dto)
        {
            var item = _mapper.Map<WorkTaskResource>(dto);
            _taskResourceService.Add(item);
            return this.Success();
        }

        [FilialAction]
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] WorkTaskResourceDto dto)
        {
            var item = await _taskResourceService.Get(dto.Id);
            if (item != null)
            {
                item.Type = dto.Type;
                item.Count = dto.Count;

                _taskResourceService.Update(item);
                return this.Success();
            }

            return this.Error($"Task resource {dto.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _taskResourceService.Delete(id);
            return this.Success();
        }

        #endregion
    }
}