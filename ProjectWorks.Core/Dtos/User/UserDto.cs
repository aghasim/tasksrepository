﻿namespace ProjectWorks.Core.Dtos.User
{
    public class UserDto
    {
        #region Properties

        public int UserId { get; set; }

        public int FilialId { get; set; }

        #endregion
    }
}