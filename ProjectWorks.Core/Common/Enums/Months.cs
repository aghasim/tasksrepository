﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Месяцы
    /// </summary>
    public enum Months
    {
        /// <summary>
        ///     Январь
        /// </summary>
        [Display(Name = "Январь")]
        January = 1,

        /// <summary>
        ///     Февраль
        /// </summary>
        [Display(Name = "Февраль")]
        February = 2,

        /// <summary>
        ///     Март
        /// </summary>
        [Display(Name = "Март")]
        March = 3,

        /// <summary>
        ///     Апрель
        /// </summary>
        [Display(Name = "Апрель")]
        April = 4,

        /// <summary>
        ///     Май
        /// </summary>
        [Display(Name = "Май")]
        May = 5,

        /// <summary>
        ///     Июнь
        /// </summary>
        [Display(Name = "Июнь")]
        June = 6,

        /// <summary>
        ///     Июль
        /// </summary>
        [Display(Name = "Июль")]
        July = 7,

        /// <summary>
        ///     Август
        /// </summary>
        [Display(Name = "Август")]
        August = 8,

        /// <summary>
        ///     Сентябрь
        /// </summary>
        [Display(Name = "Сентябрь")]
        September = 9,

        /// <summary>
        ///     Октябрь
        /// </summary>
        [Display(Name = "Октябрь")]
        October = 10,

        /// <summary>
        ///     Ноябрь
        /// </summary>
        [Display(Name = "Ноябрь")]
        November = 11,

        /// <summary>
        ///     Декабрь
        /// </summary>
        [Display(Name = "Декабрь")]
        December = 12
    }
}