import { Component, OnInit, Input } from "@angular/core";
import { first } from "rxjs/operators";

import { AmrappService } from "../../../../services";
import { Task, ListItem } from "src/app/models";
import { TaskAppointedTarget } from "src/app/common";

@Component({
  selector: "create-task-settings",
  templateUrl: "create-task-settings.component.html",
  styleUrls: ["create-task-settings.component.css"]
})
export class CreateTaskSettingsComponent implements OnInit {
  //#region Fields
  @Input() task: Task;
  private userItems: ListItem[];
  private objectTypeItems: ListItem[];
  private objectItems: ListItem[];
  private roleItems: ListItem[];
  private departmentItems: ListItem[];
  private _appointedType: TaskAppointedTarget;
  //#endregion

  //#region Properties

  get appointedType(): TaskAppointedTarget {
    return this._appointedType;
  }

  set appointedType(appointedType: TaskAppointedTarget) {
    this._appointedType = appointedType;
    this.task.appointedTarget = this._appointedType;
    this.onAppointedTypeChange();
  }

  //#endregion

  //#region Constructor
  constructor(private amrappService: AmrappService) { }
  //#endregion

  //#region Methods
  ngOnInit() {
    this.appointedType = this.task.appointedTarget;
    this.onAppointedTypeChange();
  }

  onObjectTypeChanged(selectedObjectTypes: number[]) {
    this.getObjectsByTypes(selectedObjectTypes);
  }

  onObjectChanged(selectedObjects: number[]) {
    this.getUsersByObjects(selectedObjects);
  }

  onDepartmentChanged(selectedDepartments: number[]) {
    this.getUsersByDepartments(selectedDepartments);
  }

  onRoleChanged(selectedRoles: number[]) {
    this.getUsersByRoles(selectedRoles);
  }

  onUserChanged(selectedUsers: number[]) {
    this.task.implementers = selectedUsers;
  }

  onAppointedTypeChange() {
    switch (this.task.appointedTarget) {
      case TaskAppointedTarget.ObjectType: {
        this.getObjectTypes();
        this.getObjectsByTypes(null);
        break;
      }
      case TaskAppointedTarget.Object: {
        this.getObjects();
        break;
      }
      case TaskAppointedTarget.Role: {
        this.getRoles();
        break;
      }
      case TaskAppointedTarget.Department: {
        this.getDepartments();
        break;
      }
      case TaskAppointedTarget.User: {
        this.getUsers();
        break;
      }
      default: {
        break;
      }
    }
  }

  getObjectTypes() {
    if (!this.objectTypeItems) {
      this.amrappService
        .getAllObjectTypes()
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              this.objectTypeItems = items.map(function (item) {
                let mapItem = new ListItem();
                mapItem.id = item.id;
                mapItem.name = item.name;
                mapItem.isChecked = false;
                return mapItem;
              });
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  getObjects() {
    if (!this.objectItems) {
      this.amrappService
        .getAllObjects()
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              this.objectItems = items.map(function (item) {
                let mapItem = new ListItem();
                mapItem.id = item.id;
                mapItem.name = item.name;
                mapItem.isChecked = false;
                return mapItem;
              });
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  getObjectsByTypes(selectedObjectTypes: number[]) {
    this.amrappService
        .getAllObjectsByTypes(selectedObjectTypes)
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              this.objectItems = items.map(function (item) {
                let mapItem = new ListItem();
                mapItem.id = item.id;
                mapItem.name = item.name;
                mapItem.isChecked = false;
                return mapItem;
              });
            }
          },
          error => {
            console.log(error);
          }
        );
  }

  getRoles() {
    if (!this.roleItems) {
      this.amrappService
        .getAllRoles()
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              this.roleItems = items.map(function (item) {
                let mapItem = new ListItem();
                mapItem.id = item.id;
                mapItem.name = item.name;
                mapItem.isChecked = false;
                return mapItem;
              });
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  getDepartments() {
    if (!this.departmentItems) {
      this.amrappService
        .getAllDepartments()
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              this.departmentItems = items.map(function (item) {
                let mapItem = new ListItem();
                mapItem.id = item.id;
                mapItem.name = item.name;
                mapItem.isChecked = false;
                return mapItem;
              });
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  getUsers() {
    if (!this.userItems) {
      this.amrappService
        .getAllUsers()
        .pipe(first())
        .subscribe(
          items => {
            if (items) {
              this.userItems = items.map(function (item) {
                let mapItem = new ListItem();
                mapItem.id = item.id;
                mapItem.name = item.name;
                mapItem.isChecked = false;
                return mapItem;
              });
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  getUsersByObjects(ids: number[]) {
    this.amrappService
      .getUsersByObjects(ids)
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.userItems = items.map(function (item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  getUsersByDepartments(ids: number[]) {
    this.amrappService
      .getUsersByDepartments(ids)
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.userItems = items.map(function (item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  getUsersByRoles(ids: number[]) {
    this.amrappService
      .getUsersByRoles(ids)
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.userItems = items.map(function (item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }
  //#endregion
}