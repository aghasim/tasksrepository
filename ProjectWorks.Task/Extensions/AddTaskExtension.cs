﻿using Microsoft.Extensions.DependencyInjection;
using ProjectWorks.AmrApp.Extensions;
using ProjectWorks.Data.Repository.WorkProjectPatternRepository;
using ProjectWorks.Data.Repository.WorkTaskExecutionOrderRepository;
using ProjectWorks.Data.Repository.WorkTaskHistoryRepository;
using ProjectWorks.Data.Repository.WorkTaskImplementerRepository;
using ProjectWorks.Data.Repository.WorkTaskPatternRepository;
using ProjectWorks.Data.Repository.WorkTaskRepository;
using ProjectWorks.Data.Repository.WorkTaskResourceExpenseRepository;
using ProjectWorks.Data.Repository.WorkTaskResourceRepository;
using ProjectWorks.Data.Repository.WorkTaskReviewerRepository;
using ProjectWorks.Data.Repository.WorkTaskTemplateRepository;
using ProjectWorks.Services.TaskService;
using ProjectWorks.Task.Services;

namespace ProjectWorks.Task.Extensions
{
    public static class AddTaskExtension
    {
        #region Methods

        public static IServiceCollection AddTask(this IServiceCollection services)
        {
            // Register module providers
            services.AddAmrApp();
            // Register module services
            services.AddScoped(typeof(IWorkTaskService), typeof(WorkTaskService));
            services.AddScoped(typeof(IWorkTaskExecutionOrderService), typeof(WorkTaskExecutionOrderService));
            services.AddScoped(typeof(IWorkTaskHistoryService), typeof(WorkTaskHistoryService));
            services.AddScoped(typeof(IWorkTaskResourceService), typeof(WorkTaskResourceService));
            services.AddScoped(typeof(IWorkTaskResourceExpenseService), typeof(WorkTaskResourceExpenseService));
            services.AddScoped(typeof(IWorkTaskTemplateService), typeof(WorkTaskTemplateService));
            services.AddScoped(typeof(IWorkTaskPatternService), typeof(WorkTaskPatternService));
            services.AddScoped(typeof(IWorkProjectPatternService), typeof(WorkProjectPatternService));
            services.AddScoped(typeof(IWorkTaskImplementerService), typeof(WorkTaskImplementerService));
            services.AddScoped(typeof(IWorkTaskReviewerService), typeof(WorkTaskReviewerService));
            // Register module repository
            services.AddScoped(typeof(IWorkTaskRepository), typeof(WorkTaskRepository));
            services.AddScoped(typeof(IWorkTaskExecutionOrderRepository), typeof(WorkTaskExecutionOrderRepository));
            services.AddScoped(typeof(IWorkTaskHistoryRepository), typeof(WorkTaskHistoryRepository));
            services.AddScoped(typeof(IWorkTaskResourceExpenseRepository), typeof(WorkTaskResourceExpenseRepository));
            services.AddScoped(typeof(IWorkTaskResourceRepository), typeof(WorkTaskResourceRepository));
            services.AddScoped(typeof(IWorkTaskTemplateRepository), typeof(WorkTaskTemplateRepository));
            services.AddScoped(typeof(IWorkTaskPatternRepository), typeof(WorkTaskPatternRepository));
            services.AddScoped(typeof(IWorkProjectPatternRepository), typeof(WorkProjectPatternRepository));
            services.AddScoped(typeof(IWorkTaskImplementerRepository), typeof(WorkTaskImplementerRepository));
            services.AddScoped(typeof(IWorkTaskReviewerRepository), typeof(WorkTaskReviewerRepository));

            return services;
        } 

        #endregion
    }
}