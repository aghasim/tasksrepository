﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskReviewerRepository
{
    public class WorkTaskReviewerRepository : GenericRepository<WorkTaskReviewer>, IWorkTaskReviewerRepository
    {
        #region Constructor

        public WorkTaskReviewerRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskReviewer>> GetAll(Guid taskId)
        {
            return await _entities.Include(x => x.WorkTask)
                .Where(reviewer => reviewer.WorkTaskId == taskId && !reviewer.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        } 

        #endregion
    }
}