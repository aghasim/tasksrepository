﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskHistoryRepository
{
    public class WorkTaskHistoryRepository : GenericRepository<WorkTaskHistory>, IWorkTaskHistoryRepository
    {
        #region Methods

        public WorkTaskHistoryRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskHistory>> GetAll(Guid taskId)
        {
            return await _entities
                .Where(x => x.WorkTaskId == taskId && !x.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        }

        public async Task<List<WorkTaskHistory>> GetAllByDate(Guid taskId, DateTime startDate, DateTime endDate)
        {
            return await _entities
                .Where(x => x.WorkTaskId == taskId && x.Created >= startDate && x.Created <= endDate && !x.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        }

        public async Task<List<WorkTaskHistory>> GetAllByDateAndEditor(Guid taskId, int editorId, DateTime startDate, DateTime endDate)
        {
            return await _entities
                .Where(x => x.WorkTaskId == taskId && x.EditorId == editorId && x.Created >= startDate && x.Created <= endDate && !x.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        }

        #endregion
    }
}