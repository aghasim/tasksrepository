﻿using ProjectWorks.AmrApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Intefaces
{
    public interface ITaskDbModel
    {
        Task<ICollection<TaskDbModels>> GetAll(int filialId);
        Task<TaskDbModels> Get(int id, int filialId);
        System.Threading.Tasks.Task Add(TaskDbModels task, int filialId);
        System.Threading.Tasks.Task Delete(int id, int filialId);
    }
}
