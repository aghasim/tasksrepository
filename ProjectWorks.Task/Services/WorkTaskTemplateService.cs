﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkTaskTemplateRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskTemplateService : ServiceBase<WorkTaskTemplate>, IWorkTaskTemplateService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskTemplateRepository _workTaskTemplateRepository;

        #endregion

        #region Constructor

        public WorkTaskTemplateService(ICacheManager cacheManager, IWorkTaskTemplateRepository workTaskTemplateRepository, IUserService userService) :
            base(cacheManager, workTaskTemplateRepository, userService)
        {
            _cacheManager = cacheManager;
            _workTaskTemplateRepository = workTaskTemplateRepository;
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskTemplate>> GetAll(TokenData tokenData)
        {
            var key = string.Format(Constants.CacheConstants.TEMPLATE_ALL_KEY, tokenData.FilialId, tokenData.UserId);
            return await _cacheManager.Get(key, async () => await _workTaskTemplateRepository.GetAll(tokenData));
        }

        #endregion
    }
}