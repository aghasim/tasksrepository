import { Component, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TaskService } from 'src/app/services';
import { TaskFilter, FilterRequest } from 'src/app/models';
import { DEFAULT_GUID, FilterDatePeriodType, FilterTaskType } from 'src/app/common';
import { first } from 'rxjs/operators';

@Component({
  selector: 'task-filter',
  templateUrl: 'task-filter.component.html'
})
export class TaskFilterComponent {
  //#region Fields 
  @Output() onFilterRelease = new EventEmitter<FilterRequest>();
  public filter: TaskFilter;
  private filterDatePeriodType: FilterDatePeriodType;
  private filterTaskType: FilterTaskType;
  public defaultGuid: string = DEFAULT_GUID;
  //#endregion

  //#region Constructor

  public constructor(
    private router: Router,
    private taskService: TaskService) {
    this.filterDatePeriodType = FilterDatePeriodType.Month;
    this.filter = new TaskFilter();
    this.getFilter();
  }

  //#endregion

  //#region Methods

  getFilter() {
    this.taskService
      .getFilter(DEFAULT_GUID, this.filterDatePeriodType)
      .pipe(first())
      .subscribe(
        filter => {
          if (filter) {
            this.filter = filter;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  setFilterDatePeriodType(filterDatePeriodType: FilterDatePeriodType) {
    this.filterDatePeriodType = filterDatePeriodType;
    this.getFilter();
  }

  setFilterTaskType(filterTaskType: FilterTaskType) {
    this.filterTaskType = filterTaskType;
    let filterRequest = new FilterRequest();
    filterRequest.filterDatePeriodType = this.filterDatePeriodType;
    filterRequest.filterTaskType = this.filterTaskType;

    if (this.onFilterRelease.observers.length > 0) {
      this.onFilterRelease.emit(filterRequest);
    }
    else {
      let url = "/user-tasks/" + this.filterDatePeriodType + "/" + this.filterTaskType;
      this.router.navigate([url]);
    }
  }
  //#endregion
}