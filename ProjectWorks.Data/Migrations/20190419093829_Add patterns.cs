﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Addpatterns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WorkProjectPatterns",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    WorkProjectId = table.Column<string>(nullable: true),
                    WorkProjectId1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkProjectPatterns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkProjectPatterns_WorkProjects_WorkProjectId1",
                        column: x => x.WorkProjectId1,
                        principalTable: "WorkProjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkTaskPatterns",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    WorkTaskId = table.Column<string>(nullable: true),
                    WorkTaskId1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskPatterns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkTaskPatterns_WorkTasks_WorkTaskId1",
                        column: x => x.WorkTaskId1,
                        principalTable: "WorkTasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkProjectPatterns_WorkProjectId1",
                table: "WorkProjectPatterns",
                column: "WorkProjectId1");

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskPatterns_WorkTaskId1",
                table: "WorkTaskPatterns",
                column: "WorkTaskId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkProjectPatterns");

            migrationBuilder.DropTable(
                name: "WorkTaskPatterns");
        }
    }
}
