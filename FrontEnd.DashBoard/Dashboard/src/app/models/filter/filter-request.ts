import { FilterDatePeriodType, FilterTaskType } from 'src/app/common';

export class FilterRequest {
  public filterDatePeriodType: FilterDatePeriodType;
  public filterTaskType: FilterTaskType;
}