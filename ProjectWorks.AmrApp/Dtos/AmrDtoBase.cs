﻿namespace ProjectWorks.AmrApp.Dtos
{
    public class AmrDtoBase
    {
        #region Properties

        // Id
        public int Id { get; set; }

        // Название
        public string Name { get; set; }

        #endregion
    }
}