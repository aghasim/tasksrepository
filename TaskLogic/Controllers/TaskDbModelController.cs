﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using ProjectWorks.Data.Extensions;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Contracts.Intefaces;

namespace ProjectWorks.Contracts.Controllers
{
    [Route("api/Contracts/")]
    public class TaskDbmodelController : Controller
    {
        private readonly ITaskDbModel _tasksServices;
       // private readonly AmrServices amrServices;

        public TaskDbmodelController(ITaskDbModel tasksServices)
        {
            _tasksServices = tasksServices;
        
        }


        [HttpGet]
       // [Route("{filialID}")]
        public async Task<IActionResult> GetAll(int filialID)
        {
            try
            {
                var list = await _tasksServices.GetAll(filialID);

                return Ok(list);
            }
            catch (Exception e)
            {

                return BadRequest();
                throw;
            }
        }

        //[HttpGet]
        //////[Route("{id, filialId}")]
        //public async Task<IActionResult> Get(int id, int filialId)
        //{
        //    var model = await _tasksServices.Get(id, filialId);

        //    return this.Success(model);
        //}

        [HttpDelete]
        //[Route ("{id, filialId}")]
        public async Task<IActionResult> Delete (int id,int filialId)
        {
            await _tasksServices.Delete(id, filialId);

            return this.Success();
        }

        
        [HttpPost("Add")]
        //[Route ("{task, filialId}")]
        public async Task<IActionResult> Add (TaskDbModels task, int filialId)
        {
            await _tasksServices.Add(task, filialId);

            return this.Success();
        }
    }
   
}
