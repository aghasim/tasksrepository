﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Контроль закрытия
    /// </summary>
    public enum CloseControlType
    {
        /// <summary>
        ///     Без действия
        /// </summary>
        [Display(Name = "Без действия")]
        NoAction = 1,
        /// <summary>
        ///     Закрытие задачи
        /// </summary>
        [Display(Name = "Закрытие задачи")]
        Closing,
        /// <summary>
        ///     Закрытие с принятием работы
        /// </summary>
        [Display(Name = "Закрытие с принятием работы")]
        ClosingWithJobAcceptance
    }
}