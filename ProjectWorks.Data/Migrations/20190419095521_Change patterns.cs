﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Changepatterns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WorkTaskPatterns",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "WorkTaskPatterns",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "FilialId",
                table: "WorkTaskPatterns",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WorkProjectPatterns",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "WorkProjectPatterns",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "FilialId",
                table: "WorkProjectPatterns",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "WorkTaskPatterns");

            migrationBuilder.DropColumn(
                name: "FilialId",
                table: "WorkTaskPatterns");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "WorkProjectPatterns");

            migrationBuilder.DropColumn(
                name: "FilialId",
                table: "WorkProjectPatterns");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WorkTaskPatterns",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "WorkProjectPatterns",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
