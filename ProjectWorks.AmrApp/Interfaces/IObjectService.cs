﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IObjectService : IAmrService<Object1>
    {
        #region Methods

        Task<IEnumerable<Object1>> GetByObjectType(int filialId, List<int> objectTypes);

        Task<Object1> GetDefaultExpenditure(int filialId);

        Task AddObject(TokenData tokenData, Object1 obj);

        Task<int> GetLastObjectId(TokenData tokenData);

        #endregion
    }
}