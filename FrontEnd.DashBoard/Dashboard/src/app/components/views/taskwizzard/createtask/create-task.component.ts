import { Component, Input } from "@angular/core";
import { Task, ListItem } from "src/app/models";
import { AmrappService } from 'src/app/services';
import { first } from 'rxjs/operators';
import { DEFAULT_GUID } from 'src/app/common';

@Component({
  selector: "create-task",
  templateUrl: "create-task.component.html",
  styleUrls: ["create-task.component.css"],
})
export class CreateTaskComponent {
  //#region Fields

  @Input() task: Task;
  public responsibles: ListItem[];
  public title: string;

  //#endregion

  //#region Constructor

  public constructor(private amrappService: AmrappService) {
    this.getUsers();
  }

  //#endregion

  //#region Methods

  ngOnInit() {
    this.title = this.task.id == DEFAULT_GUID ? "Создать новую задачу" : "Редактировать задачу";
  }

  getUsers() {
    this.amrappService
      .getAllUsers()
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.responsibles = items.map(function (item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  onFileChange(fileInput: any) {
    this.task.filesToUpload = <Array<File>>fileInput.target.files;
  }

  //#endregion
}
