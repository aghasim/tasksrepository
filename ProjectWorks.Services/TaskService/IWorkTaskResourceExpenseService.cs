﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkTaskResourceExpenseService : IServiceBase<WorkTaskResourceExpense>
    {
        #region Methods

        Task<List<WorkTaskResourceExpense>> GetAll(Guid resourceId);

        #endregion
    }
}