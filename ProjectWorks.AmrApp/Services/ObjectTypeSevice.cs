﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using Serilog;
using Constants = ProjectWorks.Core.Common.Constants;

namespace ProjectWorks.AmrApp.Services
{
    public class ObjectTypeSevice : IObjectTypeSevice
    {
        #region Fields

        private readonly IAmrAppRepository _amrApp;

        private readonly ICacheManager _cacheManager;

        #endregion

        #region Constructor

        public ObjectTypeSevice(IAmrAppRepository amrApp,
            ICacheManager cacheManager)
        {
            _amrApp = amrApp;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public async Task<ObjectTypes> Get(int filialId, int id)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.OBJECTTYPE_CURRENT_KEY, filialId, id);
                Log.Information(string.Format(Constants.LogConstants.OBJECTTYPESERVICE_GETOBJECTTYPE, filialId, id));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.ObjectTypes
                        .FirstOrDefaultAsync(x => x.Id == id));
            }

            return null;
        }

        public async Task<IEnumerable<ObjectTypes>> GetByFilialId(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.OBJECTTYPE_ALL_KEY, filialId);
                Log.Information(string.Format(Constants.LogConstants.OBJECTTYPESERVICE_GETOBJECTTYPES, filialId));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.ObjectTypes.ToListAsync());
            }

            return null;
        }

        #endregion
    }
}