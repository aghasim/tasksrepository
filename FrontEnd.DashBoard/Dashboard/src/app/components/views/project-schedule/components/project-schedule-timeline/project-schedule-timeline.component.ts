import * as moment from 'moment';
import { Component, Input } from '@angular/core';
import { Task } from 'src/app/models';

@Component({
    templateUrl: 'project-schedule-timeline.component.html',
})

export class ProjectScheduleTimeLineComponent {
    //#region Fields 

    @Input() tasks: Task[];

    //#endregion

    //#region Constructor

    public constructor() {
    }

    //#endregion

    //#region Methods

    //#endregion
}