import { Component, OnInit } from "@angular/core";

import { Task } from 'src/app/models';
import { TaskWizzardStep, TaskAppointedTarget, ImplementationMonitoringType } from 'src/app/common';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { TaskService } from 'src/app/services';

@Component({
  selector: 'task-edit-wizard',
  templateUrl: 'task-edit-wizard.component.html',
  styleUrls: ["task-edit-wizard.component.css"],
})
export class TaskEditWizzardComponent implements OnInit {
  //#region Fields 

  private task: Task;
  public wizzardStep: TaskWizzardStep;
  public isLastStep: boolean;

  //#endregion

  //#region Constructor 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService
  ) {
    this.route.paramMap.subscribe(params => {
      let editTaskId = params.get("taskId").toString();
      this.getTask(editTaskId);
    });
  }

  //#endregion

  //#region Methods 

  ngOnInit() {
    this.task = new Task();
    this.wizzardStep = TaskWizzardStep.Main;
  }

  onSave() {
    // https://stackoverflow.com/questions/14359566/how-to-pass-a-datetime-parameter
    this.task.startDate = moment(this.task.startDate).format('YYYY-MM-DDTHH:mm:ss');
    this.task.completionDate = moment(this.task.completionDate).format('YYYY-MM-DDTHH:mm:ss');
    this.taskService
      .update(this.task)
      .pipe(first())
      .subscribe(
        result => {
          if (result) {
            this.router.navigate(["/project/" + this.task.projectId + "/tasks"]);
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  onForward() {
    let step = this.wizzardStep + 1;
    if (step in TaskWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = step + 1 in TaskWizzardStep ? false : true;
  }

  onBackward() {
    let step = this.wizzardStep - 1;
    if (step in TaskWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = false;
  }

  getTask(projectId: string) {
    if (!this.task) {
      this.taskService
        .get(projectId)
        .pipe(first())
        .subscribe(
          item => {
            if (item) {
              this.task = item;
              this.task.startDate = moment(this.task.startDate).format('YYYY-MM-DD');
              this.task.completionDate = moment(this.task.completionDate).format('YYYY-MM-DD');
              this.task.appointedTarget = TaskAppointedTarget.User;
              this.task.implementationMonitoringType = ImplementationMonitoringType.SelectedUsers;
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  //#endregion
}