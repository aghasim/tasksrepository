import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";

import { TaskHistory } from "../../models";
import { environment } from "../../../environments/environment";

@Injectable({ providedIn: "root" })
export class TaskHistoryService {
    private apiUrl: string = environment.apiUrl + "taskhistories/";

    constructor(private http: HttpClient) { }

    get(taskHistoryId: string) {
        return this.http
            .get<any>(this.apiUrl + taskHistoryId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getAll(taskId: string) {
        return this.http
            .get<any>(this.apiUrl + "all/" + taskId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    add(history: TaskHistory) {
        return this.http
            .post<any>(this.apiUrl + "add", { history })
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }
}
