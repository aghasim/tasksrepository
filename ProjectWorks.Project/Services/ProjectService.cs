﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Project;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Helpers.Enum;
using ProjectWorks.Core.Models.Timeline;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkProjectRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.ProjectService;
using Constants = ProjectWorks.Core.Common.Constants;

namespace ProjectWorks.Project.Services
{
    public class ProjectService : ServiceBase<WorkProject>, IWorkProjectService
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly ICacheManager _cacheManager;

        private readonly IWorkProjectRepository _workProjectRepository;

        private readonly IUserService _userService;

        private readonly IObjectService _objectService;

        private readonly IPropertyValueService _propertyValueService;

        private readonly IExpenditureService _expenditureService;

        private readonly IWorkProjectHistoryService _workProjectHistoryService;

        #endregion

        #region Constructor

        public ProjectService(
            IMapper mapper,
            ICacheManager cacheManager, 
            IWorkProjectRepository workProjectRepository,
            IUserService userService,
            IObjectService objectService,
            IPropertyValueService propertyValueService,
            IExpenditureService expenditureService,
            IWorkProjectHistoryService workProjectHistoryService) :
            base(cacheManager, workProjectRepository, userService)
        {
            _mapper = mapper;
            _cacheManager = cacheManager;
            _workProjectRepository = workProjectRepository;
            _userService = userService;
            _objectService = objectService;
            _propertyValueService = propertyValueService;
            _expenditureService = expenditureService;
            _workProjectHistoryService = workProjectHistoryService;
        }

        #endregion

        #region Methods

        public async Task<WorkProject> Get(TokenData tokenData, Guid id)
        {
            var item = await Get(id);
            var user = await _userService.Get(tokenData.FilialId, tokenData.UserId);
            item.ResponsibleName = user.UserFio;

            return item;
        }

        public async Task Add(TokenData tokenData, WorkProject entity)
        {
            if (!entity.WorkProjectResource.ExpenditureId.HasValue)
            {
                var defaultExpenditure = await _objectService.GetDefaultExpenditure(tokenData.FilialId);
                if (defaultExpenditure != null)
                {
                    var obj = new Object1
                    {
                        AppUserId = tokenData.UserId,
                        ObjectName = $"Проект '{entity.Name}'",
                        ObjectTypeId = Constants.A2Constants.ObjTypeСosts,
                        NotDel = true,
                        FilialId = tokenData.FilialId,
                        NomGrp = false,
                        ParentId = defaultExpenditure?.Id ?? 0
                    };

                    await _objectService.AddObject(tokenData, obj);

                    var expenditureId = await _objectService.GetLastObjectId(tokenData);
                    var userRole = _userService.GetRoleNameByUserId(tokenData.FilialId, tokenData.UserId);

                    entity.WorkProjectResource.ExpenditureId = expenditureId;

                    //Лимит
                    var limit = new PropertyValues
                    {
                        Object1Id = expenditureId,
                        PropertyId = Constants.A2Constants.CostLimit,
                        Value = entity.WorkProjectResource.Count.ToString(CultureInfo.InvariantCulture),
                        ValueInt = (long) entity.WorkProjectResource.Count
                    };

                    await _propertyValueService.AddPropertyValue(tokenData, limit);

                    //Согласование
                    var approve = new PropertyValues
                    {
                        Object1Id = expenditureId,
                        PropertyId = Constants.A2Constants.CostApproveRoles,
                        Value = $"{userRole}",
                        ValObjSet = $"{tokenData.UserId}"
                    };

                    await _propertyValueService.AddPropertyValue(tokenData, approve);

                    //Согласование статьи 
                    var approveCost = new PropertyValues
                    {
                        Object1Id = expenditureId,
                        PropertyId = Constants.A2Constants.CostApproveCostsRoles,
                        Value = $"{userRole}",
                        ValObjSet = $"{tokenData.UserId}"
                    };

                    await _propertyValueService.AddPropertyValue(tokenData, approveCost);

                    //Кто отчитываеться
                    var report = new PropertyValues
                    {
                        Object1Id = expenditureId,
                        PropertyId = Constants.A2Constants.CostReportRoles,
                        Value = $"{userRole}",
                        ValObjSet = $"{tokenData.UserId}"
                    };

                    await _propertyValueService.AddPropertyValue(tokenData, report);

                    var property = await _propertyValueService.GetDefaultProperties(tokenData.FilialId);

                    var type = new PropertyValues
                    {
                        Object1Id = expenditureId,
                        PropertyId = property?.Id ?? 0,
                        Value = "Статья",
                        PropertyValParamId = 30
                    };

                    await _propertyValueService.AddPropertyValue(tokenData, type);

                    // auto expenditure approve in a2
                    await _expenditureService.ExpenditureApprove(tokenData, expenditureId);
                }
            }

            Add(entity);

            var history = new WorkProjectHistory
            {
                Description = $"Project {entity.Id} created",
                Status = entity.Status,
                EditorId = tokenData.UserId,
                HistoryOperationType = HistoryOperationType.Create,
                WorkProject = entity
            };

            _workProjectHistoryService.Add(history);
        }

        public void Update(TokenData tokenData, WorkProject entity)
        {
            Update(entity);

            var history = new WorkProjectHistory
            {
                Description = $"Project {entity.Id} updated",
                Status = entity.Status,
                EditorId = tokenData.UserId,
                HistoryOperationType = HistoryOperationType.Create,
                WorkProject = entity
            };

            _workProjectHistoryService.Add(history);
        }

        public async Task Delete(TokenData tokenData, Guid id)
        {
            ClearCache(tokenData.FilialId, tokenData.UserId);
            await base.Delete(id);
        }

        public new void Add(WorkProject entity)
        {
            ClearCache(entity.FilialId, entity.CreatorId);
            base.Add(entity);
        }

        public new void Update(WorkProject entity)
        {
            ClearCache(entity.FilialId, entity.CreatorId);
            base.Update(entity);
        }

        public async Task<List<WorkProjectDto>> GetAll(TokenData tokenData)
        {
            var key = string.Format(Constants.CacheConstants.PROJECT_ALL_KEY, tokenData.FilialId, tokenData.UserId);
            var projects = await _cacheManager.Get(key, async () => await _workProjectRepository.GetAll(tokenData));
            var dtos = _mapper.Map<IList<WorkProjectDto>>(projects);

            foreach (var project in projects)
            {
                var projectTaskExpense = project.WorkTasks?.Sum(x =>
                    x.WorkTaskResource?.WorkTaskResourceExpenses?.Sum(expense => expense.Count));
                var projectDto = dtos.FirstOrDefault(dto => dto.Id == project.Id);
                if (projectDto != null)
                    projectDto.ProjectResourceExpense = 
                        projectTaskExpense ?? default(decimal);
            }

            foreach (var dto in dtos)
            {
                var user = await _userService.Get(tokenData.FilialId, dto.ResponsibleId);
                if (user != null)
                    dto.ResponsibleName = user.UserFio;
            }

            return dtos.ToList();
        }

        public async Task<List<WorkProjectDto>> GetAllByMonth(TokenData tokenData, int year, int month)
        {
            var startDate = new DateTime(year, month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);

            var key = string.Format(Constants.CacheConstants.PROJECT_ALL_BY_MONTH_KEY, tokenData.FilialId, year, month);
            var projects = await _cacheManager.Get(key,
                async () => await _workProjectRepository.GetAllByDates(tokenData, startDate, endDate));
            var dtos = _mapper.Map<IList<WorkProjectDto>>(projects);

            foreach (var project in projects)
            {
                var projectTaskExpense = project.WorkTasks?.Sum(x =>
                    x.WorkTaskResource?.WorkTaskResourceExpenses?.Sum(expense => expense.Count));
                var projectDto = dtos.FirstOrDefault(dto => dto.Id == project.Id);
                if (projectDto != null)
                    projectDto.ProjectResourceExpense =
                        projectTaskExpense ?? default(decimal);
            }

            foreach (var dto in dtos)
            {
                var user = await _userService.Get(tokenData.FilialId, dto.ResponsibleId);
                if (user != null)
                    dto.ResponsibleName = user.UserFio;

                dto.Timeline = await CalculateTimeline(dto.Id, startDate, endDate);
            }

            return dtos.ToList();
        }

        public async Task UpdateStatus(TokenData tokenData, Guid id)
        {
            var project = await _workProjectRepository.Get(id);
            if (project != null)
            {
                switch (project.Status)
                {
                    case WorkProjectStatus.New:
                        if (project.WorkTasks.Any(x => x.Status != WorkTaskStatus.New))
                        {
                            project.Status = WorkProjectStatus.InProgress;
                            await _workProjectRepository.Save();

                            _workProjectHistoryService.Add(
                                new WorkProjectHistory
                                {
                                    EditorId = tokenData.UserId,
                                    HistoryOperationType = HistoryOperationType.StatusСhange,
                                    Status = project.Status,
                                    Description = $"Project {project.Id} status update.",
                                    WorkProject = project
                                });
                        }
                        break;
                    case WorkProjectStatus.InProgress:
                        if (project.WorkTasks.All(x => x.Status == WorkTaskStatus.Done))
                        {
                            project.Status = WorkProjectStatus.Done;
                            await _workProjectRepository.Save();

                            _workProjectHistoryService.Add(
                                new WorkProjectHistory
                                {
                                    EditorId = tokenData.UserId,
                                    HistoryOperationType = HistoryOperationType.StatusСhange,
                                    Status = project.Status,
                                    Description = $"Project {project.Id} status update.",
                                    WorkProject = project
                                });
                        }
                        break;
                }
            }
        }

        public void ClearCache(int filialId, int creatorId)
        {
            var keys = new List<string>
            {
                string.Format(Constants.CacheConstants.PROJECT_ALL_KEY, filialId, creatorId),
                string.Format(Constants.CacheConstants.EXPENDITURE_ALL_KEY, filialId)
            };

            foreach (var key in keys)
                _cacheManager.Remove(key);
        }

        private async Task<List<ProjectTimeline>> CalculateTimeline(Guid projectId, DateTime startDate, DateTime endDate)
        {
            var dayNumber = 1;
            var itemStatus = WorkProjectStatus.None;
            var days = new List<DateTime>();
            for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
                days.Add(dt);

            var result = new List<ProjectTimeline>();
            var histories = await _workProjectHistoryService.GetAllByDate(projectId, startDate, endDate);
            foreach (var day in days)
            {
                var history = histories.FirstOrDefault(x => x.Created.Date == day);
                if (history != null)
                {
                    itemStatus = history.Status;
                    result.Add(
                        new ProjectTimeline
                        {
                            Day = dayNumber,
                            Status = history.Status,
                            StatusName = history.Status.DisplayName(),
                        });
                }
                else
                {
                    result.Add(
                        new ProjectTimeline
                        {
                            Day = dayNumber,
                            Status = itemStatus,
                            StatusName = itemStatus.DisplayName(),
                        });
                }

                dayNumber++;
            }

            var statuses = result.Select(x => x.Status).Distinct();
            foreach (var status in statuses)
            {
                var items = result.Where(x => x.Status == status);
                foreach (var item in items)
                    item.Duration = status != WorkProjectStatus.None ? items.Count() : 1;
            }

            return GetDistinct(result);
        }

        private List<ProjectTimeline> GetDistinct(IEnumerable<ProjectTimeline> items)
        {
            var result = new List<ProjectTimeline>();
            ProjectTimeline current = null;

            foreach (var item in items)
            {
                if (current == null)
                {
                    current = item;
                    result.Add(current);
                    continue;
                }

                if (item.Status == WorkProjectStatus.None || item.Status != current.Status)
                {
                    current = item;
                    result.Add(current);
                }
            }

            return result;
        }

        #endregion
    }
}