import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaskService, AuthenticationService } from "../../../services";
import { first } from 'rxjs/operators';
import { Task } from 'src/app/models';
import * as moment from 'moment';

@Component({
    templateUrl: 'task.component.html',
})

export class TaskComponent {
    //#region Constructor

    public taskId: string;
    public task: Task;
    public isImplementer: boolean;
    public isReviewer: boolean;

    public constructor(
        private authenticationService: AuthenticationService,
        private taskService: TaskService,
        private route: ActivatedRoute) {
        this.route.paramMap.subscribe(params => {
            this.taskId = params.get("id");
            if (this.taskId)
                this.getTask();
        });
    }

    //#endregion

    //#region Methods

    getTask() {
        this.taskService
            .get(this.taskId)
            .pipe(first())
            .subscribe(
                task => {
                    task.startDate = moment(task.startDate).format('DD.MM.YYYY');
                    task.completionDate = moment(task.completionDate).format('DD.MM.YYYY');
                    this.task = task;
                    var userId = this.authenticationService.currentUserValue.id;
                    this.isImplementer = this.task.implementers.some(function (value){
                        return userId == value;
                    });
                    this.isReviewer = this.task.reviewers.some(function (value){
                        return userId == value;
                    });
                },
                error => {
                    console.log(error);
                }
            )
    }
    //#endregion
}