﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Dtos.Common;
using ProjectWorks.Core.Helpers.Enum;
using ProjectWorks.Services.CommonService;

namespace ProjectWorks.Common.Services
{
    public class CommonService : ICommonService
    {
        #region Methods

        public IEnumerable<CommonDto> GetTaskExecutionOrderTypes()
        {
            var types = Enum.GetValues(typeof(WorkTaskExecutionOrderType)).Cast<WorkTaskExecutionOrderType>();
            return types.Select(type => new CommonDto
                {
                    Id = (int) type,
                    Name = type.DisplayName()
                })
                .ToList();
        }

        public IEnumerable<CommonDto> GetTaskResourceTypes()
        {
            var types = Enum.GetValues(typeof(WorkTaskResourceType)).Cast<WorkTaskResourceType>();
            return types.Select(type => new CommonDto
                {
                    Id = (int)type,
                    Name = type.DisplayName()
                })
                .ToList();
        }

        public IEnumerable<CommonDto> GetMonths()
        {
            var types = Enum.GetValues(typeof(Months)).Cast<Months>();
            return types.Select(type => new CommonDto
                {
                    Id = (int)type,
                    Name = type.DisplayName()
                })
                .ToList();
        }

        public IEnumerable<CommonDto> GetTaskStatusTypes()
        {
            var types = Enum.GetValues(typeof(WorkTaskStatus)).Cast<WorkTaskStatus>();
            return types.Select(type => new CommonDto
                {
                    Id = (int)type,
                    Name = type.DisplayName()
                })
                .ToList();
        }

        #endregion
    }
}