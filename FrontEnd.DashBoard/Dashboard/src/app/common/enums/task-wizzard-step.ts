export enum TaskWizzardStep {
  Main = 1,
  ImplementationMonitoring,
  Resources,
  Responsible,

  TaskExecutionOrder
}