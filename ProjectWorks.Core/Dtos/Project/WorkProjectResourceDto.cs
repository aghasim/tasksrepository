﻿using System;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Dtos.Project
{
    public class WorkProjectResourceDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Наименование ресурса
        public string TypeName{ get; set; }

        // Тип ресурса
        public WorkTaskResourceType Type { get; set; }

        // Количество ресурса на выполнение
        public decimal Count { get; set; }

        // Статья расхода из а2
        public int? ExpenditureId { get; set; }

        #endregion
    }
}