﻿using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Шаблон задачи
    /// </summary>
    public class WorkTaskTemplate : FilialEntityBase
    {
        #region Properties

        // Ответственный, пользователь из А2
        public int? ResponsibleId { get; set; }

        // Тип ресурса
        public WorkTaskResourceType? WorkTaskResourceType { get; set; }

        // Количество ресурса на выполнение
        [Range(0, double.MaxValue)]
        public decimal? ResourceCount { get; set; }

        // Тип порядка исполнения задачи
        public WorkTaskExecutionOrderType WorkTaskExecutionOrderType { get; set; }

        // Порядок исполнения задачи
        [Range(0, int.MaxValue)]
        public int? ExecutionOrder { get; set; }

        #endregion
    }
}