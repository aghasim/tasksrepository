﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkProjectRepository
{
    public interface IWorkProjectRepository : IGenericRepository<WorkProject>
    {
        #region Methods

        Task<List<WorkProject>> GetAll(TokenData tokenData);

        Task<List<WorkProject>> GetAllByDates(TokenData tokenData, DateTime start, DateTime end);

        #endregion
    }
}