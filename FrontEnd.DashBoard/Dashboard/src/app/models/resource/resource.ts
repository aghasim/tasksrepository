import { ModelBase } from '../model.base';
import { ResourceType } from 'src/app/common';

export class Resource extends ModelBase {
    public count: number;
    public type: ResourceType;
    public typeName: string;
    public expenditureId: number;
}
