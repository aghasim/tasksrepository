﻿using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IAmrAppRepository
    {
        #region Properties

        aspnetsmrContext Context { get; set; }

        #endregion

        #region Methods

        void SetConnString(int filialId);

        #endregion
    }
}