﻿using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IDepartmentService : IAmrService<AppUserDepartments>
    {
    }
}