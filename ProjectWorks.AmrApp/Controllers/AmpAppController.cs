﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.AmrApp.DtoModels;
using ProjectWorks.AmrApp.Dtos;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Mapping;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Filters;
using ProjectWorks.Data.Extensions;

namespace ProjectWorks.AmrApp.Controllers
{
    [Authorize]
    [Route("api/ampapp")]
    public class AmpAppController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IUserService _userService;

        private readonly IObjectService _objectService;

        private readonly IObjectTypeSevice _objectTypeService;

        private readonly IDepartmentService _departmentService;

        private readonly IRoleService _roleService;

        private readonly IExpenditureService _expenditureService;

        #endregion

        #region Constructor

        public AmpAppController(
            IMapper mapper, 
            IUserService userService, 
            IObjectService objectService,
            IObjectTypeSevice objectTypeService,
            IDepartmentService departmentService,
            IRoleService roleService,
            IExpenditureService expenditureService)
        {
            _mapper = mapper;
            _userService = userService;
            _objectService = objectService;
            _objectTypeService = objectTypeService;
            _departmentService = departmentService;
            _roleService = roleService;
            _expenditureService = expenditureService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("users/{id}")]
        public async Task<IActionResult> GetUser(TokenData tokenData, int id)
        {
            var item = await _userService.Get(tokenData.FilialId, id);
            if (item != null)
            {
                var dto = _mapper.Map<AppUsers, AppUsersDto>(item, AmpMapper.ConfigureMap);
                return this.Success(dto);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("objects/{id}")]
        public async Task<IActionResult> GetObject(TokenData tokenData, int id)
        {
            var item = await _objectService.Get(tokenData.FilialId, id);
            if (item != null)
            {
                var dto = _mapper.Map<Object1, ObjectDto>(item, AmpMapper.ConfigureMap);
                return this.Success(dto);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("objecttypes/{id}")]
        public async Task<IActionResult> GetObjectType(TokenData tokenData, int id)
        {
            var item = await _objectTypeService.Get(tokenData.FilialId, id);
            if (item != null)
            {
                var dto = _mapper.Map<ObjectTypes, ObjectTypesDto>(item, AmpMapper.ConfigureMap);
                return this.Success(dto);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("departments/{id}")]
        public async Task<IActionResult> GetDepartment(TokenData tokenData, int id)
        {
            var item = await _departmentService.Get(tokenData.FilialId, id);
            if (item != null)
            {
                var dto = _mapper.Map<AppUserDepartments, AppUserDepartmentsDto>(item, AmpMapper.ConfigureMap);
                return this.Success(dto);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("roles/{id}")]
        public async Task<IActionResult> GetRole(TokenData tokenData, int id)
        {
            var item = await _roleService.Get(tokenData.FilialId, id);
            if (item != null)
            {
                var dto = _mapper.Map<AppFuncRoles, AppUserRolesDto>(item, AmpMapper.ConfigureMap);
                return this.Success(dto);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("users")]
        public async Task<IActionResult> GetAllUsers(TokenData tokenData)
        {
            var items = await _userService.GetByFilialId(tokenData.FilialId);
            if (items != null)
            {
                var dtos = items.Select(
                    item => new AppUsersDto {Id = item.Id, Name = item.UserFio });
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [Consumes("application/json")]
        [HttpPost("usersbyobjects")]
        public async Task<IActionResult> GetAllUsersByObjects(TokenData tokenData, [FromBody] RequestModel model)
        {
            var items = await _userService.GetAllUsersByObjects(tokenData.FilialId, model.Ids);
            if (items != null)
            {
                var dtos = items.Select(item => new ObjectDto { Id = item.Id, Name = item.UserFio });
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [Consumes("application/json")]
        [HttpPost("usersbydepartments")]
        public async Task<IActionResult> GetAllUsersByDepartments(TokenData tokenData, [FromBody] RequestModel model)
        {
            var items = await _userService.GetAllUsersByDepartments(tokenData.FilialId, model.Ids);
            if (items != null)
            {
                var dtos = items.Select(item => new ObjectDto { Id = item.Id, Name = item.UserFio });
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [Consumes("application/json")]
        [HttpPost("usersbyroles")]
        public async Task<IActionResult> GetAllUsersByRoles(TokenData tokenData, [FromBody] RequestModel model)
        {
            var items = await _userService.GetAllUsersByRoles(tokenData.FilialId, model.Ids);
            if (items != null)
            {
                var dtos = items.Select(item => new ObjectDto { Id = item.Id, Name = item.UserFio });
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("objects")]
        public async Task<IActionResult> GetAllObjects(TokenData tokenData)
        {
            var items = await _objectService.GetByFilialId(tokenData.FilialId);
            if (items != null)
            {
                var dtos = items.Select(item => new ObjectDto {Id = item.Id, Name = item.ObjectName});
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [Consumes("application/json")]
        [HttpPost("objects")]
        public async Task<IActionResult> GetAllObjectsByTypes(TokenData tokenData, [FromBody] RequestModel model)
        {
            var items = await _objectService.GetByObjectType(tokenData.FilialId, model.Ids);
            if (items != null)
            {
                var dtos = items.Select(item => new ObjectDto { Id = item.Id, Name = item.ObjectName });
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("objecttypes")]
        public async Task<IActionResult> GetAllObjectTypes(TokenData tokenData)
        {
            var items = await _objectTypeService.GetByFilialId(tokenData.FilialId);
            if (items != null)
            {
                var dtos = items.Select(item => new ObjectTypesDto {Id = item.Id, Name = item.Name});
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("departments")]
        public async Task<IActionResult> GetAllDepartments(TokenData tokenData)
        {
            var items = await _departmentService.GetByFilialId(tokenData.FilialId);
            if (items != null)
            {
                var dtos = items.Select(item => new AppUserDepartmentsDto {Id = item.Id, Name = item.Name});
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("roles")]
        public async Task<IActionResult> GetAllRoles(TokenData tokenData)
        {
            var items = await _roleService.GetByFilialId(tokenData.FilialId);
            if (items != null)
            {
                var dtos = items.Select(item => new AppUserRolesDto {Id = item.Id, Name = item.Name});
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        [FilialAction]
        [HttpGet("expenditures")]
        public async Task<IActionResult> GetAllExpenditure(TokenData tokenData)
        {
            var items = await _expenditureService.GetByFilialId(tokenData.FilialId, tokenData.UserId);
            if (items != null)
            {
                var dtos = items.Select(item => new ObjectDto { Id = item.Id, Name = item.ObjectName });
                return this.Success(dtos);
            }

            return this.Fail(null);
        }

        #endregion
    }
}