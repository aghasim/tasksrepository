﻿using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Core.Models.Response
{
    public class WorkTaskResponse
    {
        #region Properties

        public WorkTaskDto Task { get; set; }

        #endregion
    }
}