﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using ProjectWorks.Core.Dtos.User;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Token.Helpers;
using ProjectWorks.Token.Services;

namespace ProjectWorks.Token.Controllers
{
    [Route("api/tokens")]
    [Authorize]
    public class TokenController : Controller
    {
        #region Fields

        private readonly TokenService _tokenService;

        #endregion

        #region Constructor

        public TokenController(TokenService tokenService)
        {
            _tokenService = tokenService;
        }

        #endregion

        #region Methods

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] UserDto model)
        {
            var user = await _tokenService.Authenticate(model.UserId, model.FilialId);
            if (user == null)
                return this.AccessDenied();

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = JwtHelper.GetSecurityKey();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Sid, user.AppUserDepartment.FilialId.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(7),
                SigningCredentials =
                    new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return this.Success(
                new
                {
                    user.Id,
                    user.UserName,
                    user.AppUserDepartment.FilialId,
                    Token = tokenString
                });
        }

        #endregion
    }
}
