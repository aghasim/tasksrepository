﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskReviewerRepository
{
    public interface IWorkTaskReviewerRepository : IGenericRepository<WorkTaskReviewer>
    {
        #region Methods

        Task<List<WorkTaskReviewer>> GetAll(Guid taskId);

        #endregion
    }
}