﻿using System;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Models.Timeline
{
    public class TaskTimeline
    {
        #region Properties

        public int Day { get; set; }

        public int Duration { get; set; }

        public WorkTaskStatus Status { get; set; }

        public string StatusName { get; set; }

        #endregion
    }
}