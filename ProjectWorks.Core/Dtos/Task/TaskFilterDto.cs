﻿namespace ProjectWorks.Core.Dtos.Task
{
    public class TaskFilterDto
    {
        #region Properties

        public int ControlingCount { get; set; }

        public int IncomingCount { get; set; }

        public int OutgoingCount { get; set; }

        public int ActiveControlingCount { get; set; }

        public int ActiveIncomingCount { get; set; }

        public int ActiveOutgoingCount { get; set; }

        public int NewIncomingCount { get; set; }

        public int NewOutgoingCount { get; set; }

        public int TomorrowCount { get; set; }

        public int TodayCount { get; set; }

        public int PastCount { get; set; }

        #endregion
    }
}