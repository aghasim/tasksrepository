﻿using Microsoft.AspNetCore.Mvc;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Contracts.Intefaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Controllers
{
    [Route("api/Contracts/Discussion")]
    public class TaskDiscussionController:Controller
    {
        private readonly ITaskDiscussion _taskService;

        public TaskDiscussionController(ITaskDiscussion service)
        {
            _taskService = service;
        }
        
        [HttpGet]
        public async Task<ICollection<TaskDiscussionOfTheTasks>> GetAll(int filialId)
        {
            return await _taskService.GetAll(filialId);
        }

        //[HttpGet]
        //public async Task<TaskDiscussionOfTheTasks> Get (int id, int filialId)
        //{
        //    return await _taskService.Get(id, filialId);
        //}

        [HttpPost("Add")]
        public async System.Threading.Tasks.Task Add(TaskDiscussionOfTheTasks item, int filialId)
        {
            await _taskService.Add(item, filialId);
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task Delete(int id, int filialId)
        {
            await _taskService.Delete(id, filialId);
        }
    }
}
