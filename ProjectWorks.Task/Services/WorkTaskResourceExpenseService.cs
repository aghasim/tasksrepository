﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkTaskResourceExpenseRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskResourceExpenseService : ServiceBase<WorkTaskResourceExpense>, IWorkTaskResourceExpenseService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskResourceExpenseRepository _workTaskResourceExpenseRepository;

        #endregion

        #region Constructor

        public WorkTaskResourceExpenseService(ICacheManager cacheManager, IWorkTaskResourceExpenseRepository workTaskResourceExpenseRepository, IUserService userService) :
            base(cacheManager, workTaskResourceExpenseRepository, userService)
        {
            _cacheManager = cacheManager;
            _workTaskResourceExpenseRepository = workTaskResourceExpenseRepository;
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskResourceExpense>> GetAll(Guid resourceId)
        {
            var key = string.Format(Constants.CacheConstants.TASKRESOURCEEXPENSE_ALL_KEY, resourceId);
            return await _cacheManager.Get(key, async () => await _workTaskResourceExpenseRepository.GetAll(resourceId));
        }

        #endregion
    }
}