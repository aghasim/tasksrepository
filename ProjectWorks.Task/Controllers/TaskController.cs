﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Filters;
using ProjectWorks.Core.Helpers.Task;
using ProjectWorks.Core.Models.Response;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;
using ProjectWorks.Services.ProjectService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/tasks")]
    public class TasksController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IHostingEnvironment _appEnvironment;

        private readonly IWorkTaskService _taskService;

        private readonly IWorkProjectService _projectService;

        private readonly IWorkTaskHistoryService _historyService;

        #endregion

        #region Constructor

        public TasksController(
            IMapper mapper,
            IHostingEnvironment appEnvironment,
            IWorkTaskService taskService,
            IWorkProjectService projectService,
            IWorkTaskHistoryService historyService)
        {
            _mapper = mapper;
            _appEnvironment = appEnvironment;
            _taskService = taskService;
            _projectService = projectService;
            _historyService = historyService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var dto = await _taskService.GetDto(id);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpGet("all/{id}")]
        public async Task<IActionResult> GetAll(TokenData tokenData, Guid id)
        {
            try
            {
                var result = new List<WorkTaskDto>();
                var dtos = await _taskService.GetAll(tokenData, id);
                TaskHelper.GetTaskTree(dtos, result, default(Guid));
                return this.Success(result);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpGet("allbymonth/{id}/{year}/{month}")]
        public async Task<IActionResult> GetAllByMonth(TokenData tokenData, Guid id, int year, int month)
        {
            try
            {
                var result = new List<WorkTaskDto>();
                var dtos = await _taskService.GetAllByMonth(tokenData, id, year, month);
                TaskHelper.GetTaskTree(dtos, result, default(Guid));
                return this.Success(result);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpGet("allbyfilter/{filterDatePeriodType}/{filterTaskType}")]
        public async Task<IActionResult> GetAllByFilter(
            TokenData tokenData,
            FilterDatePeriodType filterDatePeriodType,
            FilterTaskType filterTaskType)
        {
            try
            {
                var dtos = await _taskService.GetAllByFilter(tokenData, filterDatePeriodType, filterTaskType);
                return this.Success(dtos);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpGet("filter/{id}/{periodType}")]
        public async Task<IActionResult> GetFilter(
            TokenData tokenData,
            Guid id,
            FilterDatePeriodType periodType = FilterDatePeriodType.Month)
        {
            try
            {
                var dto = await _taskService.GetFilter(tokenData, id, periodType);
                return this.Success(dto);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpGet("active/{id}")]
        public async Task<IActionResult> GetAllActive(TokenData tokenData, Guid id)
        {
            try
            {
                var result = new List<WorkTaskDto>();
                var dtos = await _taskService.GetAllActive(tokenData, id);
                TaskHelper.GetTaskTree(dtos, result, default(Guid));
                return this.Success(result);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpGet("incoming/{id}")]
        public async Task<IActionResult> GetAllIncoming(TokenData tokenData, Guid id)
        {
            try
            {
                var result = new List<WorkTaskDto>();
                var dtos = await _taskService.GetAllIncoming(tokenData, id);
                TaskHelper.GetTaskTree(dtos, result, default(Guid));
                return this.Success(result);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpGet("outgoing/{id}")]
        public async Task<IActionResult> GetAllOutgoing(TokenData tokenData, Guid id)
        {
            try
            {
                var result = new List<WorkTaskDto>();
                var dtos = await _taskService.GetAllOutgoing(tokenData, id);
                TaskHelper.GetTaskTree(dtos, result, default(Guid));
                return this.Success(result);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpPost("add")]
        public async Task<IActionResult> Add(TokenData tokenData, [FromBody] WorkTaskResponse response)
        {
            try
            {
                var item = _mapper.Map<WorkTask>(response.Task);

                item.CreatorId = tokenData.UserId;
                item.FilialId = tokenData.FilialId;
                item.Status = WorkTaskStatus.New;
                item.WorkProject = await _projectService.Get(item.WorkProjectId);
                item.WorkTaskReviewers = new List<WorkTaskReviewer>();
                item.WorkTaskImplementers = new List<WorkTaskImplementer>();

                var reviewers = response.Task.Reviewers;
                foreach (var reviewer in reviewers)
                {
                    item.WorkTaskReviewers.Add(
                        new WorkTaskReviewer
                        {
                            ReviewerId = reviewer,
                            WorkTask = item
                        });
                }

                var implementers = response.Task.Implementers;
                foreach (var implementer in implementers)
                {
                    item.WorkTaskImplementers.Add(
                        new WorkTaskImplementer
                        {
                            Status = WorkTaskStatus.New,
                            ImplementerId = implementer,
                            WorkTask = item
                        });
                }

                _taskService.Add(tokenData, item);
                _historyService.Add(
                    new WorkTaskHistory
                    {
                        EditorId = tokenData.UserId,
                        HistoryOperationType = HistoryOperationType.Create,
                        Status = item.Status,
                        Description = $"Task {item.Id} create.",
                        WorkTask = item
                    });

                return this.Success(item.Id);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpPut("update")]
        public async Task<IActionResult> Update(TokenData tokenData, [FromBody] WorkTaskResponse response)
        {
            var item = await _taskService.Get(response.Task.Id);
            if (item != null)
            {
                item.Name = response.Task.Name;
                item.Description = response.Task.Description;
                item.ResponsibleId = response.Task.ResponsibleId;
                item.StartDate = response.Task.StartDate;
                item.CompletionDate = response.Task.CompletionDate;
                item.CloseControlType = response.Task.CloseControlType;
                item.SettingCommunicationType = response.Task.CommunicationSettingsType;
                item.WorkTaskExecutionOrder.WorkTaskExecutionOrderType =
                    response.Task.ExecutionOrder.ExecutionOrderType;
                item.WorkTaskExecutionOrder.ExecutionOrder =
                    response.Task.ExecutionOrder.ExecutionOrder;
                item.WorkTaskReviewers = new List<WorkTaskReviewer>();
                item.WorkTaskImplementers = new List<WorkTaskImplementer>();
                item.WorkTaskResource.Type = response.Task.TaskResource.Type;
                item.WorkTaskResource.Count = response.Task.TaskResource.Count;
                item.WorkTaskResource.ExpenditureId = response.Task.TaskResource.ExpenditureId;

                var reviewers = response.Task.Reviewers;
                foreach (var reviewer in reviewers)
                {
                    item.WorkTaskReviewers.Add(
                        new WorkTaskReviewer
                        {
                            ReviewerId = reviewer,
                            WorkTask = item
                        });
                }

                var implementers = response.Task.Implementers;
                foreach (var implementer in implementers)
                {
                    item.WorkTaskImplementers.Add(
                        new WorkTaskImplementer
                        {
                            Status = item.Status,
                            ImplementerId = implementer,
                            WorkTask = item
                        });
                }

                _taskService.Update(tokenData, item);
                _historyService.Add(
                    new WorkTaskHistory
                    {
                        EditorId = tokenData.UserId,
                        HistoryOperationType = HistoryOperationType.Update,
                        Status = item.Status,
                        Description = $"Task {item.Id} update.",
                        WorkTask = item
                    });

                return this.Success();
            }

            return this.Error($"Task {response.Task.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(TokenData tokenData, Guid id)
        {
            var item = await _taskService.Get(id);
            if (item != null)
            {
                await _taskService.Delete(tokenData, item);
                return this.Success();
            }

            return this.Error($"Task {id} not found!");
        }

        [FilialAction]
        [HttpGet("updatestatus/{id}")]
        public async Task<IActionResult> UpdateStatus(TokenData tokenData, Guid id)
        {
            try
            {
                await _taskService.UpdateStatus(tokenData, id);
                return this.Success();
            }
            catch (Exception)
            {
                return this.Error($"Task {id} not found!");
            }
        }

        [FilialAction]
        [HttpPost("upload/{id}")]
        public async Task<IActionResult> AddFile(Guid id)
        {
            try
            {
                var httpRequest = HttpContext.Request;
                if (httpRequest.Form.Files.Count > 0)
                {
                    foreach (var file in httpRequest.Form.Files)
                    {
                        var dirPath =
                            $"{_appEnvironment.WebRootPath}{Constants.EnvironmentConstants.DefaultFilePath}\\{id}";
                        var path = $"{dirPath}\\{file.FileName}";
                        if (!Directory.Exists(dirPath))
                            Directory.CreateDirectory(dirPath);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                        }
                    }
                }

                return this.Success();
            }
            catch (Exception)
            {
                return this.Error($"Task {id} upload file error!");
            }
        }
    }

    #endregion
}