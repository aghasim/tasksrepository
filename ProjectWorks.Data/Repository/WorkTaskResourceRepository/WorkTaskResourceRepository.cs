﻿using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskResourceRepository
{
    public class WorkTaskResourceRepository : GenericRepository<WorkTaskResource>, IWorkTaskResourceRepository
    {
        #region Constructor

        public WorkTaskResourceRepository(IDataContext context) : base(context)
        {
        }

        #endregion
    }
}