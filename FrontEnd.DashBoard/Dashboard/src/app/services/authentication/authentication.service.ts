import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { User } from '../../models';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private apiUrl: string = environment.apiUrl;
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(userId: number, filialId: number) {
        return this.http.post<any>(this.apiUrl + `tokens/authenticate`, { userId, filialId })
            .pipe(map(result => {
                if (result.success && result.data.token) {
                    localStorage.setItem('currentUser', JSON.stringify(result.data));
                    this.currentUserSubject.next(result.data);
                }
                return result.data;
            }))
            .pipe(catchError((err, caught) => {
                localStorage.removeItem('currentUser');
                this.currentUserSubject.next(null);
                return this.currentUserSubject;
            }))
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}