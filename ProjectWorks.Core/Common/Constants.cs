﻿namespace ProjectWorks.Core.Common
{
    public static class Constants
    {
        public class EnvironmentConstants
        {
            public const string DefaultFilePath = "\\Files";
        }

        public class A2Constants
        {
            // Расходы (int)Config.ObjType.Сosts
            public const int ObjTypeСosts = 7;

            // Наименование статьи расходов по умолчанию
            public const string DefaultExpenditureName = "Проектные работы";

            // Тип расхода по привязке (int)CostType.byUser
            public const int CostTypeByUser = 0;

            // ID Свойства - лимит (int)Config.ObjProperty.costLimit
            public const int CostLimit = 50;

            // ID Свойства - Какие роли могут утверждать расход (int)Config.ObjProperty.costApproveRoles
            public const int CostApproveRoles = 53;

            // ID Свойства - Какие роли могут отчитываться по расходу (int)Config.ObjProperty.costReportRoles
            public const int CostReportRoles = 54;

            // ID Свойства - Какие роли могут утверждать расход (int)Config.ObjProperty.costApproveCostsRoles
            public const int CostApproveCostsRoles = 55;
        }

        public class CacheConstants
        {
            public const int DefaultCacheTime = 60; // minutes

            /// <summary>
            /// Ключ кеширования текущий AppUser
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string APPUSER_CURRENT_KEY = "projectworks.appuser.current-{0}-{1}";

            /// <summary>
            /// Ключ кеширования пользователи филиала
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string APPUSER_ALL_KEY = "projectworks.appuser.all-{0}";

            /// <summary>
            /// Ключ кеширования пользователи филиала
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : Ids hash
            /// </remarks>
            public const string APPUSER_ALL_BY_DEPARTMENT_KEY = "projectworks.appuser.all.by.department-{0}-{1}";

            /// <summary>
            /// Ключ кеширования пользователи филиала
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : Ids hash
            /// </remarks>
            public const string APPUSER_ALL_BY_OBJECTS_KEY = "projectworks.appuser.all.by.objects-{0}-{1}";

            /// <summary>
            /// Ключ кеширования пользователи филиала
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : Ids hash
            /// </remarks>
            public const string APPUSER_ALL_BY_ROLES_KEY = "projectworks.appuser.all.by.roles-{0}-{1}";

            /// <summary>
            /// Ключ кеширования пользователи филиала
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : Role id
            /// </remarks>
            public const string GET_APPUSER_BY_ROLE_ID_KEY = "projectworks.appuser.all.by.roleid-{0}-{1}";

            /// <summary>
            /// Ключ кеширования текущий Object
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string OBJECT_CURRENT_KEY = "projectworks.object.current-{0}-{1}";

            /// <summary>
            /// Ключ кеширования Object филиала
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string OBJECT_ALL_KEY = "projectworks.object.all-{0}";

            /// <summary>
            /// Ключ кеширования Object филиала
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string DEFAULT_EXPENDITURE_KEY = "projectworks.default.expenditure-{0}";

            /// <summary>
            /// Ключ кеширования текущий ObjectType
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string OBJECTTYPE_CURRENT_KEY = "projectworks.objecttype.current-{0}-{1}";

            /// <summary>
            /// Ключ кеширования ObjectTypes
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string OBJECTTYPE_ALL_KEY = "projectworks.objecttype.all-{0}";

            /// <summary>
            /// Ключ кеширования текущий Department
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string DEPARTMENT_CURRENT_KEY = "projectworks.department.current-{0}-{1}";

            /// <summary>
            /// Ключ кеширования текущий Expenditure
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string EXPENDITURE_CURRENT_KEY = "projectworks.expenditure.current-{0}-{1}";

            /// <summary>
            /// Ключ кеширования текущий PropertyValues
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : PropertyValue ID
            /// </remarks>
            public const string PROPERTYVALUE_CURRENT_KEY = "projectworks.propertyvalues.current-{0}-{1}";

            /// <summary>
            /// Ключ кеширования текущий Property
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string PROPERTY_DEFAULT_KEY = "projectworks.property.default-{0}";

            /// <summary>
            /// Ключ кеширования Departments
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string DEPARTMENT_ALL_KEY = "projectworks.department.all-{0}";

            /// <summary>
            /// Ключ кеширования EXPENDITURES
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string EXPENDITURE_ALL_KEY = "projectworks.expenditure.all-{0}";

            /// <summary>
            /// Ключ кеширования текущий PropertyValues
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : Object ID
            /// </remarks>
            public const string PROPERTYVALUE_ALL_KEY = "projectworks.propertyvalues.all-{0}-{1}";

            /// <summary>
            /// Ключ кеширования текущий Role
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string ROLE_CURRENT_KEY = "projectworks.role.current-{0}-{1}";

            /// <summary>
            /// Ключ кеширования Roles
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// </remarks>
            public const string ROLE_ALL_KEY = "projectworks.role.all-{0}";

            /// <summary>
            /// Ключ кеширования задача ID
            /// </summary>
            /// <remarks>
            /// {0} : Task ID
            /// </remarks>
            public const string TASK_BYID_KEY = "projectworks.task.byid-{0}";

            /// <summary>
            /// Ключ кеширования проект ID
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// </remarks>
            public const string PROJECT_BYID_KEY = "projectworks.project.byid-{0}";

            /// <summary>
            /// Ключ кеширования проекты
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string PROJECT_ALL_KEY = "projectworks.project.all-{0}-{1}";

            /// <summary>
            /// Ключ кеширования проекты
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : Year
            /// {2} : Month
            /// </remarks>
            public const string PROJECT_ALL_BY_MONTH_KEY = "projectworks.project.all-{0}-{1}-{2}";

            /// <summary>
            /// Ключ кеширования проекты
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string TEMPLATE_ALL_KEY = "projectworks.template.all-{0}-{1}";

            /// <summary>
            /// Ключ кеширования фильтр
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// {1} : User ID
            /// {2} : Filial ID
            /// </remarks>
            public const string TASK_FILTER_KEY = "projectworks.task.filter-{0}-{1}-{2}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// </remarks>
            public const string TASK_ALL_KEY = "projectworks.task.all-{0}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// {1} : Year
            /// {2} : Month
            /// </remarks>
            public const string TASK_ALL_BY_MONTH_KEY = "projectworks.task.all.by.month-{0}-{1}-{2}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : User ID
            /// {1} : Filial ID
            /// {2} : FilterDatePeriodType
            /// {3} : FilterTaskType
            /// </remarks>
            public const string TASK_ALL_BY_FILTER_KEY = "projectworks.task.allbyfilter-{0}-{1}-{2}-{3}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : Task ID
            /// </remarks>
            public const string REVIEWER_ALL_KEY = "projectworks.reviewer.all-{0}";

            /// <summary>
            /// Ключ кеширования сообщения
            /// </summary>
            /// <remarks>
            /// {0} : Task ID
            /// </remarks>
            public const string CONVERSATION_ALL_KEY = "projectworks.сonversation.all-{0}";

            /// <summary>
            /// Ключ кеширования актуальных задач
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// {1} : User ID
            /// </remarks>
            public const string TASK_ALL_ACTIVE_KEY = "projectworks.task.all.actual-{0}-{1}";

            /// <summary>
            /// Ключ кеширования входящие задач
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// {1} : User ID
            /// {2} : Filial ID
            /// </remarks>
            public const string TASK_ALL_INCOMING_KEY = "projectworks.task.all.incoming-{0}-{1}-{2}";

            /// <summary>
            /// Ключ кеширования исходящих задач
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// {1} : User ID
            /// {2} : Filial ID
            /// </remarks>
            public const string TASK_ALL_OUTGOING_KEY = "projectworks.task.all.outgoing-{0}-{1}-{2}";

            /// <summary>
            /// Ключ кеширования исходящих задач
            /// </summary>
            /// <remarks>
            /// {0} : Project ID
            /// {1} : User ID
            /// {2} : Filial ID
            /// </remarks>
            public const string TASK_ALL_CONTROLING_KEY = "projectworks.task.all.controling-{0}-{1}-{2}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : Task ID
            /// </remarks>
            public const string TASKHISTORY_ALL_KEY = "projectworks.taskhistory.all-{0}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : Task ID
            /// {1} : User ID
            /// </remarks>
            public const string IMPLEMENTER_KEY = "projectworks.implementer-{0}-{1}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : Task ID
            /// </remarks>
            public const string IMPLEMENTER_ALL_KEY = "projectworks.implementer.all-{0}";

            /// <summary>
            /// Ключ кеширования задачи
            /// </summary>
            /// <remarks>
            /// {0} : ResourceExpense ID
            /// </remarks>
            public const string TASKRESOURCEEXPENSE_ALL_KEY = "projectworks.taskresourceexpense.all-{0}";

            /// <summary>
            /// Ключ кеширования задачи ServiceBase.Get
            /// </summary>
            /// <remarks>
            /// {0} : TEntity
            /// {1} : Id
            /// </remarks>
            public const string SERVICEBASE_GET_KEY = "projectworks.servicebase.{0}.get.{1}";
        }

        public class LogConstants
        {
            /// <summary>
            /// Log user authenticate
            /// </summary>
            /// <remarks>
            /// {0} : Filial ID
            /// {1} : User ID
            /// </remarks>
            public const string TOKENSERVICE_AUTHENTICATE = "TokenService.Authenticate(filialId: {0}, userId: {1})";

            /// <summary>
            /// Log get
            /// </summary>
            /// <remarks>
            /// {0} : TEntity
            /// {1} : Id
            /// </remarks>
            public const string SERVICEBASE_GET = "ServiceBase<{0}>.Get(Id: {1})";

            /// <summary>
            /// Log add
            /// </summary>
            /// <remarks>
            /// {0} : TEntity
            /// {1} : Id
            /// </remarks>
            public const string SERVICEBASE_ADD = "ServiceBase<{0}>.ADD(Id: {1})";

            /// <summary>
            /// Log update
            /// </summary>
            /// <remarks>
            /// {0} : TEntity
            /// {1} : Id
            /// </remarks>
            public const string SERVICEBASE_UPDATE = "ServiceBase<{0}>.Update(Id: {1})";

            /// <summary>
            /// Log delete
            /// </summary>
            /// <remarks>
            /// {0} : TEntity
            /// {1} : Id
            /// </remarks>
            public const string SERVICEBASE_DELETE = "ServiceBase<{0}>.Delete(Id: {1})";

            /// <summary>
            /// Log UserService.Get
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// {1} : UserId
            /// </remarks>
            public const string USERSERVICE_GETUSER = "UserService.Get(FilialId: {0}, UserId: {1})";

            /// <summary>
            /// Log UserService.GetByFilialId
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string USERSERVICE_GETUSERS = "UserService.GetByFilialId(FilialId: {0})";

            /// <summary>
            /// Log ObjectService.Get
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// {1} : UserId
            /// </remarks>
            public const string OBJECTSERVICE_GETOBJECT = "ObjectService.Get(FilialId: {0}, UserId: {1})";

            /// <summary>
            /// Log ObjectService.GetByFilialId
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string OBJECTSERVICE_GETOBJECTS = "ObjectService.GetByFilialId(FilialId: {0})";

            /// <summary>
            /// Log ObjectTypeService.Get
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// {1} : UserId
            /// </remarks>
            public const string OBJECTTYPESERVICE_GETOBJECTTYPE = "ObjectTypeService.Get(FilialId: {0}, UserId: {1})";

            /// <summary>
            /// Log ObjectTypeService.GetByFilialId
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string OBJECTTYPESERVICE_GETOBJECTTYPES = "ObjectTypeService.GetByFilialId(FilialId: {0})";

            /// <summary>
            /// Log DepartmentService.Get
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// {1} : UserId
            /// </remarks>
            public const string DEPARTMENTSERVICE_GETDEPARTMENT = "DepartmentService.Get(FilialId: {0}, UserId: {1})";

            /// <summary>
            /// Log DepartmentService.GetByFilialId
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string DEPARTMENTSERVICE_GETDEPARTMENTS = "DepartmentService.GetByFilialId(FilialId: {0})";

            /// <summary>
            /// Log ExpenditureService.Get
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// {1} : UserId
            /// </remarks>
            public const string EXPENDITURESERVICE_GETEXPENDITURE = "ExpenditureService.Get(FilialId: {0}, UserId: {1})";

            /// <summary>
            /// Log ExpenditureService.GetByFilialId
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string EXPENDITURESERVICE_GETEXPENDITURES = "ExpenditureService.GetByFilialId(FilialId: {0})";

            /// <summary>
            /// Log ExpenditureService.AddExpenditure
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// {1} : ExpenditureId
            /// </remarks>
            public const string EXPENDITURESERVICE_ADDEXPENDITURES = "ExpenditureService.AddExpenditure(FilialId: {0}, ExpenditureId: {1})";

            /// <summary>
            /// Log RoleService.Get
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// {1} : UserId
            /// </remarks>
            public const string ROLESERVICE_GETROLE = "RoleService.Get(FilialId: {0}, UserId: {1})";

            /// <summary>
            /// Log RoleService.GetByFilialId
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string ROLESERVICE_GETROLES = "RoleService.GetByFilialId(FilialId: {0})";

            /// <summary>
            /// Log ObjectService.AddObject
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string OBJECTSERVICE_ADDOBJECT = "ObjectService.AddObject(FilialId: {0})";

            /// <summary>
            /// Log PropertyValueService.AddPropertyValue
            /// </summary>
            /// <remarks>
            /// {0} : FilialId
            /// </remarks>
            public const string PROPERTYVALUESERVICE_ADDOBJECT = "PropertyValueService.AddPropertyValue(FilialId: {0})";
        }
    }
}