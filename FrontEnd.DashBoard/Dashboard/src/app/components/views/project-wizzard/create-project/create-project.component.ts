import { Component, Input, OnInit } from "@angular/core";
import { ListItem, Project } from "src/app/models";
import { AmrappService } from 'src/app/services';
import { first } from 'rxjs/operators';
import { DEFAULT_GUID } from 'src/app/common';

@Component({
  selector: "create-project",
  templateUrl: "create-project.component.html",
  styleUrls: ["create-project.component.css"],
})
export class CreateProjectComponent implements OnInit {
  //#region Fields

  @Input() project: Project;
  public responsibles: ListItem[];
  public title: string;

  //#endregion

  //#region Constructor

  public constructor(private amrappService: AmrappService) {
    this.getUsers();
  }

  //#endregion

  //#region Methods

  ngOnInit() {
    this.title = this.project.id == DEFAULT_GUID ? "Создать новый проект" : "Редактировать проект";
  }

  getUsers() {
    this.amrappService
      .getAllUsers()
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.responsibles = items.map(function (item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  //#endregion
}
