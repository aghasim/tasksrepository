﻿using ProjectWorks.AmrApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Intefaces
{
    public interface ITaskUserTask
    {
        Task<ICollection<TaskUserTasks>> GetAll(int filialId);
        Task<TaskUserTasks> Get(int id, int filialId);
        System.Threading.Tasks.Task Add(TaskUserTasks task, int filialId);
        System.Threading.Tasks.Task Delete(int id, int filialId);
    }
}
