﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Filters;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/executionorders")]
    public class TaskExecutionOrderController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskExecutionOrderService _taskExecutionOrderService;

        #endregion

        #region Constructor

        public TaskExecutionOrderController(IMapper mapper, IWorkTaskExecutionOrderService taskExecutionOrderService)
        {
            _mapper = mapper;
            _taskExecutionOrderService = taskExecutionOrderService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var item = await _taskExecutionOrderService.Get(id);
            var dto = _mapper.Map<WorkTaskExecutionOrderDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpPost("add")]
        public IActionResult Add([FromBody] WorkTaskExecutionOrderDto dto)
        {
            var item = _mapper.Map<WorkTaskExecutionOrder>(dto);
            _taskExecutionOrderService.Add(item);
            return this.Success();
        }

        [FilialAction]
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] WorkTaskExecutionOrderDto dto)
        {
            var item = await _taskExecutionOrderService.Get(dto.Id);
            if (item != null)
            {
                item.ExecutionOrder = dto.ExecutionOrder;
                item.WorkTaskExecutionOrderType = dto.ExecutionOrderType;

                _taskExecutionOrderService.Update(item);
                return this.Success();
            }

            return this.Error($"Task execution order resource {dto.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _taskExecutionOrderService.Delete(id);
            return this.Success();
        }

        #endregion
    }
}