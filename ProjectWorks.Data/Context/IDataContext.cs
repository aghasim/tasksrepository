﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ProjectWorks.Core.Domain;

namespace ProjectWorks.Data.Context
{
    public interface IDataContext
    {
        #region Methods

        DbSet<TEntity> Set<TEntity>() where TEntity : EntityBase;

        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : EntityBase;

        Task<int> SaveChangesAsync();

        #endregion
    }
}