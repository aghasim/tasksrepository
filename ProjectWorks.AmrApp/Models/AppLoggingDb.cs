﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectWorks.AmrApp.Models
{
    /// <summary>
    /// Лог обращения к таблицам БД
    /// </summary>
    public class AppLoggingDBs
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key]
        public long ID { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [Display(Name = "Дата")]
        public DateTime DateID { get; set; } = DateTime.Now;

        /// <summary>
        /// Пользователь
        /// </summary>
        [Display(Name = "Пользователь")]
        public int? UserID { get; set; }

        /// <summary>
        /// Таблица
        /// </summary>
        [Display(Name = "Таблица")]
        [StringLength(32)]
        public string Table { get; set; }

        /// <summary>
        /// Комманда
        /// </summary>
        [Display(Name = "Комманда")]
        [StringLength(32)]
        public string Command { get; set; }

        /// <summary>
        /// ID строки
        /// </summary>
        [Display(Name = "ID строки")]
        public long RowID { get; set; }

        /// <summary>
        /// Ок
        /// </summary>
        [Display(Name = "Ок")]
        public bool OK { get; set; }
    }
}
