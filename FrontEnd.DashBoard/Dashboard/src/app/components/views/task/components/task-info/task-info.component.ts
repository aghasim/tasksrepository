import { Component, Input } from '@angular/core';
import { first } from 'rxjs/operators';
import { Task, Reviewer } from 'src/app/models';
import { ReviewerService, TaskService } from 'src/app/services';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FilterDatePeriodType, FilterTaskType } from 'src/app/common';
import { Router } from '@angular/router';

@Component({
    selector: 'task-info',
    templateUrl: 'task-info.component.html',
})

export class TaskInfoComponent {
    //#region Fields 

    private _task: Task;
    public reviewers: Reviewer[];

    get task(): Task {
        return this._task;
    }

    @Input()
    set task(task: Task) {
        this._task = task;
        if (task)
            this.getReviewers(task.id);
    }

    //#endregion

    //#region Constructor

    public constructor(
        private router: Router,
        private reviewerService: ReviewerService,
        private taskService: TaskService,
        public ngxSmartModalService: NgxSmartModalService) { }

    //#endregion

    //#region Methods

    getReviewers(taskId: string) {
        this.reviewerService
            .getAll(taskId)
            .pipe(first())
            .subscribe(
                reviewers => {
                    this.reviewers = reviewers;
                },
                error => {
                    console.log(error);
                }
            )
    }

    removeTask(taskId: string) {
        this.taskService
            .delete(taskId)
            .pipe(first())
            .subscribe(
                result => {
                    if (result) {
                        let url = "/user-tasks/" + FilterDatePeriodType.Month + "/" + FilterTaskType.Incoming;
                        this.router.navigate([url]);
                    }
                },
                error => {
                    console.log(error);
                }
            )
    }

    //#endregion
}