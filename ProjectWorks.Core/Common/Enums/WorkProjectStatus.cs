﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Статус проекта
    /// </summary>
    public enum WorkProjectStatus
    {
        /// <summary>
        ///     Новый
        /// </summary>
        [Display(Name = "Новый")]
        New = 1,

        /// <summary>
        ///     В работе
        /// </summary>
        [Display(Name = "В работе")]
        InProgress = 2,

        /// <summary>
        ///     Выполнен
        /// </summary>
        [Display(Name = "Выполнен")]
        Done = 3,

        /// <summary>
        ///     Нет
        /// </summary>
        [Display(Name = "Нет")]
        None = 7
    }
}