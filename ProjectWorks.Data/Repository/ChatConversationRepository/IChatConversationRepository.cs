﻿using ProjectWorks.Core.Domain.Task;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectWorks.Data.Repository.ChatConversationRepository
{
    public interface IChatConversationRepository: IGenericRepository<Conversation>
    {
        Task Add(Conversation conversation);

        Task<IEnumerable<Conversation>> GetAll(Guid taskId);
    }
}
