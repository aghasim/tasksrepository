﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Helpers.Cache;
using ProjectWorks.Core.Helpers.Date;
using ProjectWorks.Core.Helpers.Enum;
using ProjectWorks.Core.Helpers.Task;
using ProjectWorks.Core.Models.Timeline;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.ChatConversationRepository;
using ProjectWorks.Data.Repository.WorkTaskRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.ProjectService;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskService : ServiceBase<WorkTask>, IWorkTaskService
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskRepository _workTaskRepository;

        private readonly IUserService _userService;

        private readonly IChatConversationRepository _chatConversationRepository;

        private readonly IWorkProjectService _workProjectService;

        private readonly IWorkTaskHistoryService _workTaskHistoryService;

        #endregion

        #region Constructor

        public WorkTaskService(
            IMapper mapper,
            ICacheManager cacheManager, 
            IWorkTaskRepository workTaskRepository,
            IUserService userService,
            IChatConversationRepository chatConversationRepository,
            IWorkProjectService workProjectService,
            IWorkTaskHistoryService workTaskHistoryService) :
            base(cacheManager, workTaskRepository, userService)
        {
            _mapper = mapper;
            _cacheManager = cacheManager;
            _workTaskRepository = workTaskRepository;
            _userService = userService;
            _chatConversationRepository = chatConversationRepository;
            _workProjectService = workProjectService;
            _workTaskHistoryService = workTaskHistoryService;
        }

        #endregion

        #region Methods

        public async Task<WorkTaskDto> GetDto(Guid id)
        {
            var task = await Get(id);
            var dto = _mapper.Map<WorkTaskDto>(task);
            var user = await _userService.Get(task.FilialId, task.CreatorId);
            var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
            var childTasksCompleted = await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
            dto.ResponsibleName = user?.UserFio;
            dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;
            dto.TaskCount = childTasks;
            dto.TaskCompletedCount = childTasksCompleted;

            return dto;
        }

        public void Add(TokenData tokenData, WorkTask entity)
        {
            CacheHelper.ClearCache(_cacheManager, tokenData, entity.WorkProjectId, entity.Id);
            Add(entity);
        }

        public void Update(TokenData tokenData, WorkTask entity)
        {
            CacheHelper.ClearCache(_cacheManager, tokenData, entity.WorkProjectId, entity.Id);
            Update(entity);
        }

        public async System.Threading.Tasks.Task Delete(TokenData tokenData, WorkTask entity)
        {
            CacheHelper.ClearCache(_cacheManager, tokenData, entity.WorkProjectId, entity.Id);
            await Delete(entity.Id);
        }

        public async Task<TaskFilterDto> GetFilter(TokenData tokenData, Guid projectId, FilterDatePeriodType periodType)
        {
            var datePeriod = DateHelper.GetDatePeriod(periodType);
            var allIncoming = await GetAllIncoming(tokenData, projectId);
            if (datePeriod != null)
                allIncoming = allIncoming?.Where(x => x.StartDate >= datePeriod.Start && x.StartDate <= datePeriod.End).ToList();
            var allIncomingResult = new List<WorkTaskDto>();
            TaskHelper.FilterByExecutionOrder(allIncoming, allIncomingResult, default(Guid));
            var allActiveIncoming = allIncomingResult.Count(x => x.Status != WorkTaskStatus.Done || x.Status != WorkTaskStatus.Completed);
            var allNewIncoming = allIncomingResult.Count(x => x.Status == WorkTaskStatus.New);

            var allOutgoing = await GetAllOutgoing(tokenData, projectId);
            if (datePeriod != null)
                allOutgoing = allOutgoing?.Where(x => x.StartDate >= datePeriod.Start && x.StartDate <= datePeriod.End).ToList();
            var allOutgoingResult = new List<WorkTaskDto>();
            TaskHelper.FilterByExecutionOrder(allOutgoing, allOutgoingResult, default(Guid));
            var allActiveOutgoing = allOutgoingResult.Count(x => x.Status != WorkTaskStatus.Done || x.Status != WorkTaskStatus.Completed);
            var allNewOutgoing = allOutgoingResult.Count(x => x.Status == WorkTaskStatus.New);

            var allControling = await GetAllControling(tokenData, projectId);
            if (datePeriod != null)
                allControling = allControling?.Where(x => x.StartDate >= datePeriod.Start && x.StartDate <= datePeriod.End).ToList();
            var allControlingResult = new List<WorkTaskDto>();
            TaskHelper.FilterByExecutionOrder(allControling, allControlingResult, default(Guid));
            var allActiveControling = allControlingResult.Count(x => x.Status != WorkTaskStatus.Done || x.Status != WorkTaskStatus.Completed);
            var allToday = allIncomingResult.Count(x => x.CompletionDate.Date == DateTime.UtcNow.Date);
            var allTomorrow = allIncomingResult.Count(x => x.CompletionDate.Date == DateTime.UtcNow.Date.AddDays(1));
            var allPast = allIncomingResult.Count(x => x.CompletionDate.Date < DateTime.UtcNow.Date);
            allPast += allOutgoingResult.Count(x => x.CompletionDate.Date < DateTime.UtcNow.Date);

            return new TaskFilterDto
            {
                ControlingCount = allControlingResult.Count,
                IncomingCount = allIncomingResult.Count,
                OutgoingCount = allOutgoingResult.Count,
                ActiveControlingCount = allActiveControling,
                ActiveIncomingCount = allActiveIncoming,
                ActiveOutgoingCount = allActiveOutgoing,
                NewIncomingCount = allNewIncoming,
                NewOutgoingCount = allNewOutgoing,
                TodayCount = allToday,
                TomorrowCount = allTomorrow,
                PastCount = allPast
            };
        }

        public async Task<List<WorkTaskDto>> GetAll(TokenData tokenData, Guid projectId)
        {
            var key = string.Format(Constants.CacheConstants.TASK_ALL_KEY, projectId);
            var tasks = await _cacheManager.Get(key, async () => await _workTaskRepository.GetAll(projectId));
            var dtos = _mapper.Map<IList<WorkTaskDto>>(tasks);

            foreach (var task in tasks)
            {
                var taskTaskExpense = task.WorkTaskResource.WorkTaskResourceExpenses.Any()
                    ? task.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count)
                    : 0;
                var taskDto = dtos.FirstOrDefault(dto => dto.Id == task.Id);
                if (taskDto != null)
                    taskDto.TaskResourceExpense = taskTaskExpense;
            }

            foreach (var dto in dtos)
            {
                var task = tasks.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(task.FilialId, dto.ResponsibleId);
                dto.ResponsibleName = user?.UserFio;
                dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;

                if (projectId != Guid.Empty)
                {
                    var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
                    var childTasksCompleted =
                        await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
                    dto.TaskCount = childTasks;
                    dto.TaskCompletedCount = childTasksCompleted;
                }
            }

            return dtos.ToList();
        }

        public async Task<List<WorkTaskDto>> GetAllByMonth(TokenData tokenData, Guid projectId, int year, int month)
        {
            var startDate = new DateTime(year, month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);

            var key = string.Format(Constants.CacheConstants.TASK_ALL_BY_MONTH_KEY, projectId, year, month);
            var tasks = await _cacheManager.Get(key,
                async () => await _workTaskRepository.GetAllByDates(projectId, startDate, endDate));
            var dtos = _mapper.Map<IList<WorkTaskDto>>(tasks);

            foreach (var task in tasks)
            {
                var taskTaskExpense = task.WorkTaskResource.WorkTaskResourceExpenses.Any()
                    ? task.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count)
                    : 0;
                var taskDto = dtos.FirstOrDefault(dto => dto.Id == task.Id);
                if (taskDto != null)
                    taskDto.TaskResourceExpense = taskTaskExpense;
            }

            foreach (var dto in dtos)
            {
                var task = tasks.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(task.FilialId, dto.ResponsibleId);
                dto.ResponsibleName = user?.UserFio;
                dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;

                if (projectId != Guid.Empty)
                {
                    var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
                    var childTasksCompleted =
                        await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
                    dto.TaskCount = childTasks;
                    dto.TaskCompletedCount = childTasksCompleted;
                }

                dto.Timeline = await CalculateTimeline(dto.Id, startDate, endDate);

                var implementers = dto.Implementers;
                foreach (var implementer in implementers)
                {
                    var timeline = 
                        await CalculateUserTimeline(tokenData.FilialId, implementer, dto.Id, startDate, endDate);
                    dto.UserTimeline.Add(timeline);
                }
            }

            return dtos.ToList();
        }

        public async Task<List<WorkTaskDto>> GetAllByFilter(
            TokenData tokenData, 
            FilterDatePeriodType filterDatePeriodType = FilterDatePeriodType.Month,
            FilterTaskType filterTaskType = FilterTaskType.Incoming)
        {
            var key = string.Format(Constants.CacheConstants.TASK_ALL_BY_FILTER_KEY, 
                tokenData.UserId, tokenData.FilialId, filterDatePeriodType, filterTaskType);
            var tasks = await _cacheManager.Get(key,
                async () => await _workTaskRepository.GetAllByFilter(tokenData, filterDatePeriodType, filterTaskType));
            var dtos = _mapper.Map<IList<WorkTaskDto>>(tasks);
            var result = new List<WorkTaskDto>();
            TaskHelper.FilterByExecutionOrder(dtos.ToList(), result, default(Guid));

            foreach (var task in tasks)
            {
                var taskTaskExpense = task.WorkTaskResource.WorkTaskResourceExpenses.Any()
                    ? task.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count)
                    : 0;
                var taskDto = result.FirstOrDefault(dto => dto.Id == task.Id);
                if (taskDto != null)
                    taskDto.TaskResourceExpense = taskTaskExpense;
            }

            foreach (var dto in result)
            {
                var task = tasks.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(task.FilialId, dto.ResponsibleId);
                var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
                var childTasksCompleted = await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
                dto.ResponsibleName = user?.UserFio;
                dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;
                dto.TaskCount = childTasks;
                dto.TaskCompletedCount = childTasksCompleted;
                var messages = await _chatConversationRepository.GetAll(dto.Id);
                dto.MessagesCount = messages?.Count() ?? 0;
            }

            return result;
        }

        public async Task<List<WorkTaskDto>> GetAllActive(TokenData tokenData, Guid projectId)
        {
            var key = string.Format(Constants.CacheConstants.TASK_ALL_ACTIVE_KEY, projectId, tokenData.UserId);
            var tasks = await _cacheManager.Get(key,
                async () => await _workTaskRepository.GetAllActive(tokenData, projectId));
            var dtos = _mapper.Map<IList<WorkTaskDto>>(tasks);
            var result = new List<WorkTaskDto>();
            TaskHelper.FilterByExecutionOrder(dtos.ToList(), result, default(Guid));

            foreach (var task in tasks)
            {
                var taskTaskExpense = task.WorkTaskResource.WorkTaskResourceExpenses.Any()
                    ? task.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count)
                    : 0;
                var taskDto = result.FirstOrDefault(dto => dto.Id == task.Id);
                if (taskDto != null)
                    taskDto.TaskResourceExpense = taskTaskExpense;
            }

            foreach (var dto in result)
            {
                var task = tasks.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(task.FilialId, dto.ResponsibleId);
                dto.ResponsibleName = user?.UserFio;
                dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;

                if (projectId != Guid.Empty)
                {
                    var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
                    var childTasksCompleted =
                        await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
                    dto.TaskCount = childTasks;
                    dto.TaskCompletedCount = childTasksCompleted;
                }
            }

            return result;
        }

        public async Task<List<WorkTaskDto>> GetAllIncoming(TokenData tokenData, Guid projectId)
        {
            var key = 
                string.Format(Constants.CacheConstants.TASK_ALL_INCOMING_KEY, projectId, tokenData.UserId, tokenData.FilialId);
            var tasks = await _cacheManager.Get(key,
                async () => await _workTaskRepository.GetAllIncoming(tokenData, projectId));
            var dtos = _mapper.Map<IList<WorkTaskDto>>(tasks);

            foreach (var task in tasks)
            {
                var taskTaskExpense = task.WorkTaskResource.WorkTaskResourceExpenses.Any()
                    ? task.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count)
                    : 0;
                var taskDto = dtos.FirstOrDefault(dto => dto.Id == task.Id);
                if (taskDto != null)
                    taskDto.TaskResourceExpense = taskTaskExpense;
            }

            foreach (var dto in dtos)
            {
                var task = tasks.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(task.FilialId, dto.ResponsibleId);
                dto.ResponsibleName = user?.UserFio;
                dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;

                if (projectId != Guid.Empty)
                {
                    var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
                    var childTasksCompleted =
                        await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
                    dto.TaskCount = childTasks;
                    dto.TaskCompletedCount = childTasksCompleted;
                }
            }

            return dtos.ToList();
        }

        public async Task<List<WorkTaskDto>> GetAllOutgoing(TokenData tokenData, Guid projectId)
        {
            var key = 
                string.Format(Constants.CacheConstants.TASK_ALL_OUTGOING_KEY, projectId, tokenData.UserId, tokenData.FilialId);
            var tasks = await _cacheManager.Get(key,
                async () => await _workTaskRepository.GetAllOutgoing(tokenData, projectId));
            var dtos = _mapper.Map<IList<WorkTaskDto>>(tasks);

            foreach (var task in tasks)
            {
                var taskTaskExpense = task.WorkTaskResource.WorkTaskResourceExpenses.Any()
                    ? task.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count)
                    : 0;
                var taskDto = dtos.FirstOrDefault(dto => dto.Id == task.Id);
                if (taskDto != null)
                    taskDto.TaskResourceExpense = taskTaskExpense;
            }

            foreach (var dto in dtos)
            {
                var task = tasks.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(task.FilialId, dto.ResponsibleId);
                dto.ResponsibleName = user?.UserFio;
                dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;

                if (projectId != Guid.Empty)
                {
                    var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
                    var childTasksCompleted =
                        await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
                    dto.TaskCount = childTasks;
                    dto.TaskCompletedCount = childTasksCompleted;
                }
            }

            return dtos.ToList();
        }

        public async System.Threading.Tasks.Task UpdateStatus(TokenData tokenData, Guid id)
        {
            var task =  await _workTaskRepository.Get(id);
            if (task != null)
            {
                switch (task.Status)
                {
                    case WorkTaskStatus.New:
                        if (task.WorkTaskImplementers.Any(x => x.Status != WorkTaskStatus.New))
                        {
                            task.Status = WorkTaskStatus.InProgress;
                            await _workTaskRepository.Save();

                            _workTaskHistoryService.Add(
                                new WorkTaskHistory
                                {
                                    EditorId = tokenData.UserId,
                                    HistoryOperationType = HistoryOperationType.StatusСhange,
                                    Status = task.Status,
                                    Description = $"Task {task.Id} status update.",
                                    WorkTask = task
                                });

                            await _workProjectService.UpdateStatus(tokenData, task.WorkProjectId);
                        }
                        break;
                    case WorkTaskStatus.InProgress:
                        if (task.WorkTaskImplementers.All(x => x.Status == WorkTaskStatus.Done))
                        {
                            task.Status = WorkTaskStatus.Done;
                            await _workTaskRepository.Save();

                            _workTaskHistoryService.Add(
                                new WorkTaskHistory
                                {
                                    EditorId = tokenData.UserId,
                                    HistoryOperationType = HistoryOperationType.StatusСhange,
                                    Status = task.Status,
                                    Description = $"Task {task.Id} status update.",
                                    WorkTask = task
                                });

                            await _workProjectService.UpdateStatus(tokenData, task.WorkProjectId);
                        }
                        break;
                }
            }
        }

        public async Task<List<WorkTaskDto>> GetAllControling(TokenData tokenData, Guid projectId)
        {
            var key = 
                string.Format(Constants.CacheConstants.TASK_ALL_CONTROLING_KEY, projectId, tokenData.UserId, tokenData.FilialId);
            var tasks = await _cacheManager.Get(key,
                async () => await _workTaskRepository.GetAllControling(tokenData, projectId));
            var dtos = _mapper.Map<IList<WorkTaskDto>>(tasks);

            foreach (var task in tasks)
            {
                var taskTaskExpense = task.WorkTaskResource.WorkTaskResourceExpenses.Any()
                    ? task.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count)
                    : 0;
                var taskDto = dtos.FirstOrDefault(dto => dto.Id == task.Id);
                if (taskDto != null)
                    taskDto.TaskResourceExpense = taskTaskExpense;
            }

            foreach (var dto in dtos)
            {
                var task = tasks.FirstOrDefault(x => x.Id == dto.Id);
                var user = await _userService.Get(task.FilialId, dto.ResponsibleId);
                dto.ResponsibleName = user?.UserFio;
                dto.ResponsibleRoleName = user?.AppUserDepartment?.FuncRoles;

                if (projectId != Guid.Empty)
                {
                    var childTasks = await _workTaskRepository.GetChildTaskCount(dto.ParentId);
                    var childTasksCompleted =
                        await _workTaskRepository.GetChildTaskCount(dto.ParentId, onlyCompleted: true);
                    dto.TaskCount = childTasks;
                    dto.TaskCompletedCount = childTasksCompleted;
                }
            }

            return dtos.ToList();
        }

        private async Task<decimal> GetTaskExpenseWithChild(Guid taskId)
        {
            decimal result = 0;
            try
            {
                var items = await _workTaskRepository.GetChildByParentId(taskId);
                if (items.Any())
                {
                    result = items.Where(x => x.WorkTaskResource.WorkTaskResourceExpenses != null).Sum(item => item.WorkTaskResource.WorkTaskResourceExpenses.Sum(x => x.Count));
                    foreach (var item in items)
                    {
                        result += await GetTaskExpenseWithChild(item.Id);
                    }
                }
            }
            catch (Exception)
            {
                return result;
            }

            return result;
        }

        private async Task<List<TaskTimeline>> CalculateTimeline(Guid taskId, DateTime startDate, DateTime endDate)
        {
            var dayNumber = 1;
            var itemStatus = WorkTaskStatus.None;
            var days = new List<DateTime>();
            for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
                days.Add(dt);

            var result = new List<TaskTimeline>();
            var histories = await _workTaskHistoryService.GetAllByDate(taskId, startDate, endDate);
            foreach (var day in days)
            {
                var history = histories.FirstOrDefault(x => x.Created.Date == day);
                if (history != null)
                {
                    itemStatus = history.Status;
                    result.Add(
                        new TaskTimeline
                        {
                            Day = dayNumber,
                            Status = history.Status,
                            StatusName = history.Status.DisplayName()
                        });
                }
                else
                {
                    result.Add(
                        new TaskTimeline
                        {
                            Day = dayNumber,
                            Status = itemStatus,
                            StatusName = itemStatus.DisplayName()
                        });
                }

                dayNumber++;
            }

            var statuses = result.Select(x => x.Status).Distinct();
            foreach (var status in statuses)
            {
                var items = result.Where(x => x.Status == status);
                foreach (var item in items)
                    item.Duration = status != WorkTaskStatus.None ? items.Count() : 1;
            }

            return GetDistinct(result);
        }

        private async Task<List<UserTimeline>> CalculateUserTimeline(int filialId, int userId, Guid taskId,
            DateTime startDate, DateTime endDate)
        {
            var user = await _userService.Get(filialId, userId);
            if (user != null)
            {
                var dayNumber = 1;
                var itemStatus = WorkTaskStatus.None;
                var days = new List<DateTime>();
                for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
                    days.Add(dt);

                var result = new List<UserTimeline>();
                var histories = await _workTaskHistoryService.GetAllByDateAndEditor(taskId, userId, startDate, endDate);
                foreach (var day in days)
                {
                    var history = histories.FirstOrDefault(x => x.Created.Date == day);
                    if (history != null)
                    {
                        itemStatus = history.Status;
                        result.Add(
                            new UserTimeline
                            {
                                UserId = user.Id,
                                UserName = user.UserFio,
                                Day = dayNumber,
                                Status = history.Status,
                                StatusName = history.Status.DisplayName()
                            });
                    }
                    else
                    {
                        result.Add(
                            new UserTimeline
                            {
                                UserId = user.Id,
                                UserName = user.UserFio,
                                Day = dayNumber,
                                Status = itemStatus,
                                StatusName = itemStatus.DisplayName()
                            });
                    }

                    dayNumber++;
                }

                var statuses = result.Select(x => x.Status).Distinct();
                foreach (var status in statuses)
                {
                    var items = result.Where(x => x.Status == status);
                    foreach (var item in items)
                        item.Duration = status != WorkTaskStatus.None ? items.Count() : 1;
                }

                return GetDistinct(result);
            }

            return new List<UserTimeline>();
        }

        private List<UserTimeline> GetDistinct(IEnumerable<UserTimeline> items)
        {
            var result = new List<UserTimeline>();
            UserTimeline current = null;

            foreach (var item in items)
            {
                if (current == null)
                {
                    current = item;
                    result.Add(current);
                    continue;
                }

                if (item.Status == WorkTaskStatus.None || item.Status != current.Status)
                {
                    current = item;
                    result.Add(current);
                }
            }

            return result;
        }

        private List<TaskTimeline> GetDistinct(IEnumerable<TaskTimeline> items)
        {
            var result = new List<TaskTimeline>();
            TaskTimeline current = null;

            foreach (var item in items)
            {
                if (current == null)
                {
                    current = item;
                    result.Add(current);
                    continue;
                }

                if (item.Status == WorkTaskStatus.None || item.Status != current.Status)
                {
                    current = item;
                    result.Add(current);
                }
            }

            return result;
        }

        #endregion
    }
}