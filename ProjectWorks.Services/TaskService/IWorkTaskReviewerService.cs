﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkTaskReviewerService : IServiceBase<WorkTaskReviewer>
    {
        #region Methods

        Task<List<WorkTaskReviewerDto>> GetAll(Guid taskId);

        #endregion
    }
}