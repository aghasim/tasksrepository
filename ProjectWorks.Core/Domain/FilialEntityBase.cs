﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Domain
{
    /// <summary>
    ///     Базовая сущность для филиала
    /// </summary>
    public class FilialEntityBase : EntityBase
    {
        #region Properties

        // FilialId
        [Required]
        public int FilialId { get; set; }

        // Название
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        // Описание
        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }

        #endregion
    }
}