﻿using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskExecutionOrderRepository
{
    public class WorkTaskExecutionOrderRepository : GenericRepository<WorkTaskExecutionOrder>, IWorkTaskExecutionOrderRepository
    {
        #region Methods

        public WorkTaskExecutionOrderRepository(IDataContext context) : base(context)
        {
        } 

        #endregion
    }
}