import { Component, OnInit, Input } from '@angular/core';
import { CommonService, AmrappService } from 'src/app/services';
import { first } from 'rxjs/operators';
import { ListItem, Resource } from 'src/app/models';

@Component({
    selector: 'project-task-resource',
    templateUrl: 'project-task-resource.component.html',
    styleUrls: ['project-task-resource.component.css'],
  })

export class ProjectTaskResourceComponent implements OnInit {
  //#region Fields 

  @Input() resource : Resource;
  @Input() isProject : boolean;
  public resources: ListItem[];
  public expenditures: ListItem[];
  public needAddExpenditure: boolean = true;

  //#endregion

  //#region Constructor 

  public constructor(
    private commonService: CommonService, 
    private amrappService: AmrappService){
  }

  //#endregion

  //#region Methods 

  ngOnInit() {
    this.getTaskResourceTypeTypes();
    this.getExpenditures();
  }

  getTaskResourceTypeTypes() {
    this.commonService
      .getTaskResourceTypeTypes()
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.resources = items.map(function(item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  getExpenditures() {
    this.amrappService
      .getAllExpenditure()
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.expenditures = items.map(function(item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  //#endregion
}