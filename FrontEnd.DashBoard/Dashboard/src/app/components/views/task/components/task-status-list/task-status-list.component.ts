import { Component, Input } from '@angular/core';
import { first } from 'rxjs/operators';
import { ImplementerService } from 'src/app/services';
import { Implementer, Task } from 'src/app/models';

@Component({
    selector: 'task-status-list',
    templateUrl: 'task-status-list.component.html',
})

export class TaskStatusListComponent {
    //#region Fields 

    private _task: Task;
    public implementers: Implementer[];

    get task(): Task {
        return this._task;
    }

    @Input()
    set task(task: Task) {
        this._task = task;
        if (task)
            this.getImplementers(task.id);
    }

    //#endregion
    
    //#region Constructor
    
    public constructor(private implementerService: ImplementerService) {}

    //#endregion

    //#region Methods

    getImplementers(taskId: string) {
        this.implementerService
            .getAll(taskId)
            .pipe(first())
            .subscribe(
                implementers => {
                    this.implementers = implementers;
                },
                error => {
                    console.log(error);
                }
            )
    }

    //#endregion
}