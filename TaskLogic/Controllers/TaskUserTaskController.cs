﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using ProjectWorks.Data.Extensions;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Contracts.Intefaces;
using System.Collections.Generic;

namespace ProjectWorks.Contracts.Controllers
{
    [Route("api/Contracts/UserTasks")]
    public class TaskUserTaskController :Controller
    {
        private readonly ITaskUserTask _service;
        public TaskUserTaskController(ITaskUserTask service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ICollection<TaskUserTasks>> GetAll(int filialId)
        {
            return await _service.GetAll(filialId);
        }

        //[HttpGet]
        //public async Task<TaskUserTasks> Get(int id, int filialId)
        //{
        //    return await _service.Get(id, filialId);
        //}

        [HttpDelete]
        public async System.Threading.Tasks.Task Delete(int id, int filialId)
        {
            await _service.Delete(id, filialId);
        }

        [HttpPost("Add")]
        public async System.Threading.Tasks.Task Add(TaskUserTasks task, int filialId)
        {
            await _service.Add(task, filialId);
        }
    }
}
