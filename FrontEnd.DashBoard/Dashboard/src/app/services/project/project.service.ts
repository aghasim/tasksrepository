import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Project } from '../../models';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  private apiUrl: string = environment.apiUrl + 'projects/';

  constructor(private http: HttpClient) {
  }

  get(projectId: string) {
    return this.http.get<any>(this.apiUrl + projectId)
      .pipe(map(result => {
        return result.data;
      }))
      .pipe(catchError((err, caught) => {
        return null;
      }))
  }

  getAll() {
    return this.http.get<any>(this.apiUrl)
      .pipe(map(result => {
        return result.data;
      }))
      .pipe(catchError((err, caught) => {
        return null;
      }))
  }

  getAllByMonth(year: number, month: number) {
    return this.http
        .get<any>(this.apiUrl + "allbymonth/" + year + "/" + month)
        .pipe(
            map(result => {
                return result.data;
            })
        )
        .pipe(
            catchError((err, caught) => {
                return null;
            })
        );
}

  add(project: Project) {
    return this.http.post<any>(this.apiUrl + 'add', { project })
      .pipe(map(result => {
        return result.success;
      }))
      .pipe(catchError((err, caught) => {
        return null;
      }))
  }

  update(project: Project) {
    return this.http.put<any>(this.apiUrl + 'update', { project })
      .pipe(map(result => {
        return result.success;
      }))
      .pipe(catchError((err, caught) => {
        return null;
      }))
  }

  delete(projectId: string) {
    return this.http.delete<any>(this.apiUrl + projectId)
      .pipe(map(result => {
        return result.success;
      }))
      .pipe(catchError((err, caught) => {
        return null;
      }))
  }
}