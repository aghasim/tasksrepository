﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Filters;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;
using ProjectWorks.Task.ViewModels.WorkProjectPattern;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/workProjectPattern")]
    public class WorkProjectPatternController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkProjectPatternService _workProjectPatternService;

        private readonly ILogger<WorkProjectPatternController> _logger;

        #endregion

        #region Constructor

        public WorkProjectPatternController(IMapper mapper, IWorkProjectPatternService workProjectPatternService, ILogger<WorkProjectPatternController> logger)
        {
            _logger = logger;
            _mapper = mapper;
            _workProjectPatternService = workProjectPatternService;
        }

        #endregion


        #region Methods


        [HttpPost]
        public IActionResult Post([FromBody] CreateViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                return this.Success(_workProjectPatternService.Create(model.WorkProjectId, model.Name));
            }
            catch (Exception e) {
                _logger.LogError(e.ToString());
                return this.Error(e.ToString());
            }
        }



        #endregion
    }
}
