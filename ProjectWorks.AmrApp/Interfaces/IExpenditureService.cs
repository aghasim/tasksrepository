﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IExpenditureService : IAmrService<Object1>
    {
        #region Methods

        Task<IEnumerable<Object1>> GetByFilialId(int filialId, int userId);

        Task AddExpenditure(TokenData tokenData, WorkTaskResourceExpense expense, int expenditureId);

        Task ExpenditureApprove(TokenData tokenData, int expenditureId);

        #endregion
    }
}