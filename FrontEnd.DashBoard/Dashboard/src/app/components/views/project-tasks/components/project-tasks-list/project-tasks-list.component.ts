import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Task } from 'src/app/models';
import * as moment from 'moment';
import { TaskService } from 'src/app/services';

@Component({
    selector: 'project-task-list',
    templateUrl: 'project-tasks-list.component.html',
    styleUrls: ['project-tasks-list.component.css'],
})

export class ProjectTasksListComponent implements OnInit {

    public allTask: Task[];
    private projectId: string;

    constructor(
        private route: ActivatedRoute,
        private taskService: TaskService,
    ) {
    }
   
    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.projectId = params.get("id");
        });

        this.getAll();
    }

    getAll(){
        this.taskService
            .getAll(this.projectId)
            .pipe(first())
            .subscribe(
                tasks => {
                    if (tasks) {
                        var GetCurrentTask = function(task: Task) {
                            task.startDate = moment(task.startDate).format('DD.MM.YYYY');
                            task.completionDate = moment(task.completionDate).format('DD.MM.YYYY');
                            task.childTasks.forEach(function(inTask: Task){
                                return GetCurrentTask(inTask);
                            });
                            return task;
                        }
                        this.allTask = tasks.map(function(task: Task){
                            return GetCurrentTask(task);
                        })
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    onDelete(){
        this.getAll();
    }
}