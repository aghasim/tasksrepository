import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './helpers';
import { 
  LoginComponent, 
  ProjectTasksComponent, 
  ProjectWizzardComponent, 
  ProjectEditWizzardComponent, 
  TaskEditWizzardComponent, 
  TaskComponent,
  UserTasksComponent,
  ProjectScheduleComponent,
  ProjectsListScheduleComponent} from './components';
import { HomeComponent } from './components';
import { AccessDeniedComponent } from './components';
import { ProjectsComponent } from './components';
import { TaskWizzardComponent } from './components';
import { CreateWorkProjectPatternComponent } from './components/views/patterns/work-project-pattern/create-work-project-pattern/create-work-project-pattern.component';
import { CreateWorkTaskPatternComponent } from './components/views/patterns/work-task-pattern/create-work-task-pattern/create-work-task-pattern.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  // url like http://localhost:4200/login/64/11
  { path: 'login/:userId/:filialId', component: LoginComponent },
  // url like http://localhost:4200/login/64/11/5
  { path: 'login/:userId/:filialId/:returnUrlType', component: LoginComponent },
  { path: 'access-denied', component: AccessDeniedComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'projects', component: ProjectsComponent, canActivate: [AuthGuard] },
  { path: 'projects/schedule', component: ProjectsListScheduleComponent},
  { path: 'project/:id', component: ProjectTasksComponent, canActivate: [AuthGuard] },
  { path: 'project/:id/tasks', component: ProjectTasksComponent, canActivate: [AuthGuard] },
  { path: 'project/:id/schedule', component: ProjectScheduleComponent},
  { path: 'task/:id', component: TaskComponent, canActivate: [AuthGuard] },
  { path: 'user-tasks', component: UserTasksComponent, canActivate: [AuthGuard] },
  { path: 'user-tasks/:filterDatePeriodType/:filterTaskType', component: UserTasksComponent, canActivate: [AuthGuard] },
  { path: 'task-wizard/:projectId', component: TaskWizzardComponent, canActivate: [AuthGuard] },
  { path: 'task-wizard/:projectId/:parentId', component: TaskWizzardComponent, canActivate: [AuthGuard] },
  { path: 'task-edit-wizard/:taskId', component: TaskEditWizzardComponent, canActivate: [AuthGuard] },
  { path: 'project-wizard', component: ProjectWizzardComponent, canActivate: [AuthGuard] },
  { path: 'project-wizard/:parentId', component: ProjectWizzardComponent, canActivate: [AuthGuard] },
  { path: 'project-edit-wizard/:projectId', component: ProjectEditWizzardComponent, canActivate: [AuthGuard] },
  { path: 'create-work-project-pattern/:id', component: CreateWorkProjectPatternComponent, canActivate: [AuthGuard] },
  { path: 'create-work-task-pattern/:id', component: CreateWorkTaskPatternComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'access-denied' }
];

export const AppRoutingModule = RouterModule.forRoot(routes);