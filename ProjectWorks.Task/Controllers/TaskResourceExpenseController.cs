﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Filters;
using ProjectWorks.Core.Models.Response;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/resourceexpenses")]
    public class TaskResourceExpenseController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskResourceExpenseService _taskResourceExpenseService;

        private readonly IWorkTaskResourceService _taskResourceService;

        private readonly IExpenditureService _expenditureService;

        #endregion

        #region Constructor

        public TaskResourceExpenseController(
            IMapper mapper, 
            IWorkTaskResourceExpenseService taskResourceExpenseService, 
            IWorkTaskResourceService taskResourceService,
            IExpenditureService expenditureService)
        {
            _mapper = mapper;
            _taskResourceExpenseService = taskResourceExpenseService;
            _taskResourceService = taskResourceService;
            _expenditureService = expenditureService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var item = await _taskResourceExpenseService.Get(id);
            var dto = _mapper.Map<WorkTaskResourceExpenseDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpGet("all/{id}")]
        public async Task<IActionResult> GetAll(Guid id)
        {
            var items = await _taskResourceExpenseService.GetAll(id);
            var dtos = _mapper.Map<IList<WorkTaskResourceExpenseDto>>(items);
            return this.Success(dtos);
        }

        [FilialAction]
        [HttpPost("add")]
        public async Task<IActionResult> Add(TokenData tokenData, [FromBody]  WorkTaskResourceExpenseResponse response)
        {
            try
            {
                var item = _mapper.Map<WorkTaskResourceExpense>(response.Expense);
                var resource = await _taskResourceService.Get(response.Expense.TaskResourceId);
                if (resource != null)
                {
                    item.WorkTaskResource = resource;
                    _taskResourceExpenseService.Add(item);

                    if (resource.ExpenditureId.HasValue)
                    {
                        await _expenditureService.AddExpenditure(tokenData, item, resource.ExpenditureId.Value);
                    }

                    return this.Success();
                }
                else
                {
                    return this.Error($"Task resource {response.Expense.TaskResourceId} not found!");
                }
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] WorkTaskResourceExpenseDto dto)
        {
            var item = await _taskResourceExpenseService.Get(dto.Id);
            if (item != null)
            {
                item.Count = dto.Count;

                _taskResourceExpenseService.Update(item);
                return this.Success();
            }

            return this.Error($"Task resource expense {dto.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _taskResourceExpenseService.Delete(id);
            return this.Success();
        }

        #endregion
    }
}