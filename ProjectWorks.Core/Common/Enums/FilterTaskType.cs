﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Тип задач фильтра
    /// </summary>
    public enum FilterTaskType
    {
        /// <summary>
        ///     Все контроль
        /// </summary>
        [Display(Name = "Все контроль")]
        Controling = 1,
        /// <summary>
        ///     Все входящие
        /// </summary>
        [Display(Name = "Все входящие")]
        Incoming,
        /// <summary>
        ///     Все исходящие
        /// </summary>
        [Display(Name = "Все исходящие")]
        Outgoing,
        /// <summary>
        ///     Активные контроль
        /// </summary>
        [Display(Name = "Активные контроль")]
        ActiveControling,
        /// <summary>
        ///     Активные входящие
        /// </summary>
        [Display(Name = "Активные входящие")]
        ActiveIncoming,
        /// <summary>
        ///     Активные исходящие
        /// </summary>
        [Display(Name = "Активные исходящие")]
        ActiveOutgoing,
        /// <summary>
        ///     Новые контроль
        /// </summary>
        [Display(Name = "Новые контроль")]
        NewControling,
        /// <summary>
        ///     Новые входящие
        /// </summary>
        [Display(Name = "Новые входящие")]
        NewIncoming,
        /// <summary>
        ///     Новые исходящие
        /// </summary>
        [Display(Name = "Новые исходящие")]
        NewOutgoing,
        /// <summary>
        ///     На завтра
        /// </summary>
        [Display(Name = "На завтра")]
        Tomorrow,
        /// <summary>
        ///     На сегодня
        /// </summary>
        [Display(Name = "На сегодня")]
        Today,
        /// <summary>
        ///     Истекшие
        /// </summary>
        [Display(Name = "Истекшие")]
        Past
    }
}