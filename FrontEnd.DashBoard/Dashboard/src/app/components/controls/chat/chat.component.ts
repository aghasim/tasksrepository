import * as moment from 'moment';
import { Component, Input } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services';
import { User, Task, Conversation } from 'src/app/models';

@Component({
  selector: 'chat',
  templateUrl: 'chat.component.html'
})
export class ChatComponent {
  //#region Fields 

  private _task: Task;
  private chatUrl: string = environment.chatUrl;
  private hubConnection: HubConnection;
  public message: string;
  public conversations: Conversation[];
  private currentUser: User;

  //#endregion

  //#region Properties 

  get task(): Task {
    return this._task;
  }

  @Input()
  set task(task: Task) {
    this._task = task;
    if (task)
      this.initChat();
  }

  //#endregion

  //#region Constructor 

  public constructor(private authenticationService: AuthenticationService) {
    this.conversations = [];
    this.currentUser = this.authenticationService.currentUserValue;
  }

  //#endregion

  //#region Methods 

  public sendMessage(): void {
    let conversation = new Conversation();
    conversation.created = moment().utc().format('YYYY-MM-DDTHH:mm:ss');
    conversation.taskId = this.task.id;
    conversation.senderId = this.currentUser.id;
    conversation.message = this.message;
    this.hubConnection
      .invoke('sendToGroup', conversation)
      .then(() => this.message = '')
      .catch(error => console.error(error));
  }

  initChat() {
    this.hubConnection = new HubConnectionBuilder().withUrl(this.chatUrl).build();
    this.hubConnection
      .start()
      .then(() => this.hubConnection.invoke('connect', this.currentUser.username))
      .then(() => this.hubConnection.invoke('joinGroup', this.task.id))
      .catch(error => console.log(error));

    this.hubConnection.on('ReceiveMessage', (conversation: Conversation) => {
      conversation.created = moment(conversation.created).format('HH:mm DD.MM.YYYY');
      this.conversations.unshift(conversation);
    });
  }

  //#endregion
}