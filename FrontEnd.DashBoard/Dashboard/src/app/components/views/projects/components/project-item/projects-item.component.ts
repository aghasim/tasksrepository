import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Project } from 'src/app/models';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ProjectService } from 'src/app/services';
import { first } from 'rxjs/operators';

@Component({
    selector: 'project-item',
    templateUrl: 'projects-item.component.html',
    styleUrls: ['projects-item.component.css'],
})

export class ProjectsItemComponent {

    @Input() itemData: Project;
    @Output() onDelete = new EventEmitter();

    public visibleProjectChildStatus: boolean = false;

    //#region Constructor

    public constructor(
        private projectService: ProjectService,
        public ngxSmartModalService: NgxSmartModalService) { }

    //#endregion

    toggle() {
        this.visibleProjectChildStatus = !this.visibleProjectChildStatus;
    }

    removeProject(projectId: string) {
        this.projectService
            .delete(projectId)
            .pipe(first())
            .subscribe(
                result => {
                    if (result) {
                        this.onDelete.emit();
                    }
                },
                error => {
                    console.log(error);
                }
            )
    }
}