﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using ProjectWorks.Core.Domain;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Context
{
    public class DataContext : DbContext, IDataContext
    {
        #region Constructors

        public DataContext()
        {
            Database.EnsureCreated();
        }

        #endregion

        #region Properties
        public DbSet<WorkTaskPattern> WorkTaskPatterns { get; set; }

        public DbSet<WorkProject> WorkProjects { get; set; }

        public DbSet<WorkProjectPattern> WorkProjectPatterns { get; set; }

        public DbSet<WorkProjectResource> WorkProjectResources { get; set; }

        public DbSet<WorkTask> WorkTasks { get; set; }

        public DbSet<WorkTaskResource> WorkTaskResources { get; set; }

        public DbSet<WorkTaskResourceExpense> WorkTaskResourceExpenses { get; set; }

        public DbSet<WorkTaskExecutionOrder> WorkTaskExecutionOrders { get; set; }

        public DbSet<WorkTaskHistory> WorkTaskHistories { get; set; }

        public DbSet<WorkTaskTemplate> WorkTaskTemplates { get; set; }

        public DbSet<WorkTaskImplementer> WorkTaskImplementers { get; set; }

        public DbSet<WorkTaskReviewer> WorkTaskReviewers { get; set; }

        public DbSet<Conversation> Conversations { get; set; }

        public DbSet<WorkProjectHistory> WorkProjectHistories { get; set; }

        #endregion

        #region Methods

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true)
                .Build();

            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
        }

        public new DbSet<T> Set<T>() where T : EntityBase => base.Set<T>();

        public new EntityEntry<T> Entry<T>(T entity) where T : EntityBase => base.Entry(entity);

        public Task<int> SaveChangesAsync() => base.SaveChangesAsync();

        #endregion
    }
}