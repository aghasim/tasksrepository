﻿namespace ProjectWorks.Core.Common
{
    public class TokenData
    {
        #region Properties

        public int UserId { get; set; }

        public int FilialId { get; set; }

        #endregion
    }
}