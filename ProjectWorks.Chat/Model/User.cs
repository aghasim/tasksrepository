﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Chat.Model
{
    public class User
    {
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
    }
}
