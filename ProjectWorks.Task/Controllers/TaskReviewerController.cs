﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Filters;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/reviewers")]
    public class TaskReviewerController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskReviewerService _reviewerService;

        #endregion

        #region Constructor

        public TaskReviewerController(
            IMapper mapper,
            IWorkTaskReviewerService reviewerService)
        {
            _mapper = mapper;
            _reviewerService = reviewerService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("all/{id}")]
        public async Task<IActionResult> GetAll(Guid id)
        {
            try
            {
                var dtos = await _reviewerService.GetAll(id);
                return this.Success(dtos);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        #endregion
    }
}