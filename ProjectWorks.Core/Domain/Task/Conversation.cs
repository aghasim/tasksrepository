﻿using System;

namespace ProjectWorks.Core.Domain.Task
{
    public class Conversation : EntityBase
    {
        #region Properties

        // отправитель
        public int SenderId { get; set; }

        // получатель
        public int? RecipientId { get; set; }

        // сообщение
        public string Message { get; set; }

        // Задача Id
        public Guid WorkTaskId { get; set; }

        // Задача
        public virtual WorkTask WorkTask { get; set; }

        #endregion
    }
}
