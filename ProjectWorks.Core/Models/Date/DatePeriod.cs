﻿using System;

namespace ProjectWorks.Core.Models.Date
{
    public class DatePeriod
    {
        #region Properties

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        #endregion
    }
}