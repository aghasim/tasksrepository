﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskImplementerRepository
{
    public class WorkTaskImplementerRepository : GenericRepository<WorkTaskImplementer>, IWorkTaskImplementerRepository
    {
        #region Constructor

        public WorkTaskImplementerRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<WorkTaskImplementer> GetByTask(TokenData tokenData, Guid taskId)
        {
            return await _entities.Include(x => x.WorkTask)
                .FirstOrDefaultAsync(implementer => implementer.WorkTaskId == taskId &&
                                                    implementer.ImplementerId == tokenData.UserId &&
                                                    !implementer.IsDeleted);
        }

        public async Task<List<WorkTaskImplementer>> GetAll(Guid taskId)
        {
            return await _entities.Include(x => x.WorkTask)
                .Where(implementer => implementer.WorkTaskId == taskId && !implementer.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        }

        #endregion
    }
}