﻿using Microsoft.Extensions.DependencyInjection;
using ProjectWorks.AmrApp.Extensions;
using ProjectWorks.Data.Repository.WorkProjectHistoryRepository;
using ProjectWorks.Data.Repository.WorkProjectRepository;
using ProjectWorks.Project.Services;
using ProjectWorks.Services.ProjectService;

namespace ProjectWorks.Project.Extensions
{
    public static class AddProjectExtension
    {
        #region Methods

        public static IServiceCollection AddProjects(this IServiceCollection services)
        {
            // Register module providers
            services.AddAmrApp();
            // Register module services
            services.AddScoped(typeof(IWorkProjectService), typeof(ProjectService));
            services.AddScoped(typeof(IWorkProjectHistoryService), typeof(WorkProjectHistoryService));
            // Register module repository
            services.AddScoped(typeof(IWorkProjectRepository), typeof(WorkProjectRepository));
            services.AddScoped(typeof(IWorkProjectHistoryRepository), typeof(WorkProjectHistoryRepository));

            return services;
        } 

        #endregion
    }
}