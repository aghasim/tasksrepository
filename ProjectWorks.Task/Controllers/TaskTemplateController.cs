﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Filters;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/templates")]
    public class TaskTemplateController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskTemplateService _taskTaskTemplateService;

        #endregion

        #region Constructor

        public TaskTemplateController(IMapper mapper, IWorkTaskTemplateService taskTaskTemplateService)
        {
            _mapper = mapper;
            _taskTaskTemplateService = taskTaskTemplateService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var item = await _taskTaskTemplateService.Get(id);
            var dto = _mapper.Map<WorkTaskTemplateDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpGet]
        public async Task<IActionResult> GetAll(TokenData tokenData)
        {
            var items = await _taskTaskTemplateService.GetAll(tokenData);
            var dtos = _mapper.Map<IList<WorkTaskTemplateDto>>(items);
            return this.Success(dtos);
        }

        [FilialAction]
        [HttpPost("add")]
        public IActionResult Add([FromBody] WorkTaskTemplateDto dto)
        {
            var item = _mapper.Map<WorkTaskTemplate>(dto);
            _taskTaskTemplateService.Add(item);
            return this.Success();
        }

        [FilialAction]
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] WorkTaskTemplateDto dto)
        {
            var item = await _taskTaskTemplateService.Get(dto.Id);
            if (item != null)
            {
                item.ResponsibleId = dto.ResponsibleId;
                item.WorkTaskExecutionOrderType = dto.WorkTaskExecutionOrderType;
                item.ExecutionOrder = dto.ExecutionOrder;
                item.WorkTaskResourceType = dto.WorkTaskResourceType;
                item.ResourceCount = dto.ResourceCount;

                _taskTaskTemplateService.Update(item);
                return this.Success();
            }

            return this.Error($"Task template {dto.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _taskTaskTemplateService.Delete(id);
            return this.Success();
        }

        #endregion
    }
}