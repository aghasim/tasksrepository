﻿using System;
using System.Collections.Generic;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Models.Timeline;

namespace ProjectWorks.Core.Dtos.Project
{
    public class WorkProjectDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Название
        public string Name { get; set; }

        // Описание
        public string Description { get; set; }

        // Статус проекта
        public WorkProjectStatus Status { get; set; }

        // Статус проекта
        public string StatusName { get; set; }

        // Ответственный, пользователь из А2
        public int ResponsibleId { get; set; }

        // Ответственный, пользователь из А2
        public string ResponsibleName { get; set; }

        // Дата начало
        public DateTime StartDate { get; set; }

        // Дата завершения
        public DateTime CompletionDate { get; set; }

        // Родительский проект
        public Guid ParentId { get; set; } = Guid.Empty;

        // Количество выполненных задач
        public int TaskCompletedCount { get; set; }

        // Всего задач
        public int TaskCount { get; set; }

        // Ресурс проекта
        public WorkProjectResourceDto ProjectResource { get; set; }

        // Расход ресурса
        public decimal ProjectResourceExpense { get; set; }

        // вложенные проекты
        public List<WorkProjectDto> ChildProjects { get; set; } = new List<WorkProjectDto>();

        // Timeline
        public List<ProjectTimeline> Timeline { get; set; } = new List<ProjectTimeline>();

        #endregion
    }
}