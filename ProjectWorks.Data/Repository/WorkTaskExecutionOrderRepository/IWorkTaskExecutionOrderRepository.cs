﻿using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskExecutionOrderRepository
{
    public interface IWorkTaskExecutionOrderRepository : IGenericRepository<WorkTaskExecutionOrder>
    {
    }
}