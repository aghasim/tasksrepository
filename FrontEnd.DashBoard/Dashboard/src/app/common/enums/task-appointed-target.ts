export enum TaskAppointedTarget {
  ObjectType = 1,
  Object,
  Department,
  Role,
  User,
  AllSubordinate
}