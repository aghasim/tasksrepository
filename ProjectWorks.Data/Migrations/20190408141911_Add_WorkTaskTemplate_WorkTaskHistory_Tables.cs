﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Add_WorkTaskTemplate_WorkTaskHistory_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ParentId",
                table: "WorkProjects",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "WorkProjectResourceId",
                table: "WorkProjects",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "WorkProjectResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Count = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkProjectResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkTaskHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    EditorId = table.Column<int>(nullable: false),
                    WorkTaskId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkTaskHistories_WorkTasks_WorkTaskId",
                        column: x => x.WorkTaskId,
                        principalTable: "WorkTasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkTaskTemplates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    FilialId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ResponsibleId = table.Column<int>(nullable: true),
                    WorkTaskResourceType = table.Column<int>(nullable: true),
                    ResourceCount = table.Column<decimal>(nullable: true),
                    WorkTaskExecutionOrderType = table.Column<int>(nullable: false),
                    ExecutionOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskTemplates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkProjects_WorkProjectResourceId",
                table: "WorkProjects",
                column: "WorkProjectResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskHistories_WorkTaskId",
                table: "WorkTaskHistories",
                column: "WorkTaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkProjects_WorkProjectResources_WorkProjectResourceId",
                table: "WorkProjects",
                column: "WorkProjectResourceId",
                principalTable: "WorkProjectResources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkProjects_WorkProjectResources_WorkProjectResourceId",
                table: "WorkProjects");

            migrationBuilder.DropTable(
                name: "WorkProjectResources");

            migrationBuilder.DropTable(
                name: "WorkTaskHistories");

            migrationBuilder.DropTable(
                name: "WorkTaskTemplates");

            migrationBuilder.DropIndex(
                name: "IX_WorkProjects_WorkProjectResourceId",
                table: "WorkProjects");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "WorkProjects");

            migrationBuilder.DropColumn(
                name: "WorkProjectResourceId",
                table: "WorkProjects");
        }
    }
}
