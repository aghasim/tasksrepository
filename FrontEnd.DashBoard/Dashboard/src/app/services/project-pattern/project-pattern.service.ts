import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Project } from '../../models';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ProjectPatternService {
  private apiUrl: string = environment.apiUrl + 'workProjectPattern/';

  constructor(private http: HttpClient) {
  }

  add(name: string, workProjectId: string) {
    return this.http.post<any>(this.apiUrl, { Name: name, WorkProjectId: workProjectId })
      .pipe(map(result => {
        return result.success;
      }))
      .pipe(catchError((err, caught) => {
        return null;
      }))
  }
}