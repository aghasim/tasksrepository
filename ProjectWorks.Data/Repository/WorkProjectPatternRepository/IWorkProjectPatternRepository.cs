﻿using ProjectWorks.Core.Domain.Task;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Data.Repository.WorkProjectPatternRepository
{
    public interface IWorkProjectPatternRepository: IGenericRepository<WorkProjectPattern>
    {
    }
}
