import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { environment } from "../../../environments/environment";

@Injectable({ providedIn: "root" })
export class CommonService {
    private apiUrl: string = environment.apiUrl + "common/";

    constructor(private http: HttpClient) { }

    getTaskExecutionOrderTypes() {
        return this.http
            .get<any>(this.apiUrl + "taskexecutionordertypes")
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getTaskResourceTypeTypes() {
        return this.http
            .get<any>(this.apiUrl + "taskresourcetypes")
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getTaskStatuses() {
        return this.http
            .get<any>(this.apiUrl + "taskstatuses")
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getMonths() {
        return this.http
            .get<any>(this.apiUrl + "months")
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }
}
