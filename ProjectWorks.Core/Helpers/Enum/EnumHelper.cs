﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Helpers.Enum
{
    public static class EnumHelper
    {
        #region Methods

        public static string DisplayName(this System.Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            var attribute
                = Attribute.GetCustomAttribute(field, typeof(DisplayAttribute))
                    as DisplayAttribute;

            return attribute == null ? value.ToString() : attribute.Name;
        } 

        #endregion
    }
}