﻿using Microsoft.Extensions.DependencyInjection;
using ProjectWorks.AmrApp.Extensions;
using ProjectWorks.Contracts.Intefaces;
using ProjectWorks.Contracts.Services;
using ProjectWorks.Task.Services;

namespace ProjectWorks.Contracts.Extensions
{
    public static class AddContractsExtension
    {
        public static IServiceCollection AddContracts(this IServiceCollection services)
        {
            // Register module providers
            services.AddAmrApp();
            // Register module services
            services.AddScoped<WorkTaskService>();
            // Register module repositories
            services.AddTransient<ITaskDbModel, TaskDbModelService>();

            return services;
        }
    }
}