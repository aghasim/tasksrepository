import { ModelBase } from '../model.base';

export class Implementer extends ModelBase {
  public status: number;
  public statusName: string;
  public taskId: string;
  public implementerId: number;
  public implementerName: string;
  public implementerRoleName: string;
}
