import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/app/services';
import { Project } from 'src/app/models';
import { first } from 'rxjs/operators';

@Component({
    templateUrl: 'project-tasks.component.html',
    styleUrls: ['project-tasks.component.css'],
})

export class ProjectTasksComponent implements OnInit {
    //#region Fields 

    private projectId: string;
    public project: Project;

    //#endregion

    //#region Constructor

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private projectService: ProjectService) {
        this.route.paramMap.subscribe(params => {
            this.projectId = params.get("id").toString();
            this.setProject();
        });
    }

    //#endregion

    //#region Methods

    ngOnInit() {
    }

    onAddTask() {
        let url = "/task-wizard/" + this.projectId;
        this.router.navigate([url]);
    }

    setProject() {
        if (this.projectId) {
            this.projectService
                .get(this.projectId)
                .pipe(first())
                .subscribe(
                    result => {
                        if (result) {
                            this.project = result;
                        }
                    },
                    error => {
                        console.log(error);
                    }
                );
        }
    }
    //#endregion
}