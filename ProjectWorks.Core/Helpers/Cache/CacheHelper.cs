﻿using System;
using System.Collections.Generic;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Utils.Cache;

namespace ProjectWorks.Core.Helpers.Cache
{
    public static class CacheHelper
    {
        #region Methods

        public static void ClearCache(ICacheManager cacheManager, TokenData tokenData, Guid projectId, Guid taskId, int creatorId = 0)
        {
            var keys = new List<string>
            {
                string.Format(Constants.CacheConstants.TASK_ALL_KEY, projectId),
                string.Format(Constants.CacheConstants.TASK_FILTER_KEY, projectId, tokenData.UserId, tokenData.FilialId),
                string.Format(Constants.CacheConstants.TASK_ALL_ACTIVE_KEY, projectId, tokenData.UserId, tokenData.FilialId),
                string.Format(Constants.CacheConstants.TASK_ALL_INCOMING_KEY, projectId, tokenData.UserId, tokenData.FilialId),
                string.Format(Constants.CacheConstants.TASK_ALL_OUTGOING_KEY, projectId, tokenData.UserId, tokenData.FilialId),
                string.Format(Constants.CacheConstants.TASK_ALL_CONTROLING_KEY, projectId, tokenData.UserId, tokenData.FilialId),
                string.Format(Constants.CacheConstants.IMPLEMENTER_KEY, taskId, tokenData.UserId),
                string.Format(Constants.CacheConstants.IMPLEMENTER_ALL_KEY, taskId),
                string.Format(Constants.CacheConstants.REVIEWER_ALL_KEY, taskId),
                string.Format(Constants.CacheConstants.PROJECT_ALL_KEY, tokenData.FilialId, creatorId),
                string.Format(Constants.CacheConstants.EXPENDITURE_ALL_KEY, tokenData.FilialId)
            };

            foreach (var key in keys)
                cacheManager.Remove(key);

            cacheManager.RemoveByPattern("projectworks.task.allbyfilter");
        }

        #endregion
    }
}