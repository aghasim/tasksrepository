﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkTaskImplementerService : IServiceBase<WorkTaskImplementer>
    {
        #region Methods

        void Update(TokenData tokenData, WorkTaskImplementer entity);
        Task<WorkTaskImplementerDto> GetByTask(TokenData tokenData, Guid taskId);

        Task<List<WorkTaskImplementerDto>> GetAll(TokenData tokenData, Guid taskId);

        #endregion
    }
}