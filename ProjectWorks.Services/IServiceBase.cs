﻿using System;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain;

namespace ProjectWorks.Services
{
    public interface IServiceBase<TEntity> where TEntity : EntityBase
    {
        #region Methods

        Task<TEntity> Get(Guid id);

        void Add(TEntity entity);

        void Update(TEntity entity);

        Task Delete(Guid id);

        #endregion
    }
}