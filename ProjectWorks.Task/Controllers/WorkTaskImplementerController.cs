﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;
using ProjectWorks.Core.Filters;
using ProjectWorks.Core.Models.Response;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Controllers
{
    [Authorize]
    [Route("api/implementers")]
    public class WorkTaskImplementerController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkTaskImplementerService _workTaskImplementerService;

        #endregion

        #region Constructor

        public WorkTaskImplementerController(IMapper mapper, IWorkTaskImplementerService workTaskImplementerService)
        {
            _mapper = mapper;
            _workTaskImplementerService = workTaskImplementerService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var item = await _workTaskImplementerService.Get(id);
            var dto = _mapper.Map<WorkTaskImplementerDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpGet("getbytask/{id}")]
        public async Task<IActionResult> GetByTask(TokenData tokenData, Guid id)
        {
            var item = await _workTaskImplementerService.GetByTask(tokenData, id);
            var dto = _mapper.Map<WorkTaskImplementerDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpGet("all/{id}")]
        public async Task<IActionResult> GetAll(TokenData tokenData, Guid id)
        {
            var items = await _workTaskImplementerService.GetAll(tokenData, id);
            var dtos = _mapper.Map<IList<WorkTaskImplementerDto>>(items);
            return this.Success(dtos);
        }

        [FilialAction]
        [HttpPost("add")]
        public IActionResult Add([FromBody] WorkTaskImplementerResponse response)
        {
            var item = _mapper.Map<WorkTaskImplementer>(response.Implementer);
            _workTaskImplementerService.Add(item);
            return this.Success();
        }

        [FilialAction]
        [HttpPut("update")]
        public async Task<IActionResult> Update(TokenData tokenData, [FromBody] WorkTaskImplementerResponse response)
        {
            var item = await _workTaskImplementerService.Get(response.Implementer.Id);
            if (item != null)
            {
                item.Status = response.Implementer.Status;

                _workTaskImplementerService.Update(tokenData, item);
                return this.Success();
            }

            return this.Error($"Implementer {response.Implementer.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _workTaskImplementerService.Delete(id);
            return this.Success();
        }

        #endregion
    }
}