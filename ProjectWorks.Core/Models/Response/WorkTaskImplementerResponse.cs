﻿using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Core.Models.Response
{
    public class WorkTaskImplementerResponse
    {
        #region Properties

        public WorkTaskImplementerDto Implementer { get; set; }

        #endregion
    }
}