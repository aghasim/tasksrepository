﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Расход ресурса задачи
    /// </summary>
    public class WorkTaskResourceExpense : EntityBase
    {
        #region Properties

        // Ресурс Id
        public Guid WorkTaskResourceId { get; set; }

        // Ресурс
        public virtual WorkTaskResource WorkTaskResource { get; set; }

        // Количество расхода ресурса
        [Required]
        [Range(0, double.MaxValue)]
        public decimal Count { get; set; }

        #endregion
    }
}