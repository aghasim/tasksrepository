import * as moment from 'moment';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Task, ListItem, Implementer, TaskHistory, TaskResourceExpense } from 'src/app/models';
import { CommonService, ImplementerService, TaskHistoryService, TaskResourceExpenseService, TaskService } from 'src/app/services';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HistoryOperationType } from 'src/app/common';

@Component({
    selector: 'task-status',
    templateUrl: 'task-status.component.html',
})

export class TaskStatusComponent {
    //#region Fields 
    @Output() onStatusChanged = new EventEmitter();
    public _task: Task;
    public taskStatuses: ListItem[];
    public selectedStatus: number;
    public description: string;
    public implementer: Implementer;
    public needAddResourceExpense: boolean;
    public resourceExpense: number = 0;

    get task(): Task {
        return this._task;
    }

    @Input()
    set task(task: Task) {
        this._task = task;
        if (task) {
            this.getTaskStatuses();
            this.getImplementer();
        }
    }

    //#endregion

    //#region Constructor

    public constructor(
        private router: Router,
        private taskService: TaskService,
        private commonService: CommonService,
        private implementerService: ImplementerService,
        private taskHistoryService: TaskHistoryService,
        private taskResourceExpenseService: TaskResourceExpenseService) {
    }

    //#endregion

    //#region Methods

    getTaskStatuses() {
        this.commonService
            .getTaskStatuses()
            .pipe(first())
            .subscribe(
                items => {
                    if (items) {
                        this.taskStatuses = items.map(function (item) {
                            let mapItem = new ListItem();
                            mapItem.id = item.id;
                            mapItem.name = item.name;
                            mapItem.isChecked = false;
                            return mapItem;
                        });
                    }
                },
                error => {
                    console.log(error);
                }
            );
    }

    getImplementer() {
        this.implementerService
        .getByTask(this.task.id)
        .pipe(first())
        .subscribe(
            implementer => {
                if (implementer) {
                    this.implementer = implementer;
                    this.selectedStatus = this.implementer.status;
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    changeTaskStatus() {
        this.implementer.status = this.selectedStatus
        this.implementerService
        .update(this.implementer)
        .pipe(first())
        .subscribe(
            result => {
                if (result) {
                    this.updateStatus();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    addTaskHistory() {
        let taskHistory = new TaskHistory();
        taskHistory.editorId = this.implementer.implementerId;
        taskHistory.taskId = this.task.id;
        taskHistory.created = moment().utc().format('YYYY-MM-DDTHH:mm:ss');
        taskHistory.status = this.implementer.status;
        taskHistory.historyOperationType = HistoryOperationType.StatusСhangeByImplementer;
        taskHistory.description = this.description;
        this.taskHistoryService
        .add(taskHistory)
        .pipe(first())
        .subscribe(
            result => {
                if (result) {
                    if(this.needAddResourceExpense){
                        this.addResourceExpense();
                    }

                    this.description = '';
                    if (this.onStatusChanged)
                        this.onStatusChanged.emit();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    addResourceExpense() {
        let taskResourceExpense = new TaskResourceExpense();
        taskResourceExpense.taskResourceId = this.task.taskResource.id;
        taskResourceExpense.count = this.resourceExpense;
        this.taskResourceExpenseService
        .add(taskResourceExpense)
        .pipe(first())
        .subscribe(
            result => {
                if (result) {
                    this.needAddResourceExpense = false;
                    this.resourceExpense = 0;
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    updateStatus() {
        this.taskService
        .updateStatus(this.task.id)
        .pipe(first())
        .subscribe(
            result => {
                if (result) {
                    this.addTaskHistory();
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    //#endregion
}