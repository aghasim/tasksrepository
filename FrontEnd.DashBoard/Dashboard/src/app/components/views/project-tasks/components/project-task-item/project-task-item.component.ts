import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/app/models';
import { TaskService } from 'src/app/services';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { first } from 'rxjs/operators';

@Component({
    selector: 'project-task-item',
    templateUrl: 'project-task-item.component.html',
    styleUrls: ['project-task-item.component.css'],
})

export class ProjectTaskItemComponent {

    @Input() itemData : Task;
    @Output() onDelete = new EventEmitter();

    public visibleTaskChildStatus: boolean = false;
    
    //#region Constructor

    public constructor(
        private taskService: TaskService,
        public ngxSmartModalService: NgxSmartModalService) { }

    //#endregion

    toggle(){
        this.visibleTaskChildStatus = !this.visibleTaskChildStatus;
    }

    removeTask(taskId: string) {
        this.taskService
            .delete(taskId)
            .pipe(first())
            .subscribe(
                result => {
                    if (result) {
                        this.onDelete.emit();
                    }
                },
                error => {
                    console.log(error);
                }
            )
    }
}