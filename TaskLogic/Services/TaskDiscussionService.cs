﻿using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.AmrApp.Repository;
using ProjectWorks.Contracts.Intefaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Services
{
    public class TaskDiscussionService : ITaskDiscussion
    {
        readonly AmrAppRepository amrApp;
        public TaskDiscussionService(AmrAppRepository amrApp)
        {
            this.amrApp = amrApp;
        }

        public async Task<ICollection<TaskDiscussionOfTheTasks>> GetAll(int filialId)
        {
            try
            {
                return await amrApp.Context.TaskDiscussionOfTheTasks.ToListAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async Task<TaskDiscussionOfTheTasks> Get(int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskDiscussionOfTheTasks.FirstOrDefaultAsync(x => x.Id == id);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async System.Threading.Tasks.Task Add(TaskDiscussionOfTheTasks discussion, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                amrApp.Context.TaskDiscussionOfTheTasks.Add(discussion);
            }
            catch(Exception ex)
            {
                throw;
            }

        }

        public async System.Threading.Tasks.Task Delete (int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                var discussion = await amrApp.Context.TaskDiscussionOfTheTasks.FirstOrDefaultAsync(x => x.Id == id);
                discussion.IsDelete = true;
                await amrApp.Context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

    }
}
