import * as moment from 'moment';
import { Component, Input, OnInit, Output } from '@angular/core';
import { Task } from 'src/app/models';

@Component({
    selector: 'project-schedule-item',
    templateUrl: 'project-schedule-item.component.html',
})

export class ProjectScheduleItemComponent {
    //#region Fields 

    @Input() task: Task;

    public displayChild: boolean;


    //#endregion

    //#region Constructor

    public constructor() {
        this.displayChild = false;
    }

    //#endregion

    //#region Methods
    toggle(){
        this.displayChild = !this.displayChild;
    }
    //#endregion
}