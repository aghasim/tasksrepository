﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Helpers.Date;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkTaskRepository
{
    public class WorkTaskRepository : GenericRepository<WorkTask>, IWorkTaskRepository
    {
        #region Constructor

        public WorkTaskRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<List<WorkTask>> GetAll(Guid projectId)
        {
            var tasks = await _entities
                .Include(x => x.WorkProject)
                .Include(x => x.WorkTaskImplementers)
                .Include(x => x.WorkTaskReviewers)
                .Where(task => task.WorkProjectId == projectId && !task.IsDeleted)
                .OrderBy(entity => entity.Created)
                .ToListAsync();

            return tasks ?? new List<WorkTask>();
        }

        public async Task<List<WorkTask>> GetAllByDates(Guid projectId, DateTime start, DateTime end)
        {
            var tasks = await _entities
                .Include(x => x.WorkProject)
                .Include(x => x.WorkTaskImplementers)
                .Include(x => x.WorkTaskReviewers)
                .Include(x => x.WorkTaskHistory)
                .Where(task =>
                    task.WorkProjectId == projectId && !task.IsDeleted && task.WorkTaskHistory.Any(x => x.Created >= start && x.Created <= end))
                .OrderBy(entity => entity.Created)
                .ToListAsync();

            return tasks ?? new List<WorkTask>();
        }

        public async Task<List<WorkTask>> GetAllByFilter(TokenData tokenData, FilterDatePeriodType filterDatePeriodType = FilterDatePeriodType.Month,
            FilterTaskType filterTaskType = FilterTaskType.Incoming)
        {
            var query = _entities
                .Include(x => x.WorkProject)
                .Include(x => x.WorkTaskImplementers)
                .Include(x => x.WorkTaskReviewers)
                .Where(task => !task.IsDeleted && task.FilialId == tokenData.FilialId);

            var datePeriod = DateHelper.GetDatePeriod(filterDatePeriodType);
            if (datePeriod != null)
                query = query.Where(x => x.StartDate >= datePeriod.Start && x.StartDate <= datePeriod.End);

            switch (filterTaskType)
            {
                case FilterTaskType.Controling:
                    query = query.Where(controling => controling.WorkTaskReviewers.Any(x => x.ReviewerId == tokenData.UserId));
                    break;
                case FilterTaskType.Incoming:
                    query = query.Where(incoming => incoming.WorkTaskImplementers.Any(x => x.ImplementerId == tokenData.UserId));
                    break;
                case FilterTaskType.Outgoing:
                    query = query.Where(outgoing => outgoing.CreatorId == tokenData.UserId);
                    break;
                case FilterTaskType.ActiveControling:
                    query = query.Where(controling =>
                        controling.WorkTaskReviewers.Any(x => x.ReviewerId == tokenData.UserId) &&
                        controling.Status != WorkTaskStatus.Done && controling.Status != WorkTaskStatus.Completed);
                    break;
                case FilterTaskType.ActiveIncoming:
                    query = query.Where(controling =>
                        controling.WorkTaskImplementers.Any(x => x.ImplementerId == tokenData.UserId) &&
                        controling.Status != WorkTaskStatus.Done && controling.Status != WorkTaskStatus.Completed);
                    break;
                case FilterTaskType.ActiveOutgoing:
                    query = query.Where(controling =>
                        controling.CreatorId == tokenData.UserId &&
                        controling.Status != WorkTaskStatus.Done && controling.Status != WorkTaskStatus.Completed);
                    break;
                case FilterTaskType.NewControling:
                    query = query.Where(controling =>
                        controling.WorkTaskReviewers.Any(x => x.ReviewerId == tokenData.UserId) &&
                        controling.Status == WorkTaskStatus.New);
                    break;
                case FilterTaskType.NewIncoming:
                    query = query.Where(controling =>
                        controling.WorkTaskImplementers.Any(x => x.ImplementerId == tokenData.UserId) &&
                        controling.Status == WorkTaskStatus.New);
                    break;
                case FilterTaskType.NewOutgoing:
                    query = query.Where(controling =>
                        controling.CreatorId == tokenData.UserId &&
                        controling.Status == WorkTaskStatus.New);
                    break;
                case FilterTaskType.Tomorrow:
                    query = query.Where(x => x.CompletionDate == DateTime.UtcNow.Date.AddDays(1));
                    break;
                case FilterTaskType.Today:
                    query = query.Where(x => x.CompletionDate == DateTime.UtcNow.Date);
                    break;
                case FilterTaskType.Past:
                    query = query.Where(x => x.CompletionDate < DateTime.UtcNow.Date);
                    break;
            }

            var tasks = await query.OrderBy(entity => entity.Created).ToListAsync();
            return tasks ?? new List<WorkTask>();
        }

        public async Task<List<WorkTask>> GetAllActive(TokenData tokenData, Guid projectId)
        {
            var query = _entities
                .Include(x => x.WorkProject)
                .Include(x => x.WorkTaskImplementers)
                .Include(x => x.WorkTaskReviewers)
                .Where(task => !task.IsDeleted && task.FilialId == tokenData.FilialId &&
                    task.Status != WorkTaskStatus.Completed ||
                    task.Status != WorkTaskStatus.Done &&
                    task.WorkTaskImplementers.Any(implementer => implementer.ImplementerId == tokenData.UserId) ||
                    task.WorkTaskReviewers.Any(reviewer => reviewer.ReviewerId == tokenData.UserId));

            if (projectId != Guid.Empty)
                query = query.Where(x => x.WorkProjectId == projectId);

            var tasks = await query.OrderBy(entity => entity.Created).ToListAsync();
            return tasks ?? new List<WorkTask>();
        }

        public async Task<List<WorkTask>> GetAllControling(TokenData tokenData, Guid projectId)
        {
            var query = _entities
                .Include(x => x.WorkProject)
                .Include(x => x.WorkTaskImplementers)
                .Include(x => x.WorkTaskReviewers)
                .Where(task => !task.IsDeleted && task.FilialId == tokenData.FilialId && 
                    task.WorkTaskReviewers.Any(reviewer => reviewer.ReviewerId == tokenData.UserId));

            if (projectId != Guid.Empty)
                query = query.Where(x => x.WorkProjectId == projectId);

            var tasks = await query.OrderBy(entity => entity.Created).ToListAsync();
            return tasks ?? new List<WorkTask>();
        }

        public async Task<List<WorkTask>> GetAllIncoming(TokenData tokenData, Guid projectId)
        {
            var query = _entities
                .Include(x => x.WorkProject)
                .Include(x => x.WorkTaskImplementers)
                .Include(x => x.WorkTaskReviewers)
                .Where(task => !task.IsDeleted && task.FilialId == tokenData.FilialId &&
                               task.WorkTaskImplementers.Any(implementer =>
                                   implementer.ImplementerId == tokenData.UserId));

            if (projectId != Guid.Empty)
                query = query.Where(x => x.WorkProjectId == projectId);

            var tasks = await query.OrderBy(entity => entity.Created).ToListAsync();
            return tasks ?? new List<WorkTask>();
        }

        public async Task<List<WorkTask>> GetAllOutgoing(TokenData tokenData, Guid projectId)
        {
            var query = _entities
                .Include(x => x.WorkProject)
                .Include(x => x.WorkTaskImplementers)
                .Include(x => x.WorkTaskReviewers)
                .Where(task =>
                    !task.IsDeleted && task.FilialId == tokenData.FilialId && task.CreatorId == tokenData.UserId);

            if (projectId != Guid.Empty)
                query = query.Where(x => x.WorkProjectId == projectId);

            var tasks = await query.OrderBy(entity => entity.Created).ToListAsync();
            return tasks ?? new List<WorkTask>();
        }

        public async Task<List<WorkTask>> GetChildByParentId(Guid parentTaskId)
        {
            return await _entities
                .Where(task => task.ParentId == parentTaskId && !task.IsDeleted).ToListAsync();
        }

        public async Task<int> GetChildTaskCount(Guid taskId, bool onlyCompleted = false)
        {
            //var query = _entities.Where(task => task.ParentId == taskId && !task.IsDeleted);
            //if (onlyCompleted)
            //    query = query.Where(task => task.Status == WorkTaskStatus.Completed);

            //return await query.AsNoTracking().CountAsync();
            return 0;
        }

        public async Task<decimal> GetTaskExpense(Guid taskId)
        {
            return await _entities
                .Where(task => task.Id == taskId && !task.IsDeleted).AsNoTracking()
                .SumAsync(task => task.WorkTaskResource.WorkTaskResourceExpenses.Count);
        }

        public async Task<decimal> GetChildTaskExpenses(Guid parentTaskId)
        {
            return await _entities
                .Where(task => task.ParentId == parentTaskId && !task.IsDeleted).AsNoTracking()
                .SumAsync(task => task.WorkTaskResource.WorkTaskResourceExpenses.Count);
        }

        #endregion
    }
}