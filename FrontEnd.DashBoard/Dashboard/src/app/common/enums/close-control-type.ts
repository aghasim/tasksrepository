export enum CloseControlType {
  NoAction = 1,
  Closing,
  ClosingWithJobAcceptance
}