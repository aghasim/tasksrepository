﻿namespace ProjectWorks.AmrApp.Dtos
{
    public class AppUsersDto : AmrDtoBase
    {
        #region Properties

        public string Fio { get; set; }

        #endregion
    }
}