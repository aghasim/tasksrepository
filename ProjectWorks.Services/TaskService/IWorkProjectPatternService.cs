﻿using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkProjectPatternService : IServiceBase<WorkProjectPattern>
    {
        string Create(string workProjectId, string name);
    }
}
