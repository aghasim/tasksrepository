﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Контроль выполнения
    /// </summary>
    public enum CommunicationSettingsType
    {
        /// <summary>
        ///     С групповым обсуждением
        /// </summary>
        [Display(Name = "С групповым обсуждением")]
        WithGroupDiscussion = 1,
        /// <summary>
        ///     С личным обсуждением
        /// </summary>
        [Display(Name = "С личным обсуждением")]
        WithPersonalDiscussion,
        /// <summary>
        ///     Без обсуждения
        /// </summary>
        [Display(Name = "Без обсуждения")]
        WithoutDiscussion
    }
}