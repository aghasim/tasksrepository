import { Component, OnInit, Input } from "@angular/core";
import { Task, ListItem } from 'src/app/models';
import { CommonService } from 'src/app/services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'task-execution-order',
  templateUrl: "task-execution-order.component.html",
  styleUrls: ["task-execution-order.component.css"],
})
export class TaskExecutionOrderComponent implements OnInit {
  //#region Fields

  @Input() task: Task;
  public executionOrderTypes: ListItem[];

  //#endregion

  constructor(
    private commonService: CommonService
  ) {}

  //#region Methods 

  ngOnInit() {
    this.getTaskExecutionOrderTypes();
  }

  getTaskExecutionOrderTypes() {
    this.commonService
      .getTaskExecutionOrderTypes()
      .pipe(first())
      .subscribe(
        items => {
          if (items) {
            this.executionOrderTypes = items.map(function(item) {
              let mapItem = new ListItem();
              mapItem.id = item.id;
              mapItem.name = item.name;
              mapItem.isChecked = false;
              return mapItem;
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  //#endregion
}