﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskImplementerRepository
{
    public interface IWorkTaskImplementerRepository : IGenericRepository<WorkTaskImplementer>
    {
        #region Methods
        Task<WorkTaskImplementer> GetByTask(TokenData tokenData, Guid taskId);

        Task<List<WorkTaskImplementer>> GetAll(Guid taskId);

        #endregion
    }
}