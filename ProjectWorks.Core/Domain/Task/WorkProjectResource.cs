﻿using System;
using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Ресурс проекта
    /// </summary>
    public class WorkProjectResource : EntityBase
    {
        #region Properties

        // Тип ресурса
        public WorkTaskResourceType Type { get; set; }

        // Количество ресурса на выполнение
        [Required]
        [Range(0, double.MaxValue)]
        public decimal Count { get; set; }

        // Статья расхода из а2
        public int? ExpenditureId { get; set; }

        #endregion
    }
}