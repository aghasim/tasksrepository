﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Conversation_table_change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Room",
                table: "Conversations");

            migrationBuilder.AddColumn<int>(
                name: "RecipientId",
                table: "Conversations",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "WorkTaskId",
                table: "Conversations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Conversations_WorkTaskId",
                table: "Conversations",
                column: "WorkTaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_Conversations_WorkTasks_WorkTaskId",
                table: "Conversations",
                column: "WorkTaskId",
                principalTable: "WorkTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Conversations_WorkTasks_WorkTaskId",
                table: "Conversations");

            migrationBuilder.DropIndex(
                name: "IX_Conversations_WorkTaskId",
                table: "Conversations");

            migrationBuilder.DropColumn(
                name: "RecipientId",
                table: "Conversations");

            migrationBuilder.DropColumn(
                name: "WorkTaskId",
                table: "Conversations");

            migrationBuilder.AddColumn<string>(
                name: "Room",
                table: "Conversations",
                nullable: true);
        }
    }
}
