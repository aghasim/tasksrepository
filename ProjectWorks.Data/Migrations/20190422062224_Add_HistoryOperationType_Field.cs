﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Add_HistoryOperationType_Field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HistoryOperationType",
                table: "WorkTaskHistories",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HistoryOperationType",
                table: "WorkTaskHistories");
        }
    }
}
