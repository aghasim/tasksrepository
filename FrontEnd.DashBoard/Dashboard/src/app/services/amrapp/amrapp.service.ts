import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";

import { environment } from "../../../environments/environment";

@Injectable({ providedIn: "root" })
export class AmrappService {
  private apiUrl: string = environment.apiUrl + "ampapp/";

  constructor(private http: HttpClient) { }

  getUser(userId: number) {
    return this.http
      .get<any>(this.apiUrl + "users/" + userId)
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getAllUsers() {
    return this.http
      .get<any>(this.apiUrl + "users")
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getUsersByObjects(ids: number[]) {
    return this.http
      .post<any>(this.apiUrl + "usersbyobjects", { ids })
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getUsersByDepartments(ids: number[]) {
    return this.http
      .post<any>(this.apiUrl + "usersbydepartments", { ids })
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getUsersByRoles(ids: number[]) {
    return this.http
      .post<any>(this.apiUrl + "usersbyroles", { ids })
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getObject(objectId: number) {
    return this.http
      .get<any>(this.apiUrl + "objects/" + objectId)
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getAllObjects() {
    return this.http
      .get<any>(this.apiUrl + "objects")
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getAllObjectsByTypes(ids: number[]) {
    return this.http
      .post<any>(this.apiUrl + "objects", { ids })
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getObjectType(objectTypeId: number) {
    return this.http
      .get<any>(this.apiUrl + "objecttypes/" + objectTypeId)
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getAllObjectTypes() {
    return this.http
      .get<any>(this.apiUrl + "objecttypes")
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getDepartment(objectTypeId: number) {
    return this.http
      .get<any>(this.apiUrl + "departments/" + objectTypeId)
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getAllDepartments() {
    return this.http
      .get<any>(this.apiUrl + "departments")
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getRole(roleId: number) {
    return this.http
      .get<any>(this.apiUrl + "roles/" + roleId)
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getAllRoles() {
    return this.http
      .get<any>(this.apiUrl + "roles")
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }

  getAllExpenditure() {
    return this.http
      .get<any>(this.apiUrl + "expenditures")
      .pipe(
        map(result => {
          return result.data;
        })
      )
      .pipe(
        catchError((err, caught) => {
          return null;
        })
      );
  }
}
