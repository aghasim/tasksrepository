﻿using System;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Проверяющий задачу
    /// </summary>
    public class WorkTaskReviewer : EntityBase
    {
        #region Properties

        // Проверяющий, пользователь из А2
        public int ReviewerId { get; set; }

        // Задача Id
        public Guid WorkTaskId { get; set; }

        // Задача
        public virtual WorkTask WorkTask { get; set; }

        #endregion
    }
}