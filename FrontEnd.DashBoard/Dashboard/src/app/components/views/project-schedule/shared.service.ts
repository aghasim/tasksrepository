import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Task } from 'src/app/models';

@Injectable()
export class SharedService {
  
  private taskList = new BehaviorSubject<Task[]>(null);
  
  currentData = this.taskList.asObservable();

  updateData(tasks: Task[]){
    this.taskList.next(tasks);
  }

}
