import { TaskStatus } from './task.status';

export class TaskStatusListItem {
    public userId: string;
    public userName: string;
    public userAvatarUrl: string; 
    public actualStatus: number;
    public statusList: TaskStatus[];
}