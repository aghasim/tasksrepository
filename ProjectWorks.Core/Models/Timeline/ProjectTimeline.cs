﻿using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Models.Timeline
{
    public class ProjectTimeline
    {
        #region Properties

        public int Day { get; set; }

        public int Duration { get; set; }

        public WorkProjectStatus Status { get; set; }

        public string StatusName { get; set; }

        #endregion
    }
}