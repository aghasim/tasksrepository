﻿using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Core.Models.Response
{
    public class WorkTaskResourceExpenseResponse
    {
        #region Properties

        public WorkTaskResourceExpenseDto Expense { get; set; }

        #endregion
    }
}