import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({ 
  selector: 'wizzard-buttons', 
  templateUrl: "wizzard-buttons.component.html" })
export class WizzardButtonsComponent {
  @Input() isLastStep: boolean;
  @Output() onForward = new EventEmitter();
  @Output() onBackward = new EventEmitter();
  @Output() onSave = new EventEmitter();

  forward(){
    this.onForward.emit();
  }

  backward(){
    this.onBackward.emit();
  }

  save(){
    this.onSave.emit();
  }
}