﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using ProjectWorks.Core.Common;

namespace ProjectWorks.Core.Filters
{
    public class FilialActionAttribute : Attribute, IAsyncActionFilter
    {
        #region Methods

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var userClaim = context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
            var filialClaim = context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
            var tokenData = new TokenData
            {
                UserId = userClaim != null ? int.Parse(userClaim.Value) : 0,
                FilialId = filialClaim != null ? int.Parse(filialClaim.Value) : 0
            };

            context.ActionArguments["tokenData"] = tokenData;
            await next();
        } 

        #endregion
    }
}