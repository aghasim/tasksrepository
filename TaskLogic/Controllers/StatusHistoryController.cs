﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using ProjectWorks.Data.Extensions;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Contracts.Intefaces;
using System.Collections.Generic;

namespace ProjectWorks.Contracts.Controllers
{
    [Route("api/Contracts/History")]
    public class StatusHistoryController : Controller
    {
        private readonly ITaskStatusHistories _taskService;

        public StatusHistoryController(ITaskStatusHistories service)
        {
            _taskService = service;
        }

        [HttpGet]
        public async Task<ICollection<TaskStatusHistories>> GetAll(int filialId)
        {
            return await _taskService.GetAll(filialId);
        }

        //[HttpGet]
        //public async Task<TaskStatusHistories> Get(int id, int filialId)
        //{
        //    return await _taskService.Get(id, filialId);
        //}

        [HttpPost("Add")]
        public async System.Threading.Tasks.Task Add(TaskStatusHistories history, int filialId)
        {
            await _taskService.Add(history, filialId);
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task Delete(int id, int filialId)
        {
            await _taskService.Delete(id, filialId);
        }

    }
}
