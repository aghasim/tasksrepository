﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using Serilog;
using Constants = ProjectWorks.Core.Common.Constants;

namespace ProjectWorks.AmrApp.Services
{
    public class PropertyValueService : IPropertyValueService
    {
        #region Fields

        private readonly IAmrAppRepository _amrApp;

        private readonly ICacheManager _cacheManager;

        #endregion

        #region Constructor

        public PropertyValueService(IAmrAppRepository amrApp,
            ICacheManager cacheManager)
        {
            _amrApp = amrApp;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public async Task<PropertyValues> Get(int filialId, int objectId, int id)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.PROPERTYVALUE_CURRENT_KEY, filialId, id);
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.PropertyValues
                        .FirstOrDefaultAsync(x => x.Id == id && x.Object1Id == objectId));
            }

            return null;
        }

        public async Task<IEnumerable<PropertyValues>> GetByObjectId(int filialId, int objectId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.PROPERTYVALUE_ALL_KEY, filialId, objectId);
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.PropertyValues
                        .Where(x => x.Object1Id == objectId).ToListAsync());
            }

            return null;
        }

        public async Task AddPropertyValue(TokenData tokenData, PropertyValues propertyValue)
        {
            _amrApp.SetConnString(tokenData.FilialId);
            if (_amrApp.Context != null)
            {
                Log.Information(string.Format(Constants.LogConstants.PROPERTYVALUESERVICE_ADDOBJECT, tokenData.FilialId));
                _amrApp.Context.PropertyValues.Add(propertyValue);
                await _amrApp.Context.SaveChangesAsync();
            }
        }

        public async Task<Properties> GetDefaultProperties(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.PROPERTY_DEFAULT_KEY, filialId);
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.Properties.FirstOrDefaultAsync(x =>
                        x.PropertyName == "Тип" && x.ObjectTypeId == Constants.A2Constants.ObjTypeСosts));
            }

            return null;
        }

        #endregion
    }
}