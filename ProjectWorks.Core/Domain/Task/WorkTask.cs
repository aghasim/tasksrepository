﻿using System;
using System.Collections.Generic;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Задача
    /// </summary>
    public class WorkTask : ProjectTaskEntityBase
    {
        #region Properties

        // Статус задачи
        public WorkTaskStatus Status { get; set; }

        // Родительская задача
        public Guid ParentId { get; set; } = Guid.Empty;

        // Ресурс задачи Id
        public Guid WorkTaskResourceId { get; set; }

        // Ресурс задачи
        public virtual WorkTaskResource WorkTaskResource { get; set; }

        // Проект Id
        public Guid WorkProjectId { get; set; }

        // Проект
        public virtual WorkProject WorkProject { get; set; }

        // Порядок исполнения задачи
        public virtual WorkTaskExecutionOrder WorkTaskExecutionOrder { get; set; }

        // Контроль выполнения задачи
        public CloseControlType CloseControlType { get; set; }

        // Настройки коммуникации
        public CommunicationSettingsType SettingCommunicationType { get; set; }

        // Исполняющие
        public virtual ICollection<WorkTaskImplementer> WorkTaskImplementers { get; set; } = new List<WorkTaskImplementer>();

        // Проверяющие
        public virtual ICollection<WorkTaskReviewer> WorkTaskReviewers { get; set; } = new List<WorkTaskReviewer>();

        // История
        public virtual ICollection<WorkTaskHistory> WorkTaskHistory { get; set; } = new List<WorkTaskHistory>();

        #endregion
    }
}