﻿using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository;
using ProjectWorks.Data.Repository.WorkProjectPatternRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectWorks.Task.Services
{
    public class WorkProjectPatternService : ServiceBase<WorkProjectPattern>, IWorkProjectPatternService
    {
        public WorkProjectPatternService(ICacheManager cacheManager, IWorkProjectPatternRepository genericRepository, IUserService userService) 
            :base(cacheManager, genericRepository, userService)
        {

        }


        public string Create(string workProjectId, string name)
        {
            var item = new WorkProjectPattern
            {
                Id = Guid.NewGuid(),
                WorkProjectId = Guid.Parse(workProjectId),
                FilialId = _userService.CurrentUserFilialId,
                Name = name
            };
            Add(item);
            return item.Id.ToString();
        }



    }
}
