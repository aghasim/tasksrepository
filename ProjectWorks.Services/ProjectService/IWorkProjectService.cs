﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Project;

namespace ProjectWorks.Services.ProjectService
{
    public interface IWorkProjectService : IServiceBase<WorkProject>
    {
        #region Methods

        Task<WorkProject> Get(TokenData tokenData, Guid id);

        Task Add(TokenData tokenData, WorkProject entity);

        void Update(TokenData tokenData, WorkProject entity);

        Task Delete(TokenData tokenData, Guid id);

        Task<List<WorkProjectDto>> GetAll(TokenData tokenData);

        Task<List<WorkProjectDto>> GetAllByMonth(TokenData tokenData, int year, int month);

        Task UpdateStatus(TokenData tokenData, Guid id);

        #endregion
    }
}