﻿using System;
using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Dtos.Task
{
    public class WorkTaskTemplateDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Название
        public string Name { get; set; }

        // Описание
        public string Description { get; set; }

        // Ответственный, пользователь из А2
        public int? ResponsibleId { get; set; }

        // Тип ресурса
        public WorkTaskResourceType? WorkTaskResourceType { get; set; }

        // Количество ресурса на выполнение
        public decimal? ResourceCount { get; set; }

        // Тип порядка исполнения задачи
        public WorkTaskExecutionOrderType WorkTaskExecutionOrderType { get; set; }

        // Порядок исполнения задачи
        public int? ExecutionOrder { get; set; }

        #endregion
    }
}