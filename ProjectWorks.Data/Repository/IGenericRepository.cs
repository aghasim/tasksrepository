﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain;

namespace ProjectWorks.Data.Repository
{
    public interface IGenericRepository<TEntity> where TEntity : EntityBase
    {
        #region Methods

        Task<TEntity> Get(Guid id);

        TResult Query<T, TResult>(Func<IQueryable<T>, TResult> query) where T : EntityBase;

        Task Insert(TEntity entity);

        Task Update(TEntity entity);

        Task Delete(Guid id);

        Task Save();

        #endregion
    }
}