﻿using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.AmrApp.Repository;
using ProjectWorks.AmrApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ProjectWorks.Contracts.Intefaces;

namespace ProjectWorks.Contracts.Services
{
    public class TaskDbModelService : ITaskDbModel
    {
        readonly AmrAppRepository amrApp;


        public TaskDbModelService(AmrAppRepository amrApp)
        {
            this.amrApp = amrApp;
        }

        public async Task<ICollection<TaskDbModels>> GetAll(int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskDbModels.ToListAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async Task<TaskDbModels> Get(int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskDbModels.FirstOrDefaultAsync(x => x.Id == id);
            }
            catch(Exception ex)
            {
                throw;
            }

        }

        public async System.Threading.Tasks.Task Add(TaskDbModels task, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                amrApp.Context.TaskDbModels.Add(task);
                await amrApp.Context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async System.Threading.Tasks.Task Delete (int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                var task = await amrApp.Context.TaskDbModels.FirstOrDefaultAsync(x => x.Id == id);
                task.IsDelete = true;
                await amrApp.Context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw;
            }

        }

        public async System.Threading.Tasks.Task Update(int id, TaskDbModels newTask)
        {
        }


    }
}
