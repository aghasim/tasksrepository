﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Project;
using ProjectWorks.Core.Filters;
using ProjectWorks.Core.Helpers.Project;
using ProjectWorks.Core.Models.Response;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.ProjectService;

namespace ProjectWorks.Project.Controllers
{
    [Authorize]
    [Route("api/projects")]
    public class ProjectController : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        private readonly IWorkProjectService _projectService;

        #endregion

        #region Constructor

        public ProjectController(IMapper mapper, IWorkProjectService projectService)
        {
            _mapper = mapper;
            _projectService = projectService;
        }

        #endregion

        #region Methods

        [FilialAction]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(TokenData tokenData, Guid id)
        {
            var item = await _projectService.Get(tokenData, id);
            var dto = _mapper.Map<WorkProjectDto>(item);
            return this.Success(dto);
        }

        [FilialAction]
        [HttpGet]
        public async Task<IActionResult> GetAll(TokenData tokenData)
        {
            try
            {
                var result = new List<WorkProjectDto>();
                var dtos = await _projectService.GetAll(tokenData);
                ProjectHelper.GetProjectTree(dtos, result, default(Guid));
                return this.Success(result);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpGet("allbymonth/{year}/{month}")]
        public async Task<IActionResult> GetAllByMonth(TokenData tokenData, int year, int month)
        {
            try
            {
                var result = new List<WorkProjectDto>();
                var dtos = await _projectService.GetAllByMonth(tokenData, year, month);
                ProjectHelper.GetProjectTree(dtos, result, default(Guid));
                return this.Success(result);
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpPost("add")]
        public async Task<IActionResult> Add(TokenData tokenData, [FromBody] WorkProjectResponse response)
        {
            try
            {
                var item = _mapper.Map<WorkProject>(response.Project);

                item.CreatorId = tokenData.UserId;
                item.FilialId = tokenData.FilialId;
                item.Status = WorkProjectStatus.New;

                await _projectService.Add(tokenData, item);
                return this.Success();
            }
            catch (Exception exception)
            {
                return this.Error(exception);
            }
        }

        [FilialAction]
        [HttpPut("update")]
        public async Task<IActionResult> Update(TokenData tokenData, [FromBody] WorkProjectResponse response)
        {
            var item = await _projectService.Get(response.Project.Id);
            if (item != null)
            {
                item.Name = response.Project.Name;
                item.Description = response.Project.Description;
                item.ResponsibleId = response.Project.ResponsibleId;
                item.StartDate = response.Project.StartDate;
                item.CompletionDate = response.Project.CompletionDate;

                item.WorkProjectResource.Type = response.Project.ProjectResource.Type;
                item.WorkProjectResource.Count = response.Project.ProjectResource.Count;

                _projectService.Update(tokenData, item);
                return this.Success();
            }

            return this.Error($"Project {response.Project.Id} not found!");
        }

        [FilialAction]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(TokenData tokenData, Guid id)
        {
            await _projectService.Delete(tokenData, id);
            return this.Success();
        }

        #endregion
    }
}