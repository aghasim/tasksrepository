﻿using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.AmrApp.Repository
{
    public class AmrAppRepository : IAmrAppRepository
    {
        #region Fields

        private readonly AmrAmmContextFactory _amrAmmContextFactory;

        private readonly IChangeConnectionString _connString; 

        #endregion

        #region Properties

        public aspnetsmrContext Context { get; set; }

        #endregion

        #region Constructor

        public AmrAppRepository(
            IChangeConnectionString connString, 
            AmrAmmContextFactory amrAmmContextFactory)
        {
            _connString = connString;
            _amrAmmContextFactory = amrAmmContextFactory;
        }

        #endregion

        #region Methods

        public void SetConnString(int filialId)
        {
            var connName = _connString.GetConnStringName(filialId);
            if (!string.IsNullOrEmpty(connName))
                Context = _amrAmmContextFactory.GetDbContext(connName);
        } 

        #endregion
    }
}