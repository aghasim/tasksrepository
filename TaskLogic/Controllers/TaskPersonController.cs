﻿using Microsoft.AspNetCore.Mvc;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Contracts.Intefaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Controllers
{
    [Route ("api/Contracts/Persons")]
    public class TaskPersonController : Controller
    {
        private readonly ITaskControllingPerson _taskService;

        public TaskPersonController(ITaskControllingPerson tasksServices)
        {
            _taskService = tasksServices;

        }

        [HttpGet]
        public async Task<ICollection<TaskControllingPersons>> GetAll(int filialId)
        {
            try
            {
                return await _taskService.GetAll(filialId);
            }
            catch(Exception)
            {
                throw;
            }
        }

        //[HttpGet]
        //public async Task<TaskControllingPersons> Get(int id, int filialId)
        //{
        //    return await _taskService.Get(id, filialId);
        //}

        [HttpPost("Add")]
        public async  System.Threading.Tasks.Task Add(TaskControllingPersons person, int filialId)
        {
            await _taskService.Add(person, filialId);
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task Delete (int id, int filialId)
        {
            await _taskService.Delete(id, filialId);
        }
    }
}
