﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Базовая сущность проекта/задачи
    /// </summary>
    public class ProjectTaskEntityBase : FilialEntityBase
    {
        #region Properties

        // Ответственный, пользователь из А2
        [Required]
        public int ResponsibleId { get; set; }

        [NotMapped]
        // Ответственный, пользователь из А2
        public string ResponsibleName { get; set; }

        // Дата начало
        [DataType(DataType.DateTime)]
        public DateTime StartDate { get; set; }

        // Дата завершения
        [DataType(DataType.DateTime)]
        public DateTime CompletionDate { get; set; }

        // Кто создал, пользователь из А2
        [Required]
        public int CreatorId { get; set; }

        #endregion
    }
}