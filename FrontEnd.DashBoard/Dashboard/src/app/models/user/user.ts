export class User {
  public id: number;
  public username: string;
  public role: string;
  public filialId: number;
  public token: string;
}