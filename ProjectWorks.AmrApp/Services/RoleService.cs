﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using Serilog;
using Constants = ProjectWorks.Core.Common.Constants;

namespace ProjectWorks.AmrApp.Services
{
    public class RoleService : IRoleService
    {
        #region Fields

        private readonly IAmrAppRepository _amrApp;

        private readonly ICacheManager _cacheManager;

        #endregion

        #region Constructor

        public RoleService(IAmrAppRepository amrApp,
            ICacheManager cacheManager)
        {
            _amrApp = amrApp;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public async Task<AppFuncRoles> Get(int filialId, int id)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.ROLE_CURRENT_KEY, filialId, id);
                Log.Information(string.Format(Constants.LogConstants.ROLESERVICE_GETROLE, filialId, id));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.AppFuncRoles.FirstOrDefaultAsync(x => x.Id == id && x.NotDel));
            }

            return null;
        }

        public async Task<IEnumerable<AppFuncRoles>> GetByFilialId(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.ROLE_ALL_KEY, filialId);
                Log.Information(string.Format(Constants.LogConstants.ROLESERVICE_GETROLES, filialId));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.AppFuncRoles.Where(role => role.NotDel).ToListAsync());
            }

            return null;
        }

        #endregion
    }
}