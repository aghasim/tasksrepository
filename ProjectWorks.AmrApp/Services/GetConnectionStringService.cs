﻿using System;
using System.Collections.Generic;
using ProjectWorks.AmrApp.Interfaces;

namespace ProjectWorks.AmrApp.Services
{
    public class GetConnectionStringService : IChangeConnectionString
    {
        #region Methods

        public string GetConnStringName(int filialId)
        {
            var connString = new Dictionary<int, string>
            {
                {0, "DefaultConnection"},
                {6, "NchelnyConnection"},
                {11, "SmrConnection"},
                {5, "SrtvConnection"},
                {10, "AlmaConnection"},
                {8, "BishkekConnection"},
                {7, "EktbConnection"},
                {12, "KazanConnection"},
                {14, "MskConnection"},
                {15, "MowConnection"},
                {18, "NnvgConnection"},
                {19, "NskaConnection"},
                {3, "OrnConnection"},
                {1, "DmtrgConnection"},
                {4, "RstvConnection"},
                {16, "Rostov2Connection"},
                {17, "SptbConnection"},
                {9, "UfaConnection"}
            };

            try
            {
                return connString.ContainsKey(filialId) ? connString[filialId] : string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        #endregion
    }
}