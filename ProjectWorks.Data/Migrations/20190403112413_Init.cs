﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WorkProjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    FilialId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ResponsibleId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    CompletionDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkProjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkTaskResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Count = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkTaskResourceExpenses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    WorkTaskResourceId = table.Column<Guid>(nullable: false),
                    Count = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskResourceExpenses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkTaskResourceExpenses_WorkTaskResources_WorkTaskResourceId",
                        column: x => x.WorkTaskResourceId,
                        principalTable: "WorkTaskResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkTasks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    FilialId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ResponsibleId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    CompletionDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ParentId = table.Column<Guid>(nullable: false),
                    WorkTaskResourceId = table.Column<Guid>(nullable: false),
                    WorkProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkTasks_WorkProjects_WorkProjectId",
                        column: x => x.WorkProjectId,
                        principalTable: "WorkProjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkTasks_WorkTaskResources_WorkTaskResourceId",
                        column: x => x.WorkTaskResourceId,
                        principalTable: "WorkTaskResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkTaskExecutionOrders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    WorkTaskExecutionOrderType = table.Column<int>(nullable: false),
                    ExecutionOrder = table.Column<int>(nullable: false),
                    WorkTaskId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkTaskExecutionOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkTaskExecutionOrders_WorkTasks_WorkTaskId",
                        column: x => x.WorkTaskId,
                        principalTable: "WorkTasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskExecutionOrders_WorkTaskId",
                table: "WorkTaskExecutionOrders",
                column: "WorkTaskId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkTaskResourceExpenses_WorkTaskResourceId",
                table: "WorkTaskResourceExpenses",
                column: "WorkTaskResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkTasks_WorkProjectId",
                table: "WorkTasks",
                column: "WorkProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkTasks_WorkTaskResourceId",
                table: "WorkTasks",
                column: "WorkTaskResourceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkTaskExecutionOrders");

            migrationBuilder.DropTable(
                name: "WorkTaskResourceExpenses");

            migrationBuilder.DropTable(
                name: "WorkTasks");

            migrationBuilder.DropTable(
                name: "WorkProjects");

            migrationBuilder.DropTable(
                name: "WorkTaskResources");
        }
    }
}
