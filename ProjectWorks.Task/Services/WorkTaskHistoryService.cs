﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkTaskHistoryRepository;
using ProjectWorks.Data.Repository.WorkTaskRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskHistoryService : ServiceBase<WorkTaskHistory>, IWorkTaskHistoryService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskHistoryRepository _workTaskHistoryRepository;

        #endregion

        #region Constructor

        public WorkTaskHistoryService(ICacheManager cacheManager, IWorkTaskHistoryRepository workTaskHistoryRepository, IUserService userService) :
            base(cacheManager, workTaskHistoryRepository, userService)
        {
            _cacheManager = cacheManager;
            _workTaskHistoryRepository = workTaskHistoryRepository;
        }

        #endregion

        #region Methods

        public async Task<List<WorkTaskHistory>> GetAll(Guid taskId)
        {
            var key = string.Format(Constants.CacheConstants.TASKHISTORY_ALL_KEY, taskId);
            return await _cacheManager.Get(key, async () => await _workTaskHistoryRepository.GetAll(taskId));
        }

        public async Task<List<WorkTaskHistory>> GetAllByDate(Guid taskId, DateTime startDate, DateTime endDate)
        {
            return await _workTaskHistoryRepository.GetAllByDate(taskId, startDate, endDate);
        }

        public async Task<List<WorkTaskHistory>> GetAllByDateAndEditor(Guid taskId, int editorId, DateTime startDate, DateTime endDate)
        {
            return await _workTaskHistoryRepository.GetAllByDateAndEditor(taskId, editorId, startDate, endDate);
        }

        #endregion
    }
}