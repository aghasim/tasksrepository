﻿using System;
using System.Collections.Generic;

namespace ProjectWorks.AmrApp.DtoModels
{
    public class RequestModel
    {
        public List<int> Ids { get; set; }
    }
}
