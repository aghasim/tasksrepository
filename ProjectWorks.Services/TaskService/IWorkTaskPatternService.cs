﻿using ProjectWorks.Core.Domain.Task;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkTaskPatternService: IServiceBase<WorkTaskPattern>
    {
        string Create(string workProjectId, string name);
    }
}
