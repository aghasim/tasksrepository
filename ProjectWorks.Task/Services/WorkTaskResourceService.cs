﻿using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkTaskResourceRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskResourceService : ServiceBase<WorkTaskResource>, IWorkTaskResourceService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskResourceRepository _workTaskResourceRepository;

        #endregion

        #region Constructor

        public WorkTaskResourceService(ICacheManager cacheManager, IWorkTaskResourceRepository workTaskResourceRepository, IUserService userService) :
            base(cacheManager, workTaskResourceRepository, userService)
        {
            _cacheManager = cacheManager;
            _workTaskResourceRepository = workTaskResourceRepository;
        }

        #endregion
    }
}