﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Core.Domain.Task
{
    public class WorkProjectPattern : FilialEntityBase
    {
        public Guid WorkProjectId { get; set; }
        public virtual WorkProject WorkProject { get; set; }
    }
}
