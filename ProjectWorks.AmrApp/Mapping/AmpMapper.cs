﻿using AutoMapper;
using ProjectWorks.AmrApp.Dtos;
using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.AmrApp.Mapping
{
    public static class AmpMapper
    {
        #region Methods

        public static void ConfigureMap(IMappingOperationOptions<AppUsers, AppUsersDto> option)
        {
            option.ConfigureMap()
                .ForMember(dest => dest.Id, m => m.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, m => m.MapFrom(src => src.UserName));
        }

        public static void ConfigureMap(IMappingOperationOptions<AppUserDepartments, AppUserDepartmentsDto> option)
        {
            option.ConfigureMap()
                .ForMember(dest => dest.Id, m => m.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, m => m.MapFrom(src => src.Name));
        }

        public static void ConfigureMap(IMappingOperationOptions<AppFuncRoles, AppUserRolesDto> option)
        {
            option.ConfigureMap()
                .ForMember(dest => dest.Id, m => m.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, m => m.MapFrom(src => src.Name));
        }

        public static void ConfigureMap(IMappingOperationOptions<Object1, ObjectDto> option)
        {
            option.ConfigureMap()
                .ForMember(dest => dest.Id, m => m.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, m => m.MapFrom(src => src.ObjectName));
        }

        public static void ConfigureMap(IMappingOperationOptions<ObjectTypes, ObjectTypesDto> option)
        {
            option.ConfigureMap()
                .ForMember(dest => dest.Id, m => m.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, m => m.MapFrom(src => src.Name));
        }

        #endregion
    }
}