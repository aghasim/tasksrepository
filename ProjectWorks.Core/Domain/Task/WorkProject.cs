﻿using System;
using System.Collections.Generic;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Проект
    /// </summary>
    public class WorkProject : ProjectTaskEntityBase
    {
        #region Properties

        // Статус проекта
        public WorkProjectStatus Status { get; set; }

        // Родительский проект
        public Guid ParentId { get; set; } = Guid.Empty;

        // Ресурс проекта Id
        public Guid WorkProjectResourceId { get; set; }

        // Ресурс проекта
        public virtual WorkProjectResource WorkProjectResource { get; set; }

        // Задачи
        public virtual ICollection<WorkTask> WorkTasks { get; set; }

        // История
        public virtual ICollection<WorkProjectHistory> WorkProjectHistory { get; set; } = new List<WorkProjectHistory>();


        #endregion
    }
}