import * as moment from 'moment';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from 'src/app/services';
import { Task } from 'src/app/models';
import { first } from 'rxjs/operators';

@Component({
    selector: 'user-tasks',
    templateUrl: 'user-tasks.component.html',
})

export class UserTasksComponent {
    //#region Fields 
    public tasks: Task[];
    //#endregion

    //#region Constructor

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private taskService: TaskService
    ) {
        this.route.paramMap.subscribe(params => {
            let filterDatePeriodType = params.get("filterDatePeriodType");
            let filterTaskType = params.get("filterTaskType");
            if (filterDatePeriodType && filterTaskType)
                this.getTasksByFilter(filterDatePeriodType, filterTaskType);
        });
    }

    //#endregion

    //#region Methods
    getTasksByFilter(filterDatePeriodType: string, filterTaskType: string) {
        this.taskService
        .getAllByFilter(filterDatePeriodType, filterTaskType)
        .pipe(first())
        .subscribe(
          tasks => {
            if (tasks) {
                tasks.forEach(task => {
                    task.completionDate = moment(task.completionDate).format('DD.MM.YYYY');
                });
                this.tasks = tasks;
            }
          },
          error => {
            console.log(error);
          }
        );
    }
    //#endregion
}