﻿using System;
using System.Threading.Tasks;

namespace ProjectWorks.Services.ChatService
{
    public interface IChatSignalR
    {
        System.Threading.Tasks.Task SendToAll(string message);
        System.Threading.Tasks.Task SendToCaller(string message);
        System.Threading.Tasks.Task SendToUser(string connectionId, string message);
        System.Threading.Tasks.Task SendMessageToGroup(string group, string message);
        System.Threading.Tasks.Task Connect(string taskId);
        System.Threading.Tasks.Task Disconnect(string taskId, Exception ex);
        System.Threading.Tasks.Task JoinGroup(string taskId);
        System.Threading.Tasks.Task LeaveGroup(string groupName);
    }
}
