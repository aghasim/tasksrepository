﻿using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkTaskExecutionOrderRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskExecutionOrderService : ServiceBase<WorkTaskExecutionOrder>, IWorkTaskExecutionOrderService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;

        private readonly IWorkTaskExecutionOrderRepository _workTaskExecutionOrderRepository;

        #endregion

        #region Constructor

        public WorkTaskExecutionOrderService(ICacheManager cacheManager, IWorkTaskExecutionOrderRepository workTaskExecutionOrderRepository, IUserService userService) :
            base(cacheManager, workTaskExecutionOrderRepository, userService)
        {
            _cacheManager = cacheManager;
            _workTaskExecutionOrderRepository = workTaskExecutionOrderRepository;
        }

        #endregion
    }
}