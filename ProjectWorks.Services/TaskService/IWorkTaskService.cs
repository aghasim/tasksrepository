﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkTaskService : IServiceBase<WorkTask>
    {
        #region Methods

        Task<WorkTaskDto> GetDto(Guid id);

        void Add(TokenData tokenData, WorkTask entity);

        void Update(TokenData tokenData, WorkTask entity);

        Task Delete(TokenData tokenData, WorkTask entity);

        Task<TaskFilterDto> GetFilter(TokenData tokenData, Guid projectId, FilterDatePeriodType periodType = FilterDatePeriodType.Month);

        Task<List<WorkTaskDto>> GetAll(TokenData tokenData, Guid projectId);

        Task<List<WorkTaskDto>> GetAllByMonth(TokenData tokenData, Guid projectId, int year, int month);

        Task<List<WorkTaskDto>> GetAllByFilter(TokenData tokenData,
            FilterDatePeriodType filterDatePeriodType = FilterDatePeriodType.Month,
            FilterTaskType filterTaskType = FilterTaskType.Incoming);

        Task<List<WorkTaskDto>> GetAllActive(TokenData tokenData, Guid projectId);

        Task<List<WorkTaskDto>> GetAllIncoming(TokenData tokenData, Guid projectId);

        Task<List<WorkTaskDto>> GetAllOutgoing(TokenData tokenData, Guid projectId);

        Task UpdateStatus(TokenData tokenData, Guid id);

        #endregion
    }
}