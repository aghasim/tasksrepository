﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using Serilog;
using Constants = ProjectWorks.Core.Common.Constants;

namespace ProjectWorks.AmrApp.Services
{
    public class ObjectService : IObjectService
    {
        #region Fields

        private readonly IAmrAppRepository _amrApp;

        private readonly ICacheManager _cacheManager;

        #endregion

        #region Constructor

        public ObjectService(IAmrAppRepository amrApp,
            ICacheManager cacheManager)
        {
            _amrApp = amrApp;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public async Task<Object1> Get(int filialId, int id)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.OBJECT_CURRENT_KEY, filialId, id);
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.Object1
                        .FirstOrDefaultAsync(x => x.Id == id && x.FilialId == filialId));
            }

            return null;
        }

        public async Task<IEnumerable<Object1>> GetByFilialId(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.OBJECT_ALL_KEY, filialId);
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.Object1
                        .Where(x => x.FilialId == filialId).ToListAsync());
            }

            return null;
        }

        public async Task<IEnumerable<Object1>> GetByObjectType(int filialId, List<int> objectTypes)
        {
            if (objectTypes == null)
                return new List<Object1>();

            var objects = await GetByFilialId(filialId);
            return objects.Where(obj => objectTypes.Contains(obj.ObjectTypeId));
        }

        public async Task<Object1> GetDefaultExpenditure(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var key = string.Format(Constants.CacheConstants.DEFAULT_EXPENDITURE_KEY, filialId);
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.Object1
                        .FirstOrDefaultAsync(x =>
                            x.NotDel && x.ObjectTypeId == Constants.A2Constants.ObjTypeСosts && x.NomGrp &&
                            x.ObjectName.Equals(Constants.A2Constants.DefaultExpenditureName)));
            }

            return null;
        }

        public async Task AddObject(TokenData tokenData, Object1 obj)
        {
            _amrApp.SetConnString(tokenData.FilialId);
            if (_amrApp.Context != null)
            {
                Log.Information(string.Format(Constants.LogConstants.OBJECTSERVICE_ADDOBJECT, tokenData.FilialId));
                _amrApp.Context.Object1.Add(obj);
                await _amrApp.Context.SaveChangesAsync();
            }
        }

        public async Task<int> GetLastObjectId(TokenData tokenData)
        {
            _amrApp.SetConnString(tokenData.FilialId);
            if (_amrApp.Context != null)
            {
                return await _amrApp.Context.Object1.MaxAsync(x => x.Id);
            }

            return default(int);
        }

        #endregion
    }
}