﻿namespace ProjectWorks.Core.Dtos.Common
{
    public class CommonDto
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        #endregion
    }
}