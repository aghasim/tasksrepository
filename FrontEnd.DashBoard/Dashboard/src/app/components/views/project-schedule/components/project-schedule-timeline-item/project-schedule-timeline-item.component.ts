import * as moment from 'moment';
import { Component, Input } from '@angular/core';
import { Task } from 'src/app/models';

@Component({
    templateUrl: 'project-schedule-timeline-item.component.html',
})

export class ProjectScheduleTimeLineItemComponent {
    //#region Fields 

    @Input() task: Task;

    //#endregion

    //#region Constructor

    public constructor() {
    }

    //#endregion

    //#region Methods

    //#endregion
}