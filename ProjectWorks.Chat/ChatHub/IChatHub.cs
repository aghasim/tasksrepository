﻿using System;
using System.Threading.Tasks;
using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Chat.ChatHub
{
    public interface IChatHub
    {
        #region Properties

        Task Connect(string user);

        Task JoinGroup(Guid taskId);

        Task Disconnect();

        Task LeaveGroup(string group);

        Task SendToGroup(ConversationDto dto);

        Task SendToUser(string connId, string message);

        Task SendToCaller(string message);

        Task SendToAll(string message);

        Task Refresh(Guid taskId);

        #endregion
    }
}