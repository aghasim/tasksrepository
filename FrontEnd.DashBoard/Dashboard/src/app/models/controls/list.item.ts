export class ListItem {
  public id: number;
  public name: string;
  public isChecked: boolean = false;
}