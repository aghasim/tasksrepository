import { Component, OnInit, EventEmitter, Output } from "@angular/core";

import * as moment from 'moment';

import { TaskService } from "../../../services";
import { Task, Resource, ExecutionOrder } from 'src/app/models';
import { TaskWizzardStep, ResourceType, ExecutionOrderType, ImplementationMonitoringType, CommunicationSettingsType, CloseControlType, TaskAppointedTarget, DEFAULT_GUID } from 'src/app/common';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'task-wizzard',
  templateUrl: "task-wizzard.component.html",
  styleUrls: ["task-wizzard.component.css"],
})
export class TaskWizzardComponent implements OnInit {
  private task: Task;
  public wizzardStep: TaskWizzardStep;
  public isLastStep: boolean;
  private projectId: string;
  private parentId: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService
  ) {
    this.route.paramMap.subscribe(params => {
      this.projectId = params.get("projectId").toString();
      this.parentId = params.get("parentId");
    });
  }

  ngOnInit() {
    this.task = new Task();
    this.task.name = '';
    this.task.description = '';
    this.task.taskResource = new Resource();
    this.task.taskResource.type = ResourceType.Money;
    this.task.taskResource.count = 0;
    this.task.executionOrder = new ExecutionOrder();
    this.task.executionOrder.executionOrderType = ExecutionOrderType.Consistently;
    this.task.executionOrder.executionOrder = 1;
    this.task.appointedTarget = TaskAppointedTarget.ObjectType;
    this.task.closeControlType = CloseControlType.NoAction;
    this.task.communicationSettingsType = CommunicationSettingsType.WithoutDiscussion
    this.task.implementationMonitoringType = ImplementationMonitoringType.WithoutControl;
    this.wizzardStep = TaskWizzardStep.Main;
  }

  onSave() {
    this.task.projectId = this.projectId;
    // https://stackoverflow.com/questions/14359566/how-to-pass-a-datetime-parameter
    this.task.startDate = moment(this.task.startDate).format('YYYY-MM-DDTHH:mm:ss');
    this.task.completionDate = moment(this.task.completionDate).format('YYYY-MM-DDTHH:mm:ss');

    if (this.parentId)
      this.task.parentId = this.parentId;
    else
      this.task.parentId = DEFAULT_GUID;

    this.taskService
      .add(this.task)
      .pipe(first())
      .subscribe(
        taskId => {
          if (taskId && this.task.filesToUpload.length > 0) {
            this.uploadFiles(taskId);
          } else {
            let url = "/project/" + this.projectId + '/tasks';
            this.router.navigate([url]);
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  uploadFiles(taskId: string) {
    const formData: any = new FormData();
    let fileToUpload = <File>this.task.filesToUpload[0];
    formData.append('uploadFile', fileToUpload, fileToUpload.name);
    this.taskService
      .uploadFile(taskId, formData)
      .pipe(first())
      .subscribe(
        result => {
          if (result) {
            let url = "/project/" + this.projectId + '/tasks';
            this.router.navigate([url]);
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  onForward() {
    let step = this.wizzardStep + 1;
    if (step in TaskWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = step + 1 in TaskWizzardStep ? false : true;
  }

  onBackward() {
    let step = this.wizzardStep - 1;
    if (step in TaskWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = false;
  }
}