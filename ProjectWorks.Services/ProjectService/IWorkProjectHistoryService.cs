﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Services.ProjectService
{
    public interface IWorkProjectHistoryService : IServiceBase<WorkProjectHistory>
    {
        #region Methods

        Task<List<WorkProjectHistory>> GetAllByDate(Guid projectId, DateTime startDate, DateTime endDate);

        #endregion
    }
}