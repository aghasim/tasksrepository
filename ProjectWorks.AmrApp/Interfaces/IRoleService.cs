﻿using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IRoleService : IAmrService<AppFuncRoles>
    {
    }
}