﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Тип порядка исполнения задачи
    /// </summary>
    public enum WorkTaskExecutionOrderType
    {
        /// <summary>
        ///     Последовательно
        /// </summary>
        [Display(Name = "Последовательно")]
        Consistently = 1,

        /// <summary>
        ///     Параллельно
        /// </summary>
        [Display(Name = "Параллельно")]
        InParallel = 2
    }
}