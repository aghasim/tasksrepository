import { Component, OnInit, EventEmitter, Output } from "@angular/core";

import { ProjectService } from "../../../services";
import { Project, Resource } from 'src/app/models';
import { ProjectWizzardStep, ResourceType, DEFAULT_GUID } from 'src/app/common';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'project-wizzard',
  templateUrl: 'project-wizzard.component.html',
  styleUrls: ["project-wizzard.component.css"],
})
export class ProjectWizzardComponent implements OnInit {
  //#region Fields 

  private project: Project;
  public wizzardStep: ProjectWizzardStep;
  public isLastStep: boolean;
  private parentId: string;

  //#endregion

  //#region Constructor 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private projectService: ProjectService
  ) {
    this.route.paramMap.subscribe(params => {
      this.parentId = params.get("parentId");
    });
  }

  //#endregion

  //#region Methods 

  ngOnInit() {
    this.project = new Project();
    this.project.name = '';
    this.project.description = '';
    this.project.id = DEFAULT_GUID;
    this.project.projectResource = new Resource();
    this.project.projectResource.type = ResourceType.Money;
    this.project.projectResource.count = 0;
    this.wizzardStep = ProjectWizzardStep.Main;
  }

  onSave() {
    // https://stackoverflow.com/questions/14359566/how-to-pass-a-datetime-parameter
    this.project.startDate = moment(this.project.startDate).format('YYYY-MM-DDTHH:mm:ss');
    this.project.completionDate = moment(this.project.completionDate).format('YYYY-MM-DDTHH:mm:ss');
    if (this.parentId)
      this.project.parentId = this.parentId;
    else
      this.project.parentId = DEFAULT_GUID;

    this.projectService
      .add(this.project)
      .pipe(first())
      .subscribe(
        result => {
          if (result) {
            this.router.navigate(["/projects"]);
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  onForward() {
    let step = this.wizzardStep + 1;
    if (step in ProjectWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = step + 1 in ProjectWizzardStep ? false : true;
  }

  onBackward() {
    let step = this.wizzardStep - 1;
    if (step in ProjectWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = false;
  }

  //#endregion
}