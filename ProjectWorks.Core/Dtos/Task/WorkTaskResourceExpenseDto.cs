﻿using System;

namespace ProjectWorks.Core.Dtos.Task
{
    public class WorkTaskResourceExpenseDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Ресурс Id
        public Guid TaskResourceId { get; set; }

        // Количество расхода ресурса
        public decimal Count { get; set; }

        #endregion
    }
}