﻿using System;
using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    public class WorkProjectHistory : EntityBase
    {
        #region Properties

        // Тип операции
        public HistoryOperationType HistoryOperationType { get; set; }

        // Статус проекта
        public WorkProjectStatus Status { get; set; }

        // Кто изменил, пользователь из А2
        [Required]
        public int EditorId { get; set; }

        // Описание
        public string Description { get; set; }

        // Проект Id
        public Guid WorkProjectId { get; set; }

        // Проект
        public virtual WorkProject WorkProject { get; set; }

        #endregion
    }
}