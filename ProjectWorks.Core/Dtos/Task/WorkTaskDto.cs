﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;
using ProjectWorks.Core.Models.Timeline;

namespace ProjectWorks.Core.Dtos.Task
{
    public class WorkTaskDto
    {
        #region Properties

        // Id
        public Guid Id { get; set; }

        // Название
        public string Name { get; set; }

        // Описание
        public string Description { get; set; }

        // Ответственный, пользователь из А2
        public int ResponsibleId { get; set; }

        // Ответственный, пользователь из А2
        public string ResponsibleName { get; set; }

        // Ответственный, пользователь из А2
        public string ResponsibleRoleName { get; set; }

        // Дата начало
        public DateTime StartDate { get; set; }

        // Дата завершения
        public DateTime CompletionDate { get; set; }

        // Статус задачи
        public WorkTaskStatus Status { get; set; }

        // Наименование статуса задачи
        public string StatusName { get; set; }

        // Родительская задача
        public Guid ParentId { get; set; } = Guid.Empty;

        // Количество выполненных дочерних задач
        public int TaskCompletedCount { get; set; }

        // Всего дочерних задач
        public int TaskCount { get; set; }

        // Ресурс задачи
        public WorkTaskResourceDto TaskResource { get; set; }

        // Расход ресурса
        public decimal TaskResourceExpense { get; set; }

        // вложенные проекты
        public List<WorkTaskDto> ChildTasks { get; set; } = new List<WorkTaskDto>();

        // Проект Id
        public Guid ProjectId { get; set; }

        /// <summary>
        /// Контроль выполнения задачи
        /// </summary>
        public CloseControlType CloseControlType { get; set; }

        /// <summary>
        /// Контроль выполнения задачи
        /// </summary>
        public string CloseControlTypeName { get; set; }

        /// <summary>
        /// Настройки коммуникации
        /// </summary>
        public CommunicationSettingsType CommunicationSettingsType { get; set; }

        /// <summary>
        /// Настройки коммуникации
        /// </summary>
        public string CommunicationSettingsTypeName { get; set; }

        // вложенные проекты
        public List<int> Implementers { get; set; } = new List<int>();

        // вложенные проекты
        public List<int> Reviewers { get; set; } = new List<int>();

        // Порядок исполнения задачи
        public WorkTaskExecutionOrderDto ExecutionOrder { get; set; }

        // Количество сообщений
        public int MessagesCount { get; set; }

        // Timeline
        public List<TaskTimeline> Timeline { get; set; } = new List<TaskTimeline>();

        // User timeline
        public List<List<UserTimeline>> UserTimeline { get; set; } = new List<List<UserTimeline>>();

        #endregion
    }
}