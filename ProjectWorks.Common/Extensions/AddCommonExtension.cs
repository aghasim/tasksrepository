﻿using Microsoft.Extensions.DependencyInjection;
using ProjectWorks.AmrApp.Extensions;
using ProjectWorks.Common.Services;
using ProjectWorks.Services.CommonService;

namespace ProjectWorks.Common.Extensions
{
    public static class AddCommonExtension
    {
        #region Methods

        public static IServiceCollection AddCommon(this IServiceCollection services)
        {
            // Register module providers
            services.AddAmrApp();
            // Register module services
            services.AddScoped(typeof(ICommonService), typeof(CommonService));
            // Register module repositories

            return services;
        }

        #endregion
    }
}