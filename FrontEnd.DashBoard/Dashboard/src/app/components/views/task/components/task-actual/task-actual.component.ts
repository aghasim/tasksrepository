import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TaskService } from 'src/app/services';
import { Task } from 'src/app/models';
import { DEFAULT_GUID } from 'src/app/common';
import { first } from 'rxjs/operators';

@Component({
  selector: 'task-actual',
  templateUrl: 'task-actual.component.html'
})
export class TaskActualComponent {
  //#region Fields 

  public tasks: Task[];

  //#endregion

  //#region Constructor

  public constructor(
    private router: Router,
    private taskService: TaskService) {
      this.getTasks();
  }

  //#endregion

  //#region Methods

  getTasks() {
    this.taskService
      .getActive(DEFAULT_GUID)
      .pipe(first())
      .subscribe(
        tasks => {
          if (tasks) {
            this.tasks = tasks;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  //#endregion
}