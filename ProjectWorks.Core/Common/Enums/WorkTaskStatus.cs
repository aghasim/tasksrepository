﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Статус задачи
    /// </summary>
    public enum WorkTaskStatus
    {
        /// <summary>
        ///     Новая
        /// </summary>
        [Display(Name = "Новая")]
        New = 1,

        /// <summary>
        ///     В работе
        /// </summary>
        [Display(Name = "В работе")]
        InProgress = 2,

        /// <summary>
        ///     Выполнена
        /// </summary>
        [Display(Name = "Выполнена")]
        Done = 3,

        /// <summary>
        ///     Принята
        /// </summary>
        [Display(Name = "Принята")]
        Completed = 4,

        /// <summary>
        ///     Возвращена
        /// </summary>
        [Display(Name = "Возвращена")]
        Returned = 5,

        /// <summary>
        ///     Отклонена
        /// </summary>
        [Display(Name = "Отклонена")]
        Refused = 6,

        /// <summary>
        ///     Нет
        /// </summary>
        [Display(Name = "Нет")]
        None = 7
    }
}