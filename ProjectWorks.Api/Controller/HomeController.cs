using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using FileIO = System.IO.File;



namespace ProjectWorks.Api.Controllers {
	public class HomeController : Controller {
		private readonly IHostingEnvironment env;
		private readonly IHttpContextAccessor contextAccessor;



		public HomeController(IHostingEnvironment env, IHttpContextAccessor contextAccessor) {
			this.env = env;
			this.contextAccessor = contextAccessor;
		}



		public IActionResult Index() {
			var segments = this.contextAccessor.HttpContext.Request.Path
				.ToString()
				.Split('/')
				.Select(m => m.Trim('/'));

			segments.Prepend("");

			do {
				var directory = String.Join(Path.DirectorySeparatorChar, segments).Trim(Path.DirectorySeparatorChar);
				var index = Path.Combine(env.WebRootPath, directory, "index.html");

				if (FileIO.Exists(index)) {
					return new PhysicalFileResult(index, "text/html");
				}

				segments = segments.Take(segments.Count() - 1);
			} while (segments.Count() > 0);

			return NotFound();
		}
	}
}
