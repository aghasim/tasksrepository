﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Тип операции
    /// </summary>
    public enum HistoryOperationType
    {
        /// <summary>
        ///     Создание
        /// </summary>
        [Display(Name = "Создание")]
        Create = 1,
        /// <summary>
        ///     Обновление
        /// </summary>
        [Display(Name = "Обновление")]
        Update,
        /// <summary>
        ///     Изменение статуса исполнителем
        /// </summary>
        [Display(Name = "Изменение статуса исполнителем")]
        StatusСhangeByImplementer,
        /// <summary>
        ///     Изменение статуса
        /// </summary>
        [Display(Name = "Изменение статуса")]
        StatusСhange
    }
}