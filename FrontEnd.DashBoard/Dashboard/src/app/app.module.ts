import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { JwtInterceptor, ErrorInterceptor } from './helpers';

import { NgxSmartModalModule } from 'ngx-smart-modal';

// COMPONENTS
import { 
  CheckBoxListComponent, 
  ImplementationMonitoringComponent, 
  TaskExecutionOrderComponent, 
  ProjectTaskResourceComponent, 
  ProjectWizzardComponent, 
  CreateProjectComponent, 
  ProjectEditWizzardComponent,
  TaskEditWizzardComponent,
  TaskFilterComponent,
  TaskActualComponent} from './components';
import { PreloaderComponent } from './components';
import { CreateWorkProjectPatternComponent } from './components';
import { CreateWorkTaskPatternComponent } from './components';

// VIEWS COMPONENT
import { ProjectsListComponent } from './components';
import { ProjectsItemComponent } from './components';
import { TaskInfoComponent } from './components';
import { ProjectTasksListComponent } from './components';
import { ProjectTaskItemComponent } from './components';
import { TaskStatusComponent } from './components';
import { TaskStatusListComponent } from './components';
import { ImplementerTaskListComponent } from './components';
import { ReviewerTaskListComponent } from './components';
import { TaskAddExpenseBtn } from './components';
import { ProjectScheduleItemComponent } from './components';
import { ProjectScheduleTimeLineComponent } from './components';
import { ProjectScheduleTimeLineItemComponent } from './components';


// VIEWS
import { AppComponent } from './app.component';
import { LoginComponent } from './components';
import { HomeComponent } from './components';
import { AccessDeniedComponent } from './components';
import { CreateTaskComponent } from './components';
import { CreateTaskSettingsComponent } from './components';
import { ProjectsComponent } from './components';
import { ProjectScheduleComponent } from './components';
import { ProjectsListScheduleComponent } from './components';
import { ProjectTasksComponent } from './components';
import { TaskComponent } from './components';
import { UserTasksComponent } from './components';
import { TaskWizzardComponent } from './components';
import { WizzardButtonsComponent } from './components';
import { KeysPipe } from './common';
import { ChatComponent } from './components/controls/chat/chat.component';


@NgModule({
  declarations: [
    AppComponent,
    PreloaderComponent,
    LoginComponent,
    HomeComponent,
    AccessDeniedComponent,
    CreateTaskComponent,
    CreateTaskSettingsComponent,
    CheckBoxListComponent,
    ProjectsComponent,
    ProjectsListComponent,
    ProjectsItemComponent,
    ProjectScheduleComponent,
    ProjectsListScheduleComponent,
    TaskComponent,
    ProjectTasksComponent,
    ProjectTaskItemComponent,
    ProjectTasksListComponent,
    TaskWizzardComponent,
    WizzardButtonsComponent,
    KeysPipe,
    ImplementationMonitoringComponent,
    TaskExecutionOrderComponent,
    ProjectTaskResourceComponent,
    ProjectWizzardComponent,
    CreateProjectComponent,
    ProjectEditWizzardComponent,
    TaskEditWizzardComponent,
    ChatComponent,
    CreateWorkProjectPatternComponent,
    CreateWorkTaskPatternComponent,
    TaskFilterComponent,
    TaskActualComponent,
    TaskInfoComponent,
    TaskStatusComponent,
    TaskStatusListComponent,
    UserTasksComponent,
    ImplementerTaskListComponent,
    ReviewerTaskListComponent,
    TaskAddExpenseBtn,
    ProjectsListScheduleComponent,
    ProjectScheduleItemComponent,
    ProjectScheduleTimeLineComponent,
    ProjectScheduleTimeLineItemComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSmartModalModule.forRoot()
  ],
  exports: [
    CreateTaskComponent,
    CreateTaskSettingsComponent,
    CheckBoxListComponent,
    WizzardButtonsComponent,
    ImplementationMonitoringComponent,
    TaskExecutionOrderComponent,
    ProjectTaskResourceComponent,
    ProjectWizzardComponent,
    CreateProjectComponent,
    ProjectEditWizzardComponent,
    TaskEditWizzardComponent,
    TaskFilterComponent,
    TaskActualComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }