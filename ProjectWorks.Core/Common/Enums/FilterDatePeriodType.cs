﻿using System.ComponentModel.DataAnnotations;

namespace ProjectWorks.Core.Common.Enums
{
    /// <summary>
    ///     Период фильтра
    /// </summary>
    public enum FilterDatePeriodType
    {
        /// <summary>
        ///     Неделя
        /// </summary>
        [Display(Name = "Неделя")]
        Week = 1,
        /// <summary>
        ///     Месяц
        /// </summary>
        [Display(Name = "Месяц")]
        Month,
        /// <summary>
        ///     Квартал
        /// </summary>
        [Display(Name = "Квартал")]
        Quarter,
        /// <summary>
        ///     Нет периода
        /// </summary>
        [Display(Name = "Нет периода")]
        None
    }
}