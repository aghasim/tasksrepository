﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Data.Repository.WorkTaskResourceExpenseRepository
{
    public interface IWorkTaskResourceExpenseRepository : IGenericRepository<WorkTaskResourceExpense>
    {
        #region Methods

        Task<List<WorkTaskResourceExpense>> GetAll(Guid resourceId);

        #endregion
    }
}