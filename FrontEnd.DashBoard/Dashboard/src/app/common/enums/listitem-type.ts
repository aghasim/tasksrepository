export enum ListItemType {
  ObjectType = 1,
  Object,
  Department,
  Role,
  User
}