﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Models;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IUserService : IAmrService<AppUsers>
    {
        #region Properties
        int CurrentUserId { get; }
        int CurrentUserFilialId { get;}
        #endregion

        #region Methods

        Task<IEnumerable<AppUsers>> GetAllUsersByObjects(int filialId, List<int> ids);

        Task<IEnumerable<AppUsers>> GetAllUsersByDepartments(int filialId, List<int> ids);

        Task<IEnumerable<AppUsers>> GetAllUsersByRoles(int filialId, List<int> ids);

        string GetRoleNameByUserId(int filialId, int userId);

        #endregion
    }
}