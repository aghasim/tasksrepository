﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectWorks.Data.Extensions;
using ProjectWorks.Services.CommonService;

namespace ProjectWorks.Common.Controllers
{
    [Route("api/common")]
    [Authorize]
    public class CommonController : Controller
    {
        #region Fields

        private readonly ICommonService _commonService;

        #endregion

        #region Constructor

        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }

        #endregion

        #region Methods

        [HttpGet("taskexecutionordertypes")]
        public IActionResult GetTaskExecutionOrderTypes()
        {
            var items = _commonService.GetTaskExecutionOrderTypes();
            return this.Success(items);
        }

        [HttpGet("taskresourcetypes")]
        public IActionResult GetTaskResourceTypes()
        {
            var items = _commonService.GetTaskResourceTypes();
            return this.Success(items);
        }

        [HttpGet("taskstatuses")]
        public IActionResult GetTaskStatusTypes()
        {
            var items = _commonService.GetTaskStatusTypes();
            return this.Success(items);
        }

        [HttpGet("months")]
        public IActionResult GetMonths()
        {
            var items = _commonService.GetMonths();
            return this.Success(items);
        }

        #endregion
    }
}
