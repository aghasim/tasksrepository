import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { first } from "rxjs/operators";

import { AlertService, AuthenticationService } from "../../../services";
import { FilterDatePeriodType } from 'src/app/common';

@Component({ templateUrl: "login.component.html" })
export class LoginComponent implements OnInit {
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) {
    }

    ngOnInit() {
        this.returnUrl = this.route.snapshot.queryParams["returnUrl"];
        this.route.paramMap.subscribe(params => {
            let returnUrlType = params.get("returnUrlType");
            let userId = Number(params.get("userId"));
            let filialId = Number(params.get("filialId"));
            this.submit(userId, filialId, returnUrlType);
        });
    }

    submit(userId: number, filialId: number, returnUrlType: string) {
        this.authenticationService
            .login(userId, filialId)
            .pipe(first())
            .subscribe(
                user => {
                    if (user) {
                        if (returnUrlType) {
                            switch(returnUrlType) {
                                case '2':
                                case '3': { 
                                    let url = "/user-tasks/" + FilterDatePeriodType.Month + "/" + returnUrlType;
                                    this.router.navigate([url]);
                                   break; 
                                }
                                case '4': { 
                                    this.router.navigate(["projects/schedule"]);
                                    break; 
                                 }
                             } 
                        } else if (this.returnUrl) {
                            this.router.navigate([this.returnUrl]);
                        } else {
                            this.router.navigate(["/projects"]);
                        }
                    } else {
                        this.router.navigate(["/access-denied"]);
                    }
                },
                error => {
                    this.alertService.error(error);
                    this.router.navigate(["/access-denied"]);
                }
            );
    }
}