import { Component, Input } from '@angular/core';
import { Task } from 'src/app/models';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
    selector: 'implementer-task-list',
    templateUrl: 'implementer-task-list.component.html',
})

export class ImplementerTaskListComponent {
    //#region Fields
    @Input()
    public tasks: Task[];
    public task: Task;
    //#endregion

    //#region Constructor

    public constructor(public ngxSmartModalService: NgxSmartModalService) {
        this.task = new Task();
    }

    //#endregion

    //#region Methods

    setTask(taskId: string) {
        if (this.tasks) {
            let context = this;
            this.tasks.forEach(function (item) {
                if (item.id == taskId) {
                    context.task = item;
                }
            });
        }
    }

    //#endregion
}