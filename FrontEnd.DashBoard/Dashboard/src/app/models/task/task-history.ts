import { ModelBase } from '../model.base';
import { HistoryOperationType } from 'src/app/common/enums/history-operation-type';

export class TaskHistory extends ModelBase {
  public taskId: string;
  public created: string;
  public status: number;
  public statusName: string;
  public historyOperationType: HistoryOperationType;
  public historyOperationTypeName: string;
  public editorId: number;
  public editorName: string;
  public editorRoleName: string;
  public description: string;
}