﻿using System;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository;
using Serilog;

namespace ProjectWorks.Services
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity> where TEntity : EntityBase
    {
        #region Fields

        protected readonly ICacheManager _cacheManager;

        protected readonly IGenericRepository<TEntity> _genericRepository;

        protected readonly IUserService _userService;

        #endregion

        #region Constructor

        public ServiceBase(ICacheManager cacheManager, IGenericRepository<TEntity> genericRepository, IUserService userService)
        {
            _userService = userService;
            _cacheManager = cacheManager;
            _genericRepository = genericRepository;
        }

        #endregion

        #region Methods

        public async Task<TEntity> Get(Guid id)
        {
            var type = typeof(TEntity).ToString();
            var key = string.Format(Constants.CacheConstants.SERVICEBASE_GET_KEY, type, id);
            Log.Information(string.Format(Constants.LogConstants.SERVICEBASE_GET, type, id));

            return await _cacheManager.Get(key, async () => await _genericRepository.Get(id));
        }

        public void Add(TEntity entity)
        {
            entity.Id = Guid.NewGuid();
            Log.Information(
                string.Format(Constants.LogConstants.SERVICEBASE_ADD, typeof(TEntity).ToString(), entity.Id));
            _genericRepository.Insert(entity);
        }

        public void Update(TEntity entity)
        {
            Log.Information(
                string.Format(Constants.LogConstants.SERVICEBASE_UPDATE, typeof(TEntity).ToString(),entity.Id));
            _genericRepository.Update(entity);
        }

        public async Task Delete(Guid id)
        {
            Log.Information(
                string.Format(Constants.LogConstants.SERVICEBASE_DELETE, typeof(TEntity).ToString(), id));
            await _genericRepository.Delete(id);
        } 

        #endregion
    }
}