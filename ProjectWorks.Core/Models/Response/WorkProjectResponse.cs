﻿using ProjectWorks.Core.Dtos.Project;

namespace ProjectWorks.Core.Models.Response
{
    public class WorkProjectResponse
    {
        #region Properties

        public WorkProjectDto Project { get; set; }

        #endregion
    }
}