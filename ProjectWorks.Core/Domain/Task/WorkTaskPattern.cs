﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Core.Domain.Task
{
    public class WorkTaskPattern:FilialEntityBase
    {
        public Guid WorkTaskId { get; set; }
        public virtual WorkTask WorkTask { get; set; }
    }
}
