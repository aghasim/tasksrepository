import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";

import { Implementer } from "../../models";
import { environment } from "../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ImplementerService {
    private apiUrl: string = environment.apiUrl + "implementers/";

    constructor(private http: HttpClient) { }

    get(implementerId: string) {
        return this.http
            .get<any>(this.apiUrl + implementerId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getByTask(taskId: string) {
        return this.http
            .get<any>(this.apiUrl + "getbytask/" + taskId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    getAll(taskId: string) {
        return this.http
            .get<any>(this.apiUrl + "all/" + taskId)
            .pipe(
                map(result => {
                    return result.data;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    add(implementer: Implementer) {
        return this.http
            .post<any>(this.apiUrl + "add", { implementer })
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    update(implementer: Implementer) {
        return this.http
            .put<any>(this.apiUrl + "update", { implementer })
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }

    delete(implementerId: string) {
        return this.http
            .delete<any>(this.apiUrl + implementerId)
            .pipe(
                map(result => {
                    return result.success;
                })
            )
            .pipe(
                catchError((err, caught) => {
                    return null;
                })
            );
    }
}
