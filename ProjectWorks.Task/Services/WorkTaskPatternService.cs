﻿using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository;
using ProjectWorks.Data.Repository.WorkTaskPatternRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.TaskService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Task.Services
{
    public class WorkTaskPatternService : ServiceBase<WorkTaskPattern>, IWorkTaskPatternService
    {
        public WorkTaskPatternService(ICacheManager cacheManager, IWorkTaskPatternRepository genericRepository, IUserService userService)
            : base(cacheManager, genericRepository, userService)
        {

        }

        public string Create(string workTaskId, string name)
        {
            var item = new WorkTaskPattern
            {
                Id = Guid.NewGuid(),
                WorkTaskId = Guid.Parse(workTaskId),
                FilialId = _userService.CurrentUserFilialId,
                Name = name
            };
            Add(item);
            return item.Id.ToString();
        }

    }
}
