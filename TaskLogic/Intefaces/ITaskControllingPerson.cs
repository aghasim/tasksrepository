﻿using ProjectWorks.AmrApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Intefaces
{
    public interface ITaskControllingPerson
    {
        Task<ICollection<TaskControllingPersons>> GetAll(int filialId);
        Task<TaskControllingPersons> Get(int id, int filialId);
        System.Threading.Tasks.Task Add(TaskControllingPersons person, int filialId);
        System.Threading.Tasks.Task Delete(int id, int filialId);

    }
}
