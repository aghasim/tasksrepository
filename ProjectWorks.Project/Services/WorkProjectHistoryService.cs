﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Utils.Cache;
using ProjectWorks.Data.Repository.WorkProjectHistoryRepository;
using ProjectWorks.Services;
using ProjectWorks.Services.ProjectService;

namespace ProjectWorks.Project.Services
{
    public class WorkProjectHistoryService : ServiceBase<WorkProjectHistory>, IWorkProjectHistoryService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;

        private readonly IWorkProjectHistoryRepository _workProjectHistoryRepository;

        #endregion

        #region Constructor

        public WorkProjectHistoryService(
            ICacheManager cacheManager,
            IWorkProjectHistoryRepository workProjectHistoryRepository, 
            IUserService userService) :
            base(cacheManager, workProjectHistoryRepository, userService)
        {
            _cacheManager = cacheManager;
            _workProjectHistoryRepository = workProjectHistoryRepository;
        }

        #endregion

        #region Methods

        public async Task<List<WorkProjectHistory>> GetAllByDate(Guid projectId, DateTime startDate, DateTime endDate)
        {
            return await _workProjectHistoryRepository.GetAllByDate(projectId, startDate, endDate);
        }

        #endregion
    }
}