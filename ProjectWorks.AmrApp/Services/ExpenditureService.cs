﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.Core.Common;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Core.Extensions;
using ProjectWorks.Core.Utils.Cache;
using Serilog;
using Constants = ProjectWorks.Core.Common.Constants;

namespace ProjectWorks.AmrApp.Services
{
    public class ExpenditureService : IExpenditureService
    {
        #region Fields

        private readonly IAmrAppRepository _amrApp;

        private readonly ICacheManager _cacheManager;

        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public ExpenditureService(IAmrAppRepository amrApp,
            ICacheManager cacheManager,
            IUserService userService)
        {
            _amrApp = amrApp;
            _cacheManager = cacheManager;
            _userService = userService;
        }

        #endregion

        #region Methods

        public async Task<Object1> Get(int filialId, int id)
        {
            //_amrApp.SetConnString(filialId);
            //if (_amrApp.Context != null)
            //{
            //    var key = string.Format(Constants.CacheConstants.EXPENDITURE_CURRENT_KEY, filialId, id);
            //    Log.Information(string.Format(Constants.LogConstants.EXPENDITURESERVICE_GETEXPENDITURE, filialId, id));
            //    return await _cacheManager.Get(key,
            //        async () => await _amrApp.Context.AppUserDepartments
            //            .FirstOrDefaultAsync(x => x.Id == id && x.FilialId == filialId));
            //}

            return null;
        }

        public async Task<IEnumerable<Object1>> GetByFilialId(int filialId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                return new List<Object1>();
            }

            return null;
        }

        public async Task<IEnumerable<Object1>> GetByFilialId(int filialId, int userId)
        {
            _amrApp.SetConnString(filialId);
            if (_amrApp.Context != null)
            {
                var roleName = _userService.GetRoleNameByUserId(filialId, userId);
                var key = string.Format(Constants.CacheConstants.EXPENDITURE_ALL_KEY, filialId);
                Log.Information(string.Format(Constants.LogConstants.EXPENDITURESERVICE_GETEXPENDITURES, filialId));
                return await _cacheManager.Get(key,
                    async () => await _amrApp.Context.Object1
                        .Where(x => x.NotDel
                                    && x.ObjectTypeId == Constants.A2Constants.ObjTypeСosts
                                    && x.NomGrp == false
                                    && _amrApp.Context.CostExpenditureApprovement.Any(a =>
                                        a.Object1Id == x.Id && a.IsAprovement)
                                    && _amrApp.Context.PropertyValues.Count(p =>
                                        p.Object1Id == x.Id && p.Value.Contains(roleName) &&
                                        p.PropertyId == Constants.A2Constants.CostReportRoles) != 0)
                        .ToListAsync());
            }

            return null;
        }

        public async Task AddExpenditure(TokenData tokenData, WorkTaskResourceExpense expense, int expenditureId)
        {
            _amrApp.SetConnString(tokenData.FilialId);
            if (_amrApp.Context != null)
            {
                Log.Information(string.Format(Constants.LogConstants.EXPENDITURESERVICE_ADDEXPENDITURES, tokenData.FilialId, expenditureId));

                var cost = new Costs
                {
                    CreateDate = DateTime.UtcNow,
                    Comment = $"'Проектные работы' расход средств {expense.Id}",
                    Object1Id = expenditureId,
                    Sum = expense.Count,
                    AppUserId = tokenData.UserId,
                    CostType = Constants.A2Constants.CostTypeByUser,
                    BindleUserId = tokenData.UserId,
                    FilialId = tokenData.FilialId
                };

                _amrApp.Context.Costs.Add(cost);
                await _amrApp.Context.SaveChangesAsync();
            }
        }

        public async Task ExpenditureApprove(TokenData tokenData, int expenditureId)
        {
            _amrApp.SetConnString(tokenData.FilialId);
            if (_amrApp.Context != null)
            {
                var expenditureApprovement = new CostExpenditureApprovement
                {
                    AproveDate = DateTime.UtcNow,
                    Object1Id = expenditureId,
                    AppUserId = tokenData.UserId,
                    IsAprovement = true
                };

                _amrApp.Context.CostExpenditureApprovement.Add(expenditureApprovement);
                await _amrApp.Context.SaveChangesAsync();
            }
        }

        #endregion
    }
}