import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'projects.component.html',
    styleUrls: ['projects.component.css'],
})

export class ProjectsComponent implements OnInit {
    //#region Constructor

    public constructor(private router: Router) { }

    //#endregion

    //#region Methods

    ngOnInit() {
    }

    onAddProject() {
        this.router.navigate(["/project-wizard"]);
    }
    //#endregion
}