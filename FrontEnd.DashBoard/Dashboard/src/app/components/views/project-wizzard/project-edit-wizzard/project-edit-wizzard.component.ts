import { Component, OnInit } from "@angular/core";

import { Project, Resource } from 'src/app/models';
import { ProjectWizzardStep, ResourceType } from 'src/app/common';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ProjectService } from 'src/app/services';

@Component({
  selector: 'project-edit-wizzard',
  templateUrl: 'project-edit-wizzard.component.html',
  styleUrls: ["project-edit-wizzard.component.css"],
})
export class ProjectEditWizzardComponent implements OnInit {
  //#region Fields 

  private project: Project;
  public wizzardStep: ProjectWizzardStep;
  public isLastStep: boolean;

  //#endregion

  //#region Constructor 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private projectService: ProjectService
  ) {
    this.route.paramMap.subscribe(params => {
      let editProjectId = params.get("projectId").toString();
      this.getProject(editProjectId);
    });
  }

  //#endregion

  //#region Methods 

  ngOnInit() {
    this.project = new Project();
    this.wizzardStep = ProjectWizzardStep.Main;
  }

  onSave() {
    // https://stackoverflow.com/questions/14359566/how-to-pass-a-datetime-parameter
    this.project.startDate = moment(this.project.startDate).format('YYYY-MM-DDTHH:mm:ss');
    this.project.completionDate = moment(this.project.completionDate).format('YYYY-MM-DDTHH:mm:ss');
    this.projectService
      .update(this.project)
      .pipe(first())
      .subscribe(
        result => {
          if (result) {
            this.router.navigate(["/projects"]);
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  onForward() {
    let step = this.wizzardStep + 1;
    if (step in ProjectWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = step + 1 in ProjectWizzardStep ? false : true;
  }

  onBackward() {
    let step = this.wizzardStep - 1;
    if (step in ProjectWizzardStep)
      this.wizzardStep = step;

    this.isLastStep = false;
  }

  getProject(projectId: string) {
    if (!this.project) {
      this.projectService
        .get(projectId)
        .pipe(first())
        .subscribe(
          item => {
            if (item) {
              console.log(item);
              this.project = item;
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  //#endregion
}