﻿using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorks.Data.Repository.WorkTaskPatternRepository
{
    public class WorkTaskPatternRepository : GenericRepository<WorkTaskPattern>, IWorkTaskPatternRepository
    {
        public WorkTaskPatternRepository(IDataContext context) : base(context)
        {
        }
    }
}
