﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectWorks.Data.Migrations
{
    public partial class Add_Status_WorkProject_table_Add_WorkProjectHistory_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "WorkProjects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "WorkProjectHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    HistoryOperationType = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    EditorId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    WorkProjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkProjectHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkProjectHistories_WorkProjects_WorkProjectId",
                        column: x => x.WorkProjectId,
                        principalTable: "WorkProjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkProjectHistories_WorkProjectId",
                table: "WorkProjectHistories",
                column: "WorkProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkProjectHistories");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "WorkProjects");
        }
    }
}
