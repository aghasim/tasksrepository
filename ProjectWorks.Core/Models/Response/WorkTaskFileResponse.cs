﻿using System;
using Microsoft.AspNetCore.Http;
using ProjectWorks.Core.Dtos.Task;

namespace ProjectWorks.Core.Models.Response
{
    public class WorkTaskFileResponse
    {
        #region Properties

        public Guid Id { get; set; }

        public IFormFile FormData { get; set; }

        #endregion
    }
}