﻿using ProjectWorks.Core.Domain.Task;

namespace ProjectWorks.Services.TaskService
{
    public interface IWorkTaskResourceService : IServiceBase<WorkTaskResource>
    {
    }
}