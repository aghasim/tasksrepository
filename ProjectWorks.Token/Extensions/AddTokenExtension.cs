﻿using Microsoft.Extensions.DependencyInjection;
using ProjectWorks.AmrApp.Extensions;
using ProjectWorks.AmrApp.Repository;
using ProjectWorks.Token.Services;

namespace ProjectWorks.Token.Extensions
{
    public static class AddTokenExtension
    {
        #region Methods

        public static IServiceCollection AddToken(this IServiceCollection services)
        {
            // Register module providers
            services.AddAmrApp();
            // Register module services
            services.AddScoped<TokenService>();
            // Register module repositories
            services.AddScoped<AmrAppRepository>();

            return services;
        }

        #endregion
    }
}