export class TaskFilter {
  public controlingCount: number;
  public incomingCount: number;
  public outgoingCount: number;
  public activeControlingCount: number;
  public activeIncomingCount: number;
  public activeOutgoingCount: number;
  public newIncomingCount: number;
  public newOutgoingCount: number;
  public tomorrowCount: number;
  public todayCount: number;
  public pastCount: number;
}