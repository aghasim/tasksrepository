﻿using Microsoft.EntityFrameworkCore;
using ProjectWorks.AmrApp.Interfaces;
using ProjectWorks.AmrApp.Models;
using ProjectWorks.AmrApp.Repository;
using ProjectWorks.Contracts.Intefaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectWorks.Contracts.Services
{
    public class TaskUserTaskService : ITaskUserTask
    {
        readonly AmrAppRepository amrApp;
        public TaskUserTaskService(AmrAppRepository amrApp)
        {
            this.amrApp = amrApp;
        }

        public async Task<ICollection<TaskUserTasks>> GetAll(int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskUserTasks.ToListAsync();
            }
            catch(Exception)
            {
                throw;
            }
        }

        public async Task<TaskUserTasks> Get(int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                return await amrApp.Context.TaskUserTasks.FirstOrDefaultAsync(x => x.Id == id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async System.Threading.Tasks.Task Add(TaskUserTasks task, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                amrApp.Context.TaskUserTasks.Add(task);
                await amrApp.Context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async System.Threading.Tasks.Task Delete (int id, int filialId)
        {
            try
            {
                this.amrApp.SetConnString(filialId);
                var task = await amrApp.Context.TaskUserTasks.FirstOrDefaultAsync(x => x.Id == id);
                amrApp.Context.TaskUserTasks.Remove(task);
                await amrApp.Context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
