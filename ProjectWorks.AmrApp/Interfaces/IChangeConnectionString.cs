﻿namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IChangeConnectionString
    {
        string GetConnStringName(int filialId);
    }
}
