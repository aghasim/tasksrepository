﻿using System.Text;

namespace ProjectWorks.Token.Helpers
{
    public class JwtHelper
    {
        #region Const

        private const string Key = "M2NyYqjCzPDjbstrwXRJJv7dHuX7Uxeu";

        #endregion

        #region Methods

        public static byte[] GetSecurityKey()
        {
            return Encoding.ASCII.GetBytes(Key);
        }

        public static byte[] GetSecurityKey(string key)
        {
            return Encoding.ASCII.GetBytes(key);
        }

        #endregion
    }
}
