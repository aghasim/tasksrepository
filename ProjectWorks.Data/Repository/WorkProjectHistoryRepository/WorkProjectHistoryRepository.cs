﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectWorks.Core.Domain.Task;
using ProjectWorks.Data.Context;

namespace ProjectWorks.Data.Repository.WorkProjectHistoryRepository
{
    public class WorkProjectHistoryRepository : GenericRepository<WorkProjectHistory>, IWorkProjectHistoryRepository
    {
        #region Constructor

        public WorkProjectHistoryRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<List<WorkProjectHistory>> GetAllByDate(Guid projectId, DateTime startDate, DateTime endDate)
        {
            return await _entities
                .Where(x => x.WorkProjectId == projectId & x.Created >= startDate && x.Created <= endDate && !x.IsDeleted)
                .OrderBy(entity => entity.Id)
                .ToListAsync();
        }

        #endregion
    }
}