import { ModelBase } from '../model.base';

export class Conversation extends ModelBase {
  public created: string;
  public senderId: number;
  public senderName: string;
  public senderRoleName: string;
  public recipientId: number;
  public message: string;
  public taskId: string;
}