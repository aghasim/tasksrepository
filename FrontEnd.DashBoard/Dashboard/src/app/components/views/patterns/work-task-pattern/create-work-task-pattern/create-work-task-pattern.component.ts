import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TaskPatternService } from 'src/app/services/task-pattern/task-pattern.service';
import { first } from 'rxjs/operators';

@Component({
    templateUrl: 'create-work-task-pattern.component.html',
    styleUrls: ['create-work-task-pattern.component.css'],
})

export class CreateWorkTaskPatternComponent implements OnInit {

    public name: string;
    public workTaskId: string;

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private taskPatternService: TaskPatternService) {
        this.route.paramMap.subscribe(params => {
            this.workTaskId = params.get("id").toString();
        });
    }

    ngOnInit() {
        
    }

    onSave() {
        this.taskPatternService
          .add(this.name, this.workTaskId)
          .pipe(first())
          .subscribe(
            result => {
              if (result) {
                this.router.navigate(["/projects"]);
              }
            },
            error => {
              console.log(error);
            }
        );
    }
}