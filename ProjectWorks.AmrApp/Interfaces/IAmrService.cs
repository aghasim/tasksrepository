﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectWorks.AmrApp.Interfaces
{
    public interface IAmrService<TEntity> where TEntity : class
    {
        #region Methods

        Task<TEntity> Get(int filialId, int id);

        Task<IEnumerable<TEntity>> GetByFilialId(int filialId);

        #endregion
    }
}