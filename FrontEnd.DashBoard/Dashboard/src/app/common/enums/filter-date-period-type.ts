export enum FilterDatePeriodType {
  Week = 1,
  Month,
  Quarter,
  None
}