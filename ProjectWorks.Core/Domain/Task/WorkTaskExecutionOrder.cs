﻿using System;
using System.ComponentModel.DataAnnotations;
using ProjectWorks.Core.Common.Enums;

namespace ProjectWorks.Core.Domain.Task
{
    /// <summary>
    ///     Порядок исполнения задачи
    /// </summary>
    public class WorkTaskExecutionOrder : EntityBase
    {
        #region Properties

        // Тип порядка исполнения задачи
        public WorkTaskExecutionOrderType WorkTaskExecutionOrderType { get; set; }

        // Порядок исполнения задачи
        [Range(0, int.MaxValue)]
        public int ExecutionOrder { get; set; }

        // Задача Id
        public Guid WorkTaskId { get; set; }

        // Задача
        public virtual WorkTask WorkTask { get; set; }

        #endregion
    }
}